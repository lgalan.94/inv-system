const withMT = require("@material-tailwind/react/utils/withMT");
 
module.exports = withMT({
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
            fontFamily: {
              roboto: ['Roboto', 'sans-serif'],
            },
            colors: {
              light: '#f5f5f5',
              dark: '#2f3542',
              defaultColor: '#dbe2ef',
            },
        },
  },
  plugins: [
         require('tailwind-scrollbar'),
      ],
});