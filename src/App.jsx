import { Routes, Route, useLocation, useNavigate, useParams } from 'react-router-dom';
import { Home, Categories, Landing, ManageProfile, Items, Rooms, Notifications, Samples, SamplesStorageLocation, Experiments } from './pages/manager';
import { AdminHome, Buildings, Users, Settings, ManageBuilding } from './pages/admin';
import { ClerkHome, ClerkLanding } from './pages/clerk';
import Login from './auth/Login';
import ProtectedRoutes from './auth/ProtectedRoutes';
import LabManagerRoutes from './auth/LabManagerRoutes';
import ClerkRoutes from './auth/ClerkRoutes';
import PageNotFound from './pages/PageNotFound';
import { DataProvider } from './DataContext';
import { useState, useEffect } from 'react';
import { Typography } from '@material-tailwind/react';
import { MdNoEncryptionGmailerrorred } from "react-icons/md";

function App() {
  let storage = localStorage.getItem('token');
  let api_url = `${import.meta.env.VITE_API_URL}/ulafis/user/details`;
  let location = useLocation();
  let navigate = useNavigate();

  const [data, setData] = useState({
     id: null,
     name: null,
     email: null,
     userRole: null,
     imageUrl: null
  })

  const [building, setBuilding] = useState({
      buildingId: null,
      buildingName: null,
      buildingImage: null,
      buildingAddress: null
  })

  const [settings, setSettings] = useState({
      settingsName: null,
      shortName: null
  })
  
  const unsetData = () => {
    localStorage.clear();
  }
  
  const retrieveData = () => {
    fetch(api_url, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${storage}`
      }
    })
      .then(result => result.json())
      .then(info => {
        setData({
          id: info._id,
          name: info.name,
          email: info.email,
          userRole: info.userRole,
          imageUrl: info.imageUrl
        }) 
        info.buildingsAssigned.map(info2 => {
           setBuilding({
              buildingId: info2.buildingId,
              buildingName: info2.buildingName,
              buildingImage: info2.buildingImage,
              buildingAddress: info2.buildingAddress
           })
        })     
      })
      .catch(error => console.error)
  }; 

  const fetchSettingsData = () => {
      fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings`, {
          method: "GET"
      })
      .then(response => response.json())
      .then(resp => {
          let SNAME = resp.find((name) => name.key === 'SYSTEM NAME'); 
          let SSNAME = resp.find((sname) => sname.key === 'SHORT NAME'); 

          if (SNAME && SSNAME) {
            setSettings({
                settingsName: SNAME.value,
                shortName: SSNAME.value
            })
          }
      })
      .catch(error => console.error)
  }

  useEffect(() => {
     retrieveData();
     fetchSettingsData();
  }, [])

  return (
    <DataProvider value={{data, setData, unsetData, building, setBuilding, settings, setSettings}}>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path='/auth/login' element={<Login />} />
        <Route path="/*" element={<PageNotFound />} />
        <Route path='/manage-profile' element={<ManageProfile />} />

        <Route element={<LabManagerRoutes />}>
          <Route path='/lab-manager/' element={<Landing />} />
          <Route path='/lab-manager/:buildingId/home' element={<Home />} />
          <Route path='/lab-manager/:buildingId/inventory' element={<Items />} />
          <Route path='/lab-manager/:buildingId/categories' element={<Categories />} />
          <Route path='/lab-manager/:buildingId/storage-locations' element={<Rooms />} />
          <Route path='/lab-manager/:buildingId/notifications' element={<Notifications />} />

          <Route path='/lab-manager/:buildingId/samples' element={<Samples />} />
          <Route path='/lab-manager/:buildingId/samples/locations' element={<SamplesStorageLocation />} />
          <Route path='/lab-manager/:buildingId/experiment-tracking' element={<Experiments />} />

        </Route>

        <Route element={<ProtectedRoutes />}>
          <Route path='/admin/home' element={<AdminHome />} />
          <Route path='/admin/buildings' element={<Buildings />} />
          <Route path='/admin/buildings/building/:buildingId' element={<ManageBuilding />} />
          <Route path='/admin/users' element={<Users />} />
          <Route path='/admin/settings' element={<Settings />} />
        </Route>

        <Route element={<ClerkRoutes />}>
          <Route path='/clerk' element={<ClerkLanding />} />
          <Route path='/clerk/:buildingId/home' element={<ClerkHome />} />
        </Route>

      </Routes>
    </DataProvider>
  )
}

export default App
