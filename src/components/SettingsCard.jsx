import { Textarea, Accordion, AccordionHeader, AccordionBody, Input, Card, CardBody, Typography } from '@material-tailwind/react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { CustomDialogBox, InvisibleButton, CustomButton, CustomDrawer } from './';
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { useState, useEffect, useContext } from 'react';
import { ImSpinner2 } from "react-icons/im";
import DataContext from '../DataContext';
import { useNavigate } from 'react-router-dom';

const SettingsCard = ({SettingsProps, fetchdata}) => {
	let navigate = useNavigate();
	let { _id, key, value } = SettingsProps;
	const { unsetData, setData, setBuilding, setSettings } = useContext(DataContext);
	const [openDialog, setOpenDialog] = useState(false);
	const [openDialog2, setOpenDialog2] = useState(false);
	const [title, setTitle] = useState('');
	const [open, setOpen] = useState(0); 
	const [isClicked, setIsClicked] = useState(false);
	const [isUpdateClicked, setIsUpdateClicked] = useState(false);
	const [openDrawer, setOpenDrawer] = useState(false);
	const [isDisabled, setIsDisabled]  = useState(true);

	const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
	const handleCloseDrawer = () => setOpenDrawer(false);

	const handleOpenDialog = () => setOpenDialog(!openDialog);	
	const handleOpen = (value) => setOpen(open === value ? 0 : value);
	
	const [newKey, setKey] = useState(key, (prevKey) => prevKey); // Access previous key
	const [newValue, setValue] = useState(value, (prevValue) => prevValue); // Access previous value

	const handleKeyChange = (e) => {
				setKey((prevKey) => e.target.value); 
	}

	const handleValueChange = (e) => {
				setValue((prevValue) => e.target.value); 
	}

 const handleDelete = async (e) => {
					setIsClicked(true);
	    e.preventDefault();
	    try {
	      const response = await fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings/${_id}`, {
	        method: 'DELETE',
	        headers: {
	          'Content-Type': 'application/json'
	        }
	      });
	      if (response.ok) {
	        toast.success(`${key} Successfully Deleted!`);
	        setOpenDialog(false)
	        setIsClicked(false)
	        fetchData();
	      } else {
	        toast.error('Delete Error! ');
	        setTimeout(() => 1000);
	        setIsClicked(false)
	      }
	    } catch (error) {
	      alert("unknown error")
	    }
	  };

	 const handleUpdate = (event) => {
	   event.preventDefault();
	   setIsDisabled(true);
	   setIsUpdateClicked(true);
	   fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings/${_id}`, {
	     method: 'PATCH',
	     headers: {
	       'Content-Type': 'application/json',
	     },
	     body: JSON.stringify({
	       newKey,
	       newValue
	     })
	   })
	     .then(response => response.json())
	     .then(data => {
	       if (data === true) {
	         toast.success(<span className="text-xs">{key.toUpperCase()} has been updated Successfully!</span>);
	         setOpenDrawer(false);
	         setIsUpdateClicked(false);
	         setTimeout(() => setOpenDialog2(true), 3500)
	         fetchData();
	         setIsDisabled(false);
	       } else {
	         toast.error('Error updating settings!');
	         setTimeout(() => 1000);
	         setIsUpdateClicked(false);
	       }
	     });
	 };

	 useEffect(() => {
	 		newValue !== value ? setIsDisabled(false) : setIsDisabled(true)
	 }, [newValue])

	 const handleLogout = () => {
	     unsetData();
	     setData({
	       id: null,
	       name: null,
	       email: null,
	       userRole: null,
	       imageUrl: null,
	       buildingsAssigned: []
	     });
	     setBuilding({
	       buildingId: null,
	       buildingName: null
	     })
	     setSettings({
	     		settingsName: null,
	     		shortName: null
	     })
	     navigate('/')
	 }

		return (
			<>
				<CustomDrawer
						open={openDrawer}
						handleOpenDrawer={handleOpenDrawer}
						handleCloseDrawer={handleOpenDrawer}
						title="Update"
				>
						<form className="flex flex-col gap-4 p-3">
						  <div className="">
						  		<Input
						  				disabled
						  				label="KEY" 
						  				value={newKey}
						  				onChange={handleKeyChange}
						  				color="teal"
						  		/>
						  </div>
						  <div className="">
						  		<Textarea
						  				label="VALUE" 
						  				value={newValue}
						  				onChange={handleValueChange}
						  				color="teal"
						  		/>
						  </div>
						</form>
						<div className="flex flex-row mt-4 gap-2 justify-center">
								<CustomButton 
										label="Cancel"
										handleClick={handleCloseDrawer}
										tooltip="hidden"
								/>
								<CustomButton 
										label={ isUpdateClicked ? "Updating ..." : "Update" }
										tooltip="hidden"
										handleClick={handleUpdate}
										color="green"
										isDisabled={isDisabled}
										loading={ isUpdateClicked ? true : false }
								/>
						</div>
				</CustomDrawer>

				<Card className="shadow-none border border-2 rounded-md border-blue-gray-300 group bg-[#E9E9EA]/60">
						<CardBody className="flex flex-col gap-1">
								<Typography className="uppercase text-xl tracking-wider font-bold">{key}</Typography>
								<Typography className="capitalize text-md tracking-wide font-normal">{value}</Typography>

								<div className="flex flex-row gap-2">
											<InvisibleButton 
										  label="edit"
										  icon={<MdRebaseEdit className="w-4 h-4" />}
										  tooltip="!bg-green-600"
										  buttonClass="!text-green-600"
										  color="green"
										  handleClick={handleOpenDrawer}
										/>
										{/*<InvisibleButton
												icon={<RiDeleteBin3Fill />}
												label="delete"
												tooltip="!bg-red-500"
												placement="top"
												btn=""
												handleClick={handleOpenDialog}
										/>*/}
								</div>

						</CardBody>
				</Card>
   	<CustomDialogBox
   			title="Confirmation"
   			open={openDialog}
   			handleOpen={handleOpenDialog}
   	>
   				Delete {key}?
   			
   			<div className="mt-10 flex flex-row justify-center gap-1">
   					<CustomButton
   							label="cancel"
   							handleClick={handleOpenDialog}
   					/>
   					{
   							!isClicked ? (
												<CustomButton
														label="delete"
														color="red"
														handleClick={handleDelete}
												/>
   								):(
   									<CustomButton
   											label="deleting..."
   											color="red"
   											isDisabled={true}
   									/>
   							)
   					}
   			</div>

   </CustomDialogBox><CustomDialogBox
	  					title="Re-login"
	  					open={openDialog2}
	  			>
	  				 <Typography className="py-5"> For the changes to take effect, you have to log back in! </Typography>

	  				 	<CustomButton
	  				 			label="Go"
	  				 			tooltip="hidden"
	  				 			handleClick={handleLogout}
	  				 			btn="mx-auto"
	  				 	/>

	  			</CustomDialogBox>
						</>
			)
}

export default SettingsCard