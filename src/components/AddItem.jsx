import { Input, Textarea, Select, Option } from '@material-tailwind/react';
import { CustomButton } from './'
import DataContext from '../DataContext';
import { useState, useContext, useEffect } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const AddItem = ({ fetchData }) => {

	const { building } = useContext(DataContext);
	let url = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMtaN6L0FkLLyNRmuaul-Vdh6MIt9P5M5bqpihnmWpzw&s';
	const [itemName, setItemName] = useState('');
	const [description, setDescription] = useState('');
	const [quantity, setQuantity] = useState('');
	const [location, setLocation] = useState(null);
	const [category, setCategory] = useState(null);
	const [categoryId, setCategoryId] = useState('');
	const [imageUrl, setImageUrl] = useState(url);
	const [lowStockThreshold, setLowStockThreshold] = useState('');
	const [isSave, setIsSave] = useState(false);
	const [isDisabled, setIsDisabled] = useState(true);

	const [categoryItems, setCategoryItems] = useState([]);
	const [locationItems, setLocationItems] = useState([]);

	useEffect(() => {
	  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`)
	  .then(response => response.json())
	  .then(result => {
	     setCategoryItems(result.categories.map(cat => {
	        return (
	            <Option key={cat._id} value={cat.categoryName}> {cat.categoryName.toUpperCase()} </Option>
	         )
	     }))
	     setLocationItems(result.rooms.map(loc => {
	        return (
	            <Option key={loc._id} value={loc.roomName} > {loc.roomName.toUpperCase()} </Option>
	         )
	     }))
	  })
	}, [])

	useEffect(() => {
			itemName.length > 0 &&
			description.length > 0 &&
			quantity.length > 0 &&
			location !== null &&
			category !== null > 0 &&
			imageUrl.length > 0 && 
			lowStockThreshold.length > 0 ? setIsDisabled(false) : setIsDisabled(true);
	}, [itemName, description, quantity, location, category, imageUrl, lowStockThreshold])

	/*handle add*/
	let itemData = {
			itemName,
			quantity,
			lowStockThreshold,
			location,
			category,
			categoryId,
			imageUrl,
			description
	}
		const handleAdd = (e) => {
				e.preventDefault();
				setIsDisabled(true);
				setIsSave(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/${building.buildingId}/add-item`, {
						method: 'POST',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify(itemData)
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${itemName.toUpperCase()} Successfully Added!`);
										fetchData();
										setTimeout(() => {
											setIsSave(false)
											setIsDisabled(false)
										}, 4000)
										clearFields();
							} else {
										toast.error('Unable to add new item!')
										setIsSave(false);
										setIsDisabled(false);
							}
				})
		}
	/**/

		const clearFields = () => {
				setItemName("");
				setDescription("");
				setQuantity("");
				setLocation([]);
				setCategory(null);
				setImageUrl(url);
				setLowStockThreshold("");
		}

		// const handleCategoryChange = (e) => {

		//   if (e) {
		//     const { _id, categoryName } = e; // Extract from the selected category object
		//     setCategoryId(e._id);
		//     setCategory(e.categoryName);
		//     // Now you have categoryId and categoryName to post
		//   }
		// };

			return (
			  <div>
			     <form className="flex flex-col gap-2">

			       <div>
			         <Input
			           type="text"
			           size="sm"
			           value={itemName.toUpperCase()}
			           onChange={(e) => setItemName(e.target.value)}
			           className={`capitalize`}
			           label="Name"
			           color="teal"
			         />
			       </div>
			       
			       <div>
			         <Input
			           type="number"
			           size="sm"
			           value={quantity}
			           onChange={(e) => setQuantity(e.target.value)}
			           className={`capitalize`}
			           label="Quantity"
			           color="teal"
			         />
			       </div>

			       <div>
			         <Input
			           type="number"
			           size="sm"
			           value={lowStockThreshold}
			           onChange={(e) => setLowStockThreshold(e.target.value)}
			           className={`capitalize`}
			           label="Low Stock Threshold"
			           color="teal"
			         />
			       </div>

			       <div className="w-full">
			          <Select color="teal" onChange={(e) => setCategory(e)} label="Category">
			             {categoryItems}
			          </Select>
			       </div>

			       <div className="w-full">
			          <Select color="teal" onChange={(e) => setLocation(e)} label="Storage Location">
			             {locationItems}
			          </Select>
			       </div>
			        
			       <div className="w-full">
			         <Textarea 
			           label="Description"
			           value={description} 
			           onChange={(e) => setDescription(e.target.value)}
			           resize={true}
			           className="min-h-[40px]"
			           color="teal"
			         />
			       </div>
			       <div className="-mt-2 w-full">
			         <Textarea 
			           label="Image Url"
			           value={imageUrl}
			           onChange={(e) => setImageUrl(e.target.value)}
			           resize={true}
			           className="min-h-[60px] text-xs scrollbar-thin"
			           color="teal"
			         />
			       </div>
			       <div>
			         <img className="m-auto shadow-md" src={imageUrl} />
			       </div>
			     </form>

			     <div className="mt-6 flex flex-row gap-1 justify-center mx-auto">
			         <CustomButton
			           label="Save"
			           handleClick={handleAdd}
			           btnClass="rounded px-3 py-2 hover:scale-105"
			           color="green"
			           isDisabled={isDisabled}
			           tooltip="hidden"
			           loading={isSave ? true : false}
			         />
			     </div>

			  </div>
			);
}

export default AddItem