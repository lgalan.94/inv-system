import { InvisibleButton, CustomDrawer, CustomButton, CustomDialogBox } from '../';
import { Typography, Input, Select, Option, Textarea } from '@material-tailwind/react';
import { MdDeleteOutline } from "react-icons/md";
import { IoMdEye } from "react-icons/io";
import { FaRegEdit } from "react-icons/fa";
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Items = ({ itemProps, buildingId, fetchData, count }) => {

		const { _id, imageUrl, itemName, description, category, quantity, lowStockThreshold, location } = itemProps;
		const [openViewDrawer, setOpenViewDrawer] = useState(false);
		const [openUpdateDrawer, setOpenUpdateDrawer] = useState(false);
		const [openDialogBox, setOpenDialogBox] = useState(false);

		const [newItemName, setNewItemName] = useState(itemName);
		const [newDescription, setNewDescription] = useState(description);
		const [newQuantity, setNewQuantity] = useState(quantity);
		const [newLocation, setNewLocation] = useState(location);
		const [newCategory, setNewCategory] = useState(category);
		const [newImageUrl, setNewImageUrl] = useState(imageUrl);
		const [newLowStockThreshold, setNewLowStockThreshold] = useState(lowStockThreshold);

		const [isDeleted, setIsDeleted] = useState(false);
		const [isUpdated, setIsUpdated] = useState(false);
		const [isDisabled, setIsDisabled] = useState(true);

		useEffect(() => {
				newItemName !== itemName ||
				newDescription !== description ||
				newQuantity !== quantity ||
				newLocation !== location ||
				newCategory !== category ||
				newImageUrl !== imageUrl ||
				newLowStockThreshold !== lowStockThreshold ?
				setIsDisabled(false) : setIsDisabled(true)
		}, [newItemName, newDescription, newQuantity, newLocation, newCategory, newImageUrl, newLowStockThreshold])

		const handleOpenViewDrawer = () => setOpenViewDrawer(!openViewDrawer);
		const handleOpenUpdateDrawer = () => {
					setOpenViewDrawer(false);
					setOpenUpdateDrawer(!openUpdateDrawer)
		}
		const handleOpenDialogBox = () => {
					setOpenViewDrawer(false);
					setOpenDialogBox(!openDialogBox);
		}

		const [categoryItems, setCategoryItems] = useState([]);
		const [locationItems, setLocationItems] = useState([]);

		useEffect(() => {
		  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${buildingId}`)
		  .then(response => response.json())
		  .then(result => {
		     setCategoryItems(result.categories.map(cat => {
		        return (
		            <Option key={cat._id} value={cat.categoryName}> {cat.categoryName.toUpperCase()} </Option>
		         )
		     }))
		     setLocationItems(result.rooms.map(loc => {
		        return (
		            <Option key={loc._id} value={loc.roomName} > {loc.roomName.toUpperCase()} </Option>
		         )
		     }))
		  })
		}, [])

		const handleDelete = (_id) => {
				setIsDeleted(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/${buildingId}/remove/${_id}`, {
							method: 'PATCH',
							headers: {
									'Content-type': 'application/json'
							}
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${itemName.toUpperCase()} Successfully Deleted!`);
										fetchData();
										setTimeout(() => {
												setIsDeleted(false)
												setOpenDialogBox(false)
										}, 3000)
							}
							else
							{
									toast.error('Cannot remove item!');
									setOpenDialogBox(false);
							}
				})
		}

		const handleUpdate = () => {
				
				let updatedItem = {
						newItemName: newItemName,
						newDescription: newDescription,
						newQuantity: newQuantity,
						newLocation: newLocation,
						newCategory: newCategory,
						newImageUrl: newImageUrl,
						newLowStockThreshold: newLowStockThreshold
				}

				setIsDisabled(true);
				setIsUpdated(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/${buildingId}/update/${_id}`, {
						method: "PATCH",
						headers: {
							'Content-type': 'application/json'
						},
						body: JSON.stringify(updatedItem)
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
									toast.success("Successfully Updated!");
									fetchData();
									setTimeout(() => {
											setOpenUpdateDrawer(false);
											setIsUpdated(false);
									}, 3000)
							} else {
									toast.error("Unable to update data!");
									setOpenUpdateDrawer(false);
									setIsUpdated(false);
							}

				})
				.catch(error => console.error)
		}

			return (
								<>
											<tr className="hover:bg-blue-gray-300/30 group text-xs">
													<td className="border-b border-blue-gray-50">
													  <Typography className="text-md ml-1" > {count} </Typography>
													</td>
											  <td className="border-b border-blue-gray-50">
											    <img className="w-9 border border-dark/10 ml-1 shadow shadow-sm h-9 rounded-full" src={imageUrl} />
											  </td>
											  <td className="border-b border-blue-gray-50">
											    <Typography className="text-md" >{itemName.toUpperCase().substring(0, 25) + ""}</Typography>
											  </td>
											  <td className="border-b border-blue-gray-50">
											    <div className="flex flex-row justify-end mr-3">
											      <div className="mr-2">
											      	<InvisibleButton 
											      	  label="view"
											      	  icon={<IoMdEye className="w-3 h-3" />}
											      	  color="cyan"
											      	  handleClick={handleOpenViewDrawer}
											      	/>
											      </div>
											      <div className="">
											      	<InvisibleButton 
											        label="edit"
											        icon={<FaRegEdit className="w-3 h-3" />}
											        tooltip="!bg-green-600"
											        buttonClass="!text-green-600"
											        color="green"
											        handleClick={handleOpenUpdateDrawer}
											      />
											      </div>
											      <div className="ml-2">
										      		<InvisibleButton 
										      		  label="delete"
										      		  icon={<MdDeleteOutline className="w-3 h-3" />}
										      		  tooltip="!bg-red-600"
										      		  buttonClass="!text-red-600"
										      		  color="red"
										      		  handleClick={handleOpenDialogBox}
										      		/>
											      </div>
											    </div>
											  </td>
											</tr>
											
											{/*View item drawer*/}
													<CustomDrawer
															title={itemName.toUpperCase()}
															open={openViewDrawer}
															handleOpenDrawer={handleOpenViewDrawer}
															handleCloseDrawer={handleOpenViewDrawer}
															className={ openViewDrawer ? "min-w-[50vw]" : "" }
													>

													<div className="flex flex-col gap-4">
															<img className="border border-dark/10 max-w-[20rem] max-h-[22rem] mx-auto" src={imageUrl} />
															<div className="px-20 flex flex-col gap-3">
																<div className="flex flex-row">
																  <div className="basis-1/5">
																    <Typography className="font-normal text-sm text-dark/50">Description:</Typography>
																  </div>
																  <div className="basis-4/5">
																    <Typography className="font-normal text-sm">{description}</Typography>
																  </div>
																</div>

																<div className="flex flex-row">
																  <div className="basis-1/5">
																    <Typography className="font-normal text-sm text-dark/50">Quantity/Stock:</Typography>
																  </div>
																  <div className="basis-4/5">
																    <Typography className="font-normal text-sm">{quantity}</Typography>
																  </div>
																</div>

																<div className="flex flex-row">
																  <div className="basis-1/5">
																    <Typography className="font-normal text-sm text-dark/50">LS Threshold:</Typography>
																  </div>
																  <div className="basis-4/5">
																    <Typography className="font-normal text-sm">{lowStockThreshold}</Typography>
																  </div>
																</div>

																<div className="flex flex-row">
																  <div className="basis-1/5">
																    <Typography className="font-normal text-sm text-dark/50">Category:</Typography>
																  </div>
																  <div className="basis-4/5">
																    <Typography className="font-normal text-sm">{category}</Typography>
																  </div>
																</div>

																<div className="flex flex-row">
																  <div className="basis-1/5">
																    <Typography className="font-normal text-sm text-dark/50">Location:</Typography>
																  </div>
																  <div className="basis-4/5">
																    <Typography className="font-normal text-sm">{location}</Typography>
																  </div>
																</div>

															</div>
													</div>

													
													<div className="flex flex-row gap-2 mt-10 justify-center">
														<CustomButton
																label="Close"
																tooltip="hidden"
																handleClick={handleOpenViewDrawer}
														/>
														<CustomButton
																label="Edit"
																tooltip="hidden"
																color="green"
																handleClick={handleOpenUpdateDrawer}
														/>
														<CustomButton
																label="Delete"
																tooltip="hidden"
																color="red"
																handleClick={handleOpenDialogBox}
														/>
													</div>

													</CustomDrawer>
											{/**/}							

											{/*Handle edit/update item drawer*/}
													<CustomDrawer
															title={`Update ${itemName.toUpperCase()}`}
															open={openUpdateDrawer}
															handleOpenDrawer={handleOpenUpdateDrawer}
															handleCloseDrawer={handleOpenUpdateDrawer}
															className={ openUpdateDrawer ? "min-w-[50vw]" : "" }
													>

														
															{/*handle update form*/}
																	<div className="p-5"	>
																			<form className="flex flex-col gap-2">
																					<div className="w-full">
																					  <Input
																					    type="text"
																					    size="sm"
																					    value={newItemName.toUpperCase()}
																					    onChange={(e) => setNewItemName(e.target.value)}
																					    className={`capitalize`}
																					    label="Name"
																					    color="teal"
																					  />
																					</div>
																			  <div className="flex flex-row gap-2">
																			  	<div className="w-full">
																			  	  <Input
																			  	    type="number"
																			  	    size="sm"
																			  	    value={newQuantity}
																			  	    onChange={(e) => setNewQuantity(e.target.value)}
																			  	    className={`capitalize`}
																			  	    label="Quantity"
																			  	    color="teal"
																			  	  />
																			  	</div>

																			  	<div className="w-full">
																			  	  <Input
																			  	    type="number"
																			  	    size="sm"
																			  	    value={newLowStockThreshold}
																			  	    onChange={(e) => setNewLowStockThreshold(e.target.value)}
																			  	    className={`capitalize`}
																			  	    label="Low Stock Threshold"
																			  	    color="teal"
																			  	  />
																			  	</div>
																			  </div>

																			  <div className="flex flex-row gap-2">
																			  	<div className="w-full">
																			  	   <Select color="teal" onChange={(e) => setNewCategory(e)} label={`Current: ${category}`}>
																			  	      {categoryItems}
																			  	   </Select>
																			  	</div>

																			  	<div className="w-full">
																			  	   <Select color="teal" onChange={(e) => setNewLocation(e)} label={`Current: ${location}`}>
																			  	      {locationItems}
																			  	   </Select>
																			  	</div>
																			  </div>

																			  <div className="flex flex-row gap-2">
																			  		<div className="w-full">
																			  		  <Textarea 
																			  		    label="Description"
																			  		    value={newDescription} 
																			  		    onChange={(e) => setNewDescription(e.target.value)}
																			  		    resize={true}
																			  		    className="min-h-[40px]"
																			  		    color="teal"
																			  		  />
																			  		</div>
																			  		<div className="w-full">
																			  		  <Textarea 
																			  		    label="Image Url"
																			  		    value={newImageUrl}
																			  		    onChange={(e) => setNewImageUrl(e.target.value)}
																			  		    resize={true}
																			  		    className="min-h-[60px] text-xs scrollbar-thin"
																			  		    color="teal"
																			  		  />
																			  		</div>
																			  </div>

																			  <div>
																			    <img className="m-auto shadow-md w-[40%]" src={newImageUrl} />
																			  </div>
																			</form>
																	</div>
																	<div className="flex flex-row gap-2 mt-6 justify-center">
																		<CustomButton
																				label="Close"
																				tooltip="hidden"
																				handleClick={handleOpenUpdateDrawer}
																		/>
																		<CustomButton
																				label="Update"
																				tooltip="hidden"
																				color="cyan"
																				loading={isUpdated ? true : false}
																				isDisabled={isDisabled}
																				handleClick={handleUpdate}
																		/>
																	</div>
															{/**/}
														
														
													</CustomDrawer>
											{/**/}

											<CustomDialogBox
												title="Confirmation"
												open={openDialogBox}
												handler={handleOpenDialogBox}
											>

													<div className="flex items-center justify-center min-h-[30vh]">
															Delete {itemName}?
													</div>
													<div className="flex flex-row gap-2 mt-10 justify-center">
														<CustomButton
																label="Cancel"
																tooltip="hidden"
																handleClick={handleOpenDialogBox}
														/>
														<CustomButton
																label="Delete"
																tooltip="hidden"
																color="red"
																loading={isDeleted ? true : false}
																isDisabled={isDeleted ? true : false}
																handleClick={() => handleDelete(_id)}
														/>
													</div>

											</CustomDialogBox>
								</>
				)
}

export default Items