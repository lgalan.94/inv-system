import { InvisibleButton, CustomDrawer, CustomButton, CustomDialogBox } from '../';
import { Typography, Input, Select, Option, Textarea } from '@material-tailwind/react';
import { MdDeleteOutline } from "react-icons/md";
import { IoMdEye } from "react-icons/io";
import { FaRegEdit } from "react-icons/fa";
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const StorageLocations = ({ itemProps, buildingId, fetchData, count }) => {

		const { _id, roomName } = itemProps;
		const [openViewDrawer, setOpenViewDrawer] = useState(false);
		const [openUpdateDrawer, setOpenUpdateDrawer] = useState(false);
		const [openDialogBox, setOpenDialogBox] = useState(false);

		const [newRoomName, setNewRoomName] = useState(roomName);

		const [isDeleted, setIsDeleted] = useState(false);
		const [isUpdated, setIsUpdated] = useState(false);
		const [isDisabled, setIsDisabled] = useState(true);

		useEffect(() => {
				newRoomName !== roomName ?
				setIsDisabled(false) : setIsDisabled(true)
		}, [newRoomName])

		const handleOpenViewDrawer = () => setOpenViewDrawer(!openViewDrawer);
		const handleOpenUpdateDrawer = () => {
					setOpenViewDrawer(false);
					setOpenUpdateDrawer(!openUpdateDrawer)
		}
		const handleOpenDialogBox = () => {
					setOpenViewDrawer(false);
					setOpenDialogBox(!openDialogBox);
		}

		const handleDelete = (_id) => {
				setIsDeleted(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/room/${buildingId}/remove/${_id}`, {
							method: 'PATCH',
							headers: {
									'Content-type': 'application/json'
							}
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${roomName.toUpperCase()} Successfully Deleted!`);
										fetchData();
										setTimeout(() => {
												setIsDeleted(false)
												setOpenDialogBox(false)
										}, 3000)
							}
							else
							{
									toast.error('Cannot remove item!');
									setOpenDialogBox(false);
							}
				})
		}

		const handleUpdate = () => {
				
				let updatedItem = {
						newRoomName: newRoomName
				}

				setIsDisabled(true);
				setIsUpdated(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/room/${buildingId}/update/${_id}`, {
						method: "PATCH",
						headers: {
							'Content-type': 'application/json'
						},
						body: JSON.stringify(updatedItem)
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
									toast.success("Successfully Updated!");
									fetchData();
									setTimeout(() => {
											setOpenUpdateDrawer(false);
											setIsUpdated(false);
									}, 3000)
							} else {
									toast.error("Unable to update data!");
									setOpenUpdateDrawer(false);
									setIsUpdated(false);
							}

				})
				.catch(error => console.error)
		}

			return (
								<>
											<tr className="hover:bg-blue-gray-300/30 group text-xs">
													<td className="border-b border-blue-gray-50">
													  <Typography className="text-md ml-1" > {count} </Typography>
													</td>
											  <td className="border-b border-blue-gray-50">
											    <Typography className="text-md" >{roomName.toUpperCase().substring(0, 25) + ""}</Typography>
											  </td>
											  <td className="border-b border-blue-gray-50">
											    <div className="flex flex-row justify-end mr-3">
											      <div className="mr-1">
											      	<InvisibleButton 
											        label="edit"
											        icon={<FaRegEdit className="w-3 h-3" />}
											        tooltip="!bg-green-600"
											        buttonClass="!text-green-600"
											        color="green"
											        handleClick={handleOpenUpdateDrawer}
											      />
											      </div>
											      <div className="ml-1">
										      		<InvisibleButton 
										      		  label="delete"
										      		  icon={<MdDeleteOutline className="w-3 h-3" />}
										      		  tooltip="!bg-red-600"
										      		  buttonClass="!text-red-600"
										      		  color="red"
										      		  handleClick={handleOpenDialogBox}
										      		/>
											      </div>
											    </div>
											  </td>
											</tr>
																	

											{/*Handle edit/update item drawer*/}
													<CustomDrawer
															title="Update"
															open={openUpdateDrawer}
															handleOpenDrawer={handleOpenUpdateDrawer}
															handleCloseDrawer={handleOpenUpdateDrawer}
															className={ openUpdateDrawer ? "min-w-[30vw]" : "" }
													>

														
															{/*handle update form*/}
																	<div className="p-5"	>
																			<form className="flex flex-col gap-2">
																					<div className="w-full">
																					  <Input
																					    type="text"
																					    size="sm"
																					    value={newRoomName.toUpperCase()}
																					    onChange={(e) => setNewRoomName(e.target.value)}
																					    className={`capitalize`}
																					    label="Name"
																					    color="teal"
																					  />
																					</div>
																			</form>
																	</div>
																	<div className="flex flex-row gap-2 mt-6 justify-center">
																		<CustomButton
																				label="Close"
																				tooltip="hidden"
																				handleClick={handleOpenUpdateDrawer}
																		/>
																		<CustomButton
																				label="Update"
																				tooltip="hidden"
																				color="cyan"
																				loading={isUpdated ? true : false}
																				isDisabled={isDisabled}
																				handleClick={handleUpdate}
																		/>
																	</div>
															{/**/}
														
														
													</CustomDrawer>
											{/**/}

											<CustomDialogBox
												title="Confirmation"
												open={openDialogBox}
												handler={handleOpenDialogBox}
											>

													<div className="flex items-center justify-center min-h-[30vh]">
															Delete {roomName}?
													</div>
													<div className="flex flex-row gap-2 mt-10 justify-center">
														<CustomButton
																label="Cancel"
																tooltip="hidden"
																handleClick={handleOpenDialogBox}
														/>
														<CustomButton
																label="Delete"
																tooltip="hidden"
																color="red"
																loading={isDeleted ? true : false}
																isDisabled={isDeleted ? true : false}
																handleClick={() => handleDelete(_id)}
														/>
													</div>

											</CustomDialogBox>
								</>
				)
}

export default StorageLocations