import { Input, Textarea } from '@material-tailwind/react';
import { CustomButton } from '../'
import { useState, useEffect } from 'react';

const AddStorageLocation = ({ 
	locationName,
	setLocationName,
	handleAddLocation,
	isSave,
	isDisabled
	  }) => {

			return (
			  <div>
			     <form className="flex flex-col gap-2">
			       <div>
			         <Input
			           type="text"
			           value={locationName.toUpperCase()}
			           onChange={setLocationName}
			           className={`capitalize`}
			           label="Storage Location Name"
			           color="teal"
			         />
			       </div>
			     </form>

			     <div className="mt-6 flex flex-row gap-1 justify-center mx-auto">
			         <CustomButton
			           label={ !isSave ? "Save" : "Saving ..." }
			           handleClick={handleAddLocation}
			           btnClass="rounded px-3 py-2 hover:scale-105"
			           color="green"
			           isDisabled={isDisabled}
			           tooltip="hidden"
			           loading={isSave ? true : false}
			         />
			     </div>

			  </div>
			);
}

export default AddStorageLocation