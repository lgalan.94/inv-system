import { Input, Textarea, Select, Option } from '@material-tailwind/react';
import { CustomButton } from '../'
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const AddItem = ({ 
	fetchData, 
	categoryItems, 
	locationItems, 
	_id,
	itemName,
	description,
	quantity,
	location,
	category,
	imageUrl,
	lowStockThreshold,
	setItemName,
	setDescription,
	setQuantity,
	setLocation,
	setCategory,
	setImageUrl,
	setLowStockThreshold,
	handleAdd,
	isSave
	  }) => {

	let url = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMtaN6L0FkLLyNRmuaul-Vdh6MIt9P5M5bqpihnmWpzw&s';
	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(() => {
			itemName.length > 0 &&
			description.length > 0 &&
			quantity.length > 0 &&
			location !== null &&
			category !== null > 0 &&
			imageUrl.length > 0 && 
			lowStockThreshold.length > 0 ? setIsDisabled(false) : setIsDisabled(true);
	}, [itemName, description, quantity, location, category, imageUrl, lowStockThreshold])

		const clearFields = () => {
				setItemName("");
				setDescription("");
				setQuantity("");
				setLocation([]);
				setCategory(null);
				setImageUrl(url);
				setLowStockThreshold("");
		}

			return (
			  <div>
			     <form className="flex flex-col gap-2">

			       <div>
			         <Input
			           type="text"
			           value={itemName.toUpperCase()}
			           onChange={setItemName}
			           className={`capitalize`}
			           label="Name"
			           color="teal"
			         />
			       </div>
			       
			       <div className="flex flex-row gap-2">
			       	<div className="w-full">
			       	  <Input
			       	    type="number"
			       	    value={quantity}
			       	    onChange={setQuantity}
			       	    className={`capitalize`}
			       	    label="Quantity"
			       	    color="teal"
			       	  />
			       	</div>

			       	<div className="w-full">
			       	  <Input
			       	    type="number"
			       	    value={lowStockThreshold}
			       	    onChange={setLowStockThreshold}
			       	    className={`capitalize`}
			       	    label="Low Stock Threshold"
			       	    color="teal"
			       	  />
			       	</div>
			       </div>

			       <div className="flex flex-row gap-2">
			       		<div className="w-full">
			       		   <Select color="teal" onChange={setCategory} label="Category">
			       		      {categoryItems}
			       		   </Select>
			       		</div>

			       		<div className="w-full">
			       		   <Select color="teal" onChange={setLocation} label="Storage Location">
			       		      {locationItems}
			       		   </Select>
			       		</div>
			       </div>
			        
			       <div className="w-full">
			         <Textarea 
			           label="Description"
			           value={description} 
			           onChange={setDescription}
			           resize={true}
			           className="min-h-[40px]"
			           color="teal"
			         />
			       </div>
			       <div className="-mt-2 w-full">
			         <Textarea 
			           label="Image Url"
			           value={imageUrl}
			           onChange={setImageUrl}
			           resize={true}
			           className="min-h-[60px] text-xs scrollbar-thin"
			           color="teal"
			         />
			       </div>
			       <div>
			         <img className="m-auto shadow-md" src={imageUrl} />
			       </div>
			     </form>

			     <div className="mt-6 flex flex-row gap-1 justify-center mx-auto">
			         {
			         	 !isSave ? (
			         	 				<CustomButton
			         	 				  label="Save"
			         	 				  handleClick={handleAdd}
			         	 				  btnClass="rounded px-3 py-2 hover:scale-105"
			         	 				  color="green"
			         	 				  isDisabled={isDisabled}
			         	 				  tooltip="hidden"
			         	 				  loading={isSave ? true : false}
			         	 				/>
			         	 	) : (
			         	 			<CustomButton
			         	 			  label="Saving ..."
			         	 			  btnClass="rounded px-3 py-2 hover:scale-105"
			         	 			  color="green"
			         	 			  isDisabled={true}
			         	 			  tooltip="hidden"
			         	 			  loading={true}
			         	 			/>
			         	 	)
			         }
			     </div>

			  </div>
			);
}

export default AddItem