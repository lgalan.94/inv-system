import { Input, Textarea } from '@material-tailwind/react';
import { CustomButton } from '../'
import { useState, useEffect } from 'react';

const AddCategory = ({ 
	categoryName,
	imageUrl,
	setCategoryName,
	setImageUrl,
	handleAddCategory,
	isSave,
	isDisabled
	  }) => {

	let url = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMtaN6L0FkLLyNRmuaul-Vdh6MIt9P5M5bqpihnmWpzw&s';

			return (
			  <div>
			     <form className="flex flex-col gap-2">

			       <div>
			         <Input
			           type="text"
			           value={categoryName.toUpperCase()}
			           onChange={setCategoryName}
			           className={`capitalize`}
			           label="Category Name"
			           color="teal"
			         />
			       </div>
			       <div className="w-full">
			         <Textarea 
			           label="Image Url"
			           value={imageUrl}
			           onChange={setImageUrl}
			           resize={true}
			           className="min-h-[60px] text-xs scrollbar-thin"
			           color="teal"
			         />
			       </div>
			       <div>
			         <img className="m-auto shadow-md" src={imageUrl} />
			       </div>
			     </form>

			     <div className="mt-6 flex flex-row gap-1 justify-center mx-auto">
			         <CustomButton
			           label={ !isSave ? "Save" : "Saving ..."}
			           handleClick={handleAddCategory}
			           btnClass="rounded px-3 py-2 hover:scale-105"
			           color="green"
			           isDisabled={isDisabled}
			           tooltip="hidden"
			           loading={isSave ? true : false}
			         />
			     </div>

			  </div>
			);
}

export default AddCategory