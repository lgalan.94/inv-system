import { Card, CardBody, CardFooter, Typography, Option, Input } from '@material-tailwind/react';
import { CustomButton, CustomDrawer } from '../';
import { AddItem } from './';
import { MdDeleteOutline } from "react-icons/md";
import { IoMdEye } from "react-icons/io";
import { FaRegEdit } from "react-icons/fa";
import { FaPlus } from "react-icons/fa6";
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const InventoryCard = ({ name, imageUrl, address, usersList, categories, items, rooms, handleAddItemDrawer, handleAddCategoryDrawer, handleAddLocationDrawer, handleInputSearchChange, searchTerm, itemCount }) => {

/*table headers*/
	const TABLE_HEAD1 = ["#", "Image", "Item Name", ""];
	const TABLE_HEAD2 = ["#", "Image", "Category Name", ""];
	const TABLE_HEAD3 = ["#", "Storage Location Name", ""];
/**/

		return (
					<>
								<div className="flex flex-col gap-3">
									<div className="flex flex-row gap-3">
											<Card className="min-w-[28vw] max-w-[28vw] border border-2 border-gray-400/20 shadow-none bg-[#E9E9EA]/80">
													<CardBody className="p-0 min-h-[48vh] max-h-[48vh]">															
													<div className="flex justify-between">
															
															<div className="flex flex-row gap-2">
																	<Typography variant="h6" color="green" className="py-2 ml-1">ITEMS</Typography>
																	<CustomButton
																			label=""
																			icon={<FaPlus />}
																			btn="lowercase"
																			variant="text"
																			handleClick={handleAddItemDrawer}
																			tooltip=""
																			content="add item"
																			placement="top"
																	/>
															</div>

															<div className="">
															 <div className="relative w-full">
															   <Input
															     type="search"
															     value={searchTerm}
															     onChange={handleInputSearchChange}
															     placeholder="Search Item"
															     containerProps={{
															       className: "min-w-[180px] max-w-[180px]",
															     }}
															     className="!border-blue-gray-50 shadow-lg pl-9 placeholder:text-blue-gray-300 focus:!border-blue-gray-100"
															     labelProps={{
															       className: "before:content-none after:content-none",
															     }}
															   />

															   <div className="!absolute left-3 top-[13px]">
															     <svg
															       width="13"
															       height="14"
															       viewBox="0 0 14 15"
															       fill="none"
															       xmlns="http://www.w3.org/2000/svg"
															     >
															       <path
															         d="M9.97811 7.95252C10.2126 7.38634 10.3333 6.7795 10.3333 6.16667C10.3333 4.92899 9.84167 3.742 8.9665 2.86683C8.09133 1.99167 6.90434 1.5 5.66667 1.5C4.42899 1.5 3.242 1.99167 2.36683 2.86683C1.49167 3.742 1 4.92899 1 6.16667C1 6.7795 1.12071 7.38634 1.35523 7.95252C1.58975 8.51871 1.93349 9.03316 2.36683 9.4665C2.80018 9.89984 3.31462 10.2436 3.88081 10.4781C4.447 10.7126 5.05383 10.8333 5.66667 10.8333C6.2795 10.8333 6.88634 10.7126 7.45252 10.4781C8.01871 10.2436 8.53316 9.89984 8.9665 9.4665C9.39984 9.03316 9.74358 8.51871 9.97811 7.95252Z"
															         fill="#CFD8DC"
															       />
															       <path
															         d="M13 13.5L9 9.5M10.3333 6.16667C10.3333 6.7795 10.2126 7.38634 9.97811 7.95252C9.74358 8.51871 9.39984 9.03316 8.9665 9.4665C8.53316 9.89984 8.01871 10.2436 7.45252 10.4781C6.88634 10.7126 6.2795 10.8333 5.66667 10.8333C5.05383 10.8333 4.447 10.7126 3.88081 10.4781C3.31462 10.2436 2.80018 9.89984 2.36683 9.4665C1.93349 9.03316 1.58975 8.51871 1.35523 7.95252C1.12071 7.38634 1 6.7795 1 6.16667C1 4.92899 1.49167 3.742 2.36683 2.86683C3.242 1.99167 4.42899 1.5 5.66667 1.5C6.90434 1.5 8.09133 1.99167 8.9665 2.86683C9.84167 3.742 10.3333 4.92899 10.3333 6.16667Z"
															         stroke="#CFD8DC"
															         stroke-width="2"
															         stroke-linecap="round"
															         stroke-linejoin="round"
															       />
															     </svg>
															   </div>
															 </div>
															</div>
													</div>											
													<div className="max-h-[42vh] overflow-y-auto scrollbar-thin">
															<table className="w-full min-w-max table-auto text-left">
															  <thead className="sticky top-0 shadow-none uppercase ">
															    <tr>
															      {TABLE_HEAD1.map((head) => (
															        <th
															          key={head}
															          className="border-b border-blue-gray-100 bg-gray-100 first:px-1 py-2"
															        >
															          <Typography
															            className="text-xs font-semibold"
															          >
															            {head}
															          </Typography>
															        </th>
															      ))}
															    </tr>
															  </thead>
															  <tbody>
															  				{
															  						items
															  				}
															  </tbody>
															</table>
													</div>
														
													</CardBody>
													<CardFooter className="p-0">
															<Typography className="italic text-xs text-center"> {itemCount} </Typography>
													</CardFooter>
											</Card>

											<Card className="min-w-[23vw] max-w-[23vw] border border-2 border-gray-400/20 shadow-none bg-[#E9E9EA]/80">
													<CardBody className="p-0 min-h-[48vh] max-h-[48vh]">
																	
													<div className="flex justify-between">
															<Typography variant="h6" color="green" className="py-2 ml-1">CATEGORIES</Typography>
															<CustomButton
																	label="add category"
																	btn="lowercase"
																	icon={<FaPlus />}
																	variant="text"
																	handleClick={handleAddCategoryDrawer}
																	tooltip="hidden"
															/>
													</div>

													<div className="max-h-[42vh] overflow-y-auto scrollbar-thin">
														<table className="w-full min-w-max table-auto text-left">
														  <thead className="sticky top-0 shadow-none uppercase">
														    <tr>
														      {TABLE_HEAD2.map((head) => (
														        <th
														          key={head}
														          className="border-b border-blue-gray-100 bg-gray-100 first:px-1 py-2"
														        >
														          <Typography
														            className="text-xs font-semibold"
														          >
														            {head}
														          </Typography>
														        </th>
														      ))}
														    </tr>
														  </thead>
														  <tbody>
														  				{categories}
														  </tbody>
														</table>
													</div>
														
													</CardBody>
													<CardFooter className="p-0">
															<Typography className="italic text-xs text-center"> {categories.length} items </Typography>
													</CardFooter>
											</Card>

											<Card className="min-w-[23vw] max-w-[23vw] border border-2 border-gray-400/20 shadow-none bg-[#E9E9EA]/80">
													<CardBody className="p-0 min-h-[48vh] max-h-[48vh]">
																	
													<div className="flex justify-between">
															<Typography variant="h6" color="green" className="py-2 ml-1">STORAGE LOCATIONS</Typography>
															<CustomButton
																	label="add location"
																	icon={<FaPlus />}
																	btn="lowercase"
																	variant="text"
																	handleClick={handleAddLocationDrawer}
																	tooltip="hidden"
															/>
													</div>

													<div className="max-h-[42vh] overflow-y-auto scrollbar-thin">
															<table className="w-full min-w-max table-auto text-left">
															  <thead className="sticky top-0 shadow-none uppercase">
															    <tr>
															      {TABLE_HEAD3.map((head) => (
															        <th
															          key={head}
															          className="border-b border-blue-gray-100 bg-gray-100 first:px-1 py-2"
															        >
															          <Typography
															            className="text-xs font-semibold"
															          >
															            {head}
															          </Typography>
															        </th>
															      ))}
															    </tr>
															  </thead>
															  <tbody>
															  		{rooms}
															  </tbody>
															</table>
													</div>
														
													</CardBody>
													<CardFooter className="p-0">
															<Typography className="italic text-xs text-center"> {rooms.length} items </Typography>
													</CardFooter>
											</Card>

									</div>

									<Card className="w-full min-w-[75.8vw] max-w-[75.8vw] border border-2 border-gray-400/20 shadow-none bg-[#E9E9EA]/80">
						      <CardBody className="p-0 min-h-[18vh] max-h-[18vh] flex flex-row items-center">
						      		<img
						      		  src={imageUrl}
						      		  className="rounded-lg ml-2 max-w-[6rem] max-h-[6rem] "
						      		/>
						        <div className="pl-2">
						        	<Typography variant="h4" color="green" className="leading-5">
						        	  {name.substring(0,12) + "..."}
						        	</Typography>
						        	<Typography color="gray" className="text-sm italic font-normal">
						        	  {address.substring(0,15) + "..."}
						        	</Typography>
						        </div>
						        
						        <div className="pl-14 pr-8 italic">
						        		<Typography variant="h6"> Number of items: {items.length} </Typography>
						        </div>
						        <div className="px-8 italic">
						        		<Typography variant="h6"> Number of categories: {categories.length} </Typography>
						        </div>
						        <div className="px-8 italic">
						        		<Typography variant="h6"> Number of storage locations: {rooms.length} </Typography>
						        </div>

						      </CardBody>
						   </Card>

								</div>

								
					</>
			)
}

export default InventoryCard