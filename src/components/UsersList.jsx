import {
  Card,
  CardHeader,
  CardBody,
  Typography,
  Button,
  Tooltip,
  Input,
  Textarea, CardFooter, Chip, Select, Option
} from "@material-tailwind/react";
import { AiOutlineCopy } from "react-icons/ai";
import { useState, useEffect } from 'react';
import { FaRegEdit } from "react-icons/fa";
import { MdDeleteOutline } from "react-icons/md";
import { InvisibleButton, CustomDialogBox, CustomButton, CustomDrawer } from './';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FaRegCheckCircle } from "react-icons/fa";
import { GrStatusCriticalSmall } from "react-icons/gr";
import { FaBuildingUser } from "react-icons/fa6";
 
const UsersList = ({ 
  userProps,
  fetchData }) => {

  const { _id, name, email, buildingsAssigned, imageUrl, password, isActive, userRole } = userProps;
  const [isCopied, setIsCopied] = useState(false);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [openDrawer2, setOpenDrawer2] = useState(false);
  const [status, setStatus] = useState('');

  const [newName, setNewName] = useState(name);
  const [newEmail, setNewEmail] = useState(email);
  const [newImageUrl, setNewImageUrl] = useState(imageUrl);
  const [isUpdated, setIsUpdated] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [open, setOpen] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [role, setRole] = useState(userRole);
  const [isChanged, setIsChanged] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [isStatusUpdated, setIsStatusUpdated] = useState(false);

  const handleOpen = () => setOpen(!open);
  const handleOpenDialog = () => setOpenDialog(!openDialog);
  const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
  const handleOpenDrawer2 = () => setOpenDrawer2(!openDrawer2);

   const handleCopy = () => {
    navigator.clipboard.writeText(_id);
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 3000);
  };

  const handleUpdate = (event) => {
     event.preventDefault();
     setIsUpdated(true);

     fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/${_id}`, {
        method: "PATCH",
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          newName,
          newEmail,
          newImageUrl
        })
     })
     .then(response => response.json())
     .then(data => {
        if (data === true) {
           toast.success("Successfully Updated!");
           setOpenDrawer(false)
           setIsUpdated(false);
           fetchData();
        } else {
          toast.error("Unable to update user");
          setIsUpdated(false);
          setOpenDrawer(false)
        }
     })
  }

  const Deactivate = (event) => {
     event.preventDefault();
     setIsStatusUpdated(true);
     setIsDisabled(true);
     fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/user-status/${_id}`, {
        method: "PATCH",
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          isActive: false
        })
     })
     .then(response => response.json())
     .then(data => {
        if (data === true) {
           toast.success(`Successfully ${isActive ? "Deactivated!" : "Activated!"}`);
           setOpenDialog(false);
           fetchData();
           setIsStatusUpdated(false);
           setIsDisabled(false);
        } else {
          toast.error("Unable to update user");
          setOpenDialog(false);
          setIsStatusUpdated(false);
          setIsDisabled(false);
        }
     })
  }

  const Activate = (event) => {
     event.preventDefault();
     setIsStatusUpdated(true);
     setIsDisabled(true);
     fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/user-status/${_id}`, {
        method: "PATCH",
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          isActive: true
        })
     })
     .then(response => response.json())
     .then(data => {
        if (data === true) {
           toast.success(`Successfully ${isActive ? "Deactivated!" : "Activated!"}`);
           setOpenDialog(false)
           fetchData();
           setIsStatusUpdated(false);
           setIsDisabled(false);
        } else {
          toast.error("Unable to update user");
          setOpenDialog(false)
          setIsStatusUpdated(false);
          setIsDisabled(false);
        }
     })
  }

  const handleDelete = (_id) => {
    setIsDeleted(true);
      fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/${_id}`, {
            method: "DELETE"
      })
      .then(response => response.json())
      .then(data => {
            if (data === true) {
                  toast.success(`${name} Successfully deleted!`);
                  fetchData();
                  setIsDeleted(false);
                  setOpen(false);
            } else {
                toast.error("Unable to delete user");
                setIsDeleted(false);
                setOpen(false);
            }
      })
  }

  const currentUserRole = (userRole) => {
    switch (userRole) {
      case 0:
        return "Admin";
      case 1:
        return "Lab Manager";
      case 2:
        return "Inventory Clerk/Technician";
      default:
        return "Unknown Role";
    }
  };


  const handleChangeUserRole = (e) => {
    e.preventDefault();
    setIsChanged(true);
    setIsDisabled(true);
    fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/user-role/${_id}`, {
       method: "PATCH",
       headers: {
         'Content-type': 'application/json'
       },
       body: JSON.stringify({
         newUserRole: role
       })
    })
    .then(response => response.json())
    .then(data => {
     console.log(data)
       if (data === true) {
          toast.success(`Successfully Changed User Role!`);
          setOpenDrawer2(false);
          fetchData();
          setIsChanged(false);
          setIsDisabled(false);
       } else {
         toast.error("Unable to change user Role!");
         setOpenDrawer2(false);
         setIsChanged(false);
         setIsDisabled(false);
       }
    })
    .catch(error => console.error(error));
  }

  return (
    <Card className="w-full max-w-[48rem] flex-row group">
      <CardHeader
        shadow={false}
        floated={false}
        className="m-0 w-2/5 shrink-0 rounded-r-none"
      >
        <img
          src={imageUrl}
          className="h-full w-full object-cover"
        />

      </CardHeader>
      <CardBody>
        <Tooltip
          content={
             buildingsAssigned.map(buildingName => {
                return (
                   <div className="flex flex-col justtify-center items-center p-4">
                    <span key={buildingName._id}> Assigned at {buildingName.buildingName} </span>
                    <span> <img src={buildingName.buildingImage} className="mx-auto w-[40%] max-h-[35rem]" /> </span>
                   </div>
                 )
             })
          }
          className="py-0 rounded-md capitalize px-2 text-xs bg-green-600 max-w-[20rem]"
          placement="left"
        >
          <span className="hover:cursor-pointer absolute top-1 right-1 text-sm rounded-full py-0.5 px-2">
            {buildingsAssigned.length > 0 ? (<FaRegCheckCircle color="green" />) : ""}
          </span>
        </Tooltip>
        <Typography variant="h6" color="blue-gray" className="font-bold capitalize">
          {name}
        </Typography>
        <Typography color="gray" className="font-normal italic">
          {email}
        </Typography>
        <Typography color="gray" className="font-normal text-xs">
          {currentUserRole(userRole)}
        </Typography>
        <p className="mt-3">{ isActive ? <Chip color="blue" className="p-0 text-center text-xs" value="Active" variant="gradient" /> : <Chip color="red" className="p-0 text-center text-xs" value="Inactive" variant="ghost" /> }</p>
        {/*<Typography className="text-xs mt-0.5">{_id}</Typography>*/}
        <Tooltip
          content={isCopied ? "copied" : "copy user id"}
          className="py-0 rounded-md uppercase px-2 text-xs"
          placement="right"
        >
          <Button className="p-2 rounded-full mt-4 uppercase hover:bg-gray-300" variant="text" onClick={handleCopy}>
          {isCopied ? 'Copied!' : < AiOutlineCopy size={12} />}
          </Button>
        </Tooltip>

        <div className="flex flex-row gap-1">
          <InvisibleButton 
            label={ isActive ? "Deactivate" : "Activate" }
            icon={<GrStatusCriticalSmall className="w-4 h-4" />}
            tooltip="!bg-dark"
            placement="top"
            variant="text"
            handleClick={handleOpenDialog}
          />
          <InvisibleButton 
            label="Change User Role"
            icon={<FaBuildingUser className="w-4 h-4" />}
            tooltip="!bg-cyan-600"
            buttonClass="!text-cyan-600"
            color="cyan"
            placement="top"
            variant="text"
            handleClick={handleOpenDrawer2}
          />
          <InvisibleButton 
            label="Edit"
            icon={<FaRegEdit className="w-4 h-4" />}
            tooltip="!bg-green-600"
            buttonClass="!text-green-600"
            color="green"
            placement="top"
            variant="text"
            handleClick={handleOpenDrawer}
          />
          <InvisibleButton 
            label="Delete"
            icon={<MdDeleteOutline className="w-4 h-4" />}
            tooltip="!bg-red-600"
            buttonClass="!text-red-600"
            color="red"
            placement="top"
            variant="text"
            handleClick={handleOpen}
          />
        </div>
      </CardBody>


       <CustomDialogBox
         open={open}
         handleOpen={handleOpen}
         title="Confirmation"
       >

         Delete {name}?

       <div className="flex flex-row justify-center gap-2 mt-8">
         <CustomButton
           label="Cancel"
           tooltip="hidden"
           handleClick={handleOpen}
         />
         <CustomButton
           label={isDeleted ? "Deleting..." : "Delete"}
           color="red"
           tooltip="hidden"
           handleClick={() => handleDelete(_id)}
           isDisabled={isDeleted ? true : false}
         />
       </div>

       </CustomDialogBox>

       <CustomDialogBox
         open={openDialog}
         handleOpen={handleOpenDialog}
         title="Confirmation"
       >

         { isActive ? "Deactivate" : "Activate"} this user?

       <div className="flex flex-row justify-center gap-2 mt-8">
         <CustomButton
           label="Cancel"
           tooltip="hidden"
           handleClick={handleOpenDialog}
         />
         <CustomButton
           label="Confirm"
           color="green"
           tooltip="hidden"
           isDisabled={isDisabled}
           handleClick={ isActive ? Deactivate : Activate }
           loading={isStatusUpdated ? true : false}
         />
       </div>

       </CustomDialogBox>

       <CustomDrawer
         title={`Update ${name}`}
         open={openDrawer}
         handleOpenDrawer={handleOpenDrawer}
         handleCloseDrawer={handleOpenDrawer}
       >
         <form className="flex flex-col gap-3 p-5" onSubmit={handleUpdate}>
             <Input
               label="Name"
               value={newName}
               onChange={(e) => setNewName(e.target.value)}
               color="teal"
               className="capitalize"
             />
             <Input
               label="Email"
               value={newEmail}
               onChange={(e) => setNewEmail(e.target.value)}
               color="teal"
             />
             <Textarea
               label="Image Url"
               value={newImageUrl}
               onChange={(e) => setNewImageUrl(e.target.value)}
               color="teal"
               className="text-xs min-h-[70px]"
             />
             <img
               src={newImageUrl}
               className="w-20 mx-auto"
             />

             <div className="flex flex-row justify-center gap-2 mt-6">
               <CustomButton
                 handleClick={handleOpenDrawer}
                 label="Cancel"
                 tooltip="hidden"

               />
               <CustomButton
                 type="submit"
                 label={!isUpdated ? "Update" : "Updating..."}
                 tooltip="hidden"
                 loading={isUpdated ? true : false}
                 isDisabled={isUpdated ? true : false}
                 color="green"
               />
             </div>
         </form>
       </CustomDrawer>

       <CustomDrawer
         title="Change User Role"
         open={openDrawer2}
         handleOpenDrawer={handleOpenDrawer2}
         handleCloseDrawer={handleOpenDrawer2}
       >

         <div className="px-3 py-5 flex flex-col gap-6 justify-center">
          <Select label={`Current: ${currentUserRole(userRole)}`} onChange={(e) => setRole(e)} >
            <Option value="0" disabled>Admininistrator</Option>
            <Option value="1">Lab Manager</Option>
            <Option value="2">Inventory Clerk/Technician</Option>
          </Select>

          <div className="">
           <CustomButton
             label={!isChanged ? "Save" : "Saving ..."}
             loading={isChanged ? true : false}
             isDisabled={isDisabled}
             tooltip="hidden"
             btn="mx-auto"
             handleClick={handleChangeUserRole}
           />
          </div>

         </div>

       </CustomDrawer>

    </Card>
  );
}

export default UsersList