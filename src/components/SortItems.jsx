import DataContext from '../DataContext';
import { useState, useEffect, useContext } from 'react';
import { Card, CardBody, Typography, CardFooter } from '@material-tailwind/react';
import { FaRegArrowAltCircleRight } from "react-icons/fa";
import { Link } from 'react-router-dom';
import { InvisibleButton, CustomButton } from './';
import { MdDeleteOutline } from "react-icons/md";
import { IoMdEye } from "react-icons/io";
import { FaRegEdit } from "react-icons/fa";

const SortItems = ({ props, count }) => {

 const { itemName, quantity, imageUrl, lowStockThreshold, description, location, category } = props;
   return (
     <Card className="shadow-none rounded-sm group hover:scale-105 bg-[#E9E9EA]/80">
       <CardBody className="flex flex-col w-full h-full gap-1">
         <Typography className="absolute left-1 top-1 rounded-full px-1 py-0.2 text-light bg-gray-600 text-xs hidden"> {count} </Typography>
         <div className="absolute">
           <img src={imageUrl} className="text-green-300 w-14 h-14 shadow-lg border border-1 border-blue-gray-300 rounded-sm p-1" />
         </div>
         <div className="flex flex-col text-left ml-16 mt-3 capitalize"> 
          <Typography className="text-xs font-normal">{category}</Typography>
          <Typography className="text-xs font-normal">{location}</Typography>
         </div>
         <Typography className="text-left mt-3 uppercase text-sm tracking-wider font-normal">{itemName}</Typography>
         <Typography className="absolute right-2 top-1 uppercase text-sm font-normal">{quantity}</Typography>
         
       </CardBody>
       
     </Card>
   )
 }  

export default SortItems