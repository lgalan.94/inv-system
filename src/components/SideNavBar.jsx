import { Card, CardBody, Typography } from "@material-tailwind/react";
import { Navlinks, AdminNavlinks, ClerkNavlinks } from './';
import { useNavigate } from 'react-router-dom';
import DataContext from '../DataContext';
import { useContext } from 'react';
 
export default function SideNavBar() {

  const navigate = useNavigate();

  const { data, building } = useContext(DataContext);
 
  return (
  <>
    <Card variant="gradient" color="transparent" className={`h-full shadow-none w-full max-w-[18rem] p-1 rounded-none`}>
      <div className={`capitalize max-h-[20rem] ${data.userRole === 0 ? "p-4" : "p-1"}`}>
        <Typography variant="h5" color="blue-gray">
          {
            data.userRole === 0 ? <AdminNavlinks /> : data.userRole === 1 ? <Navlinks /> : <ClerkNavlinks />
          }
        </Typography>
      </div>
      {/*{
        data.userRole === 0 ? "" : (
              <div className="justify-center items-center mt-6">
                <Card className="min-w-[16rem] mx-auto max-w-[16rem] shadow-none border border-2 bg-[#E9E9EA]/60">
                    <CardBody className="flex flex-col justify-center text-center gap-1">
                        <img src={building.buildingImage} className="rounded-lg max-h-[8rem] w-[70%] mx-auto shadow mb-1" />
                        <Typography className="mx-auto uppercase text-md tracking-wide font-bold leading-4">{building.buildingName}</Typography>
                    </CardBody>
                </Card>
              </div>
          )
      }*/}
    </Card>
  </>
      
  );
}