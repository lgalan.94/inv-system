import { MdDeleteOutline } from "react-icons/md";
import { IoMdEye } from "react-icons/io";
import { FaRegEdit } from "react-icons/fa";
import { InvisibleButton, CustomDialogBox, CustomButton, CustomDrawer } from '../';
import { useState, useEffect, useContext } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Input, Typography, Textarea, Select, Option } from '@material-tailwind/react';
import DataContext from '../../DataContext';

const ExperimentCard = ({ props, fetchData, count }) => {

	const { building } = useContext(DataContext);
	const { _id, title, description, startDate, endDate, status, results, researcher, equipmentsUsed } = props;
	const [isDelete, setIsDelete] = useState(false);
	const [isUpdated, setIsUpdated] = useState(false);
	const [open, setOpen] = useState(false);
	const [openDrawerView, setOpenDrawerView] = useState(false);
	const [openDrawerUpdate, setOpenDrawerUpdate] = useState(false);
	const [isDisabled, setIsDisabled] = useState(true);
	// const [newSampleName, setNewSampleName] = useState(sampleName);
	// const [newLocation, setNewLocation] = useState(location);
	// const [newDescription, setNewDescription] = useState(description);

	const handleDrawerView = () => setOpenDrawerView(!openDrawerView);
	const handleDrawerUpdate = () => {
			setOpenDrawerView(false);
			setOpenDrawerUpdate(!openDrawerUpdate)
	}
	const handleOpen = () => setOpen(!open);

	/*const handleDelete = (_id) => {
			setIsDelete(true);
			fetch(`${import.meta.env.VITE_API_URL}/ulafis/sample/${building.buildingId}/remove/${_id}`, {
						method: 'PATCH',
						headers: {
								'Content-type': 'application/json'
						}
			})
			.then(response => response.json())
			.then(data => {
						if (data === true) {
									toast.success(`${sampleName.toUpperCase()} Successfully Deleted!`);
									fetchData();
									setTimeout(() => {
											setIsDelete(false)
											setOpen(false)
									}, 3000)
						}
						else
						{
								toast.error('Cannot remove item!');
								setOpen(false);
						}
			})
			.catch(error => console.error(error))
	}*/

	/*let updatedData = {
			newSampleName,
			newDescription,
			newLocation
	}*/

	/*const clearData = () => {
			setNewSampleName("");
			setNewLocation("");
			setNewDescription("");
	}*/

	/*useEffect(() => {	
			newSampleName !== sampleName ||
			newDescription !== description ||
			newLocation !== location ?
			setIsDisabled(false) : setIsDisabled(true)
	}, [newSampleName, newDescription, newLocation])*/

	/*const handleUpdate = () => {
			setIsDisabled(true);
			setIsUpdated(true);
			fetch(`${import.meta.env.VITE_API_URL}/ulafis/sample/${building.buildingId}/update/${_id}`, {
					method: "PATCH",
					headers: {
						'Content-type': 'application/json'
					},
					body: JSON.stringify(updatedData)
			})
			.then(response => response.json())
			.then(data => {
						if (data === true) {
								toast.success("Successfully Updated!")
								setOpenDrawerUpdate(false);
								fetchData();
								clearData();
								setIsUpdated(false);
						} else {
								toast.error("Unable to update data!");
								setOpenDrawerUpdate(false);
								setIsUpdated(false);
						}

			})
			.catch(error => console.error)
	}*/


	/*const [storageLocations, setStorageLocations] = useState([]);

	const fetchStorageLocations = () => {
	  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
	    method: "GET"
	  })
	  .then(response => response.json())
	  .then(result => {
	    	setStorageLocations(result.samplesStorage);
	  })
	  .catch(error => console.error);
	}

	useEffect(() => {
			fetchStorageLocations();
	}, [])*/

		return (
						<>
								
									<tr className="hover:bg-blue-gray-300/30 group text-sm">
									  <td className="">
									    <Typography className="ml-1 text-xs font-semibold" >{count}</Typography>
									  </td>
									  <td className="">
									    <Typography className="font-semibold text-sm">{title.toUpperCase().substring(0, 25) + ""}</Typography>
									  </td>
									  <td className="">
									    {status}
									  </td>
									  <td className="">
									    {description.substring(0, 50) + "..."}
									  </td>
									  <td className="border-b border-blue-gray-50">
									    <div className="flex flex-row justify-end mr-3">
									      <div className="mr-2">
									      	<InvisibleButton 
									      	  label="view"
									      	  icon={<IoMdEye className="w-4 h-4" />}
									      	  color="cyan"
									      	  handleClick={handleDrawerView}
									      	/>
									      </div>
									      <div className="">
									      	<InvisibleButton 
									        label="edit"
									        icon={<FaRegEdit className="w-4 h-4" />}
									        tooltip="!bg-green-600"
									        buttonClass="!text-green-600"
									        color="green"
									        handleClick={handleDrawerUpdate}
									      />
									      </div>
									      <div className="ml-2">
								      		<InvisibleButton 
								      		  label="delete"
								      		  icon={<MdDeleteOutline className="w-4 h-4" />}
								      		  tooltip="!bg-red-600"
								      		  buttonClass="!text-red-600"
								      		  color="red"
								      		  handleClick={handleOpen}
								      		/>
									      </div>
									    </div>
									  </td>
									</tr>		

							{/*<CustomDialogBox
									title="Confirmation"
									open={open}
									handleOpen={handleOpen}
							>

								Delete {sampleName.toUpperCase()}?

								<CustomButton
										handleClick={() => handleDelete(_id)}
										label="Delete"
										loading={ isDelete ? true : false }
										tooltip="hidden"
										color="red"
										btn="mx-auto mt-10"
								/>

							</CustomDialogBox>*/}
							
							{/*<CustomDrawer
									title="Update"
									open={openDrawerUpdate}
									handleOpenDrawer={handleDrawerUpdate}
							>
									<div className="py-10 px-5">
										<form className="flex flex-col gap-7">
												<div className="flex flex-col gap-2">
												  <Input
												    type="text"
												    className="capitalize"
												    value={newSampleName}
												    onChange={(e) => setNewSampleName(e.target.value)}
												    label="Sample Name"
												    color="teal"
												  />
												  <Select onChange={(e) => setNewLocation(e)} label={`Current: ${location}`} >
												  		{
												  				storageLocations.map((storage) => (
												  							<Option key={storage._id} value={storage.storageName} > {storage.storageName} </Option>
												  					))
												  		}
												  </Select>
												  <Textarea
												  		value={newDescription}
												  		onChange={(e) => setNewDescription(e.target.value)}
												    label="Description"
												    color="teal"
												  />
												  
												</div>
												<CustomButton
														label={!isUpdated ? "Update" : "Updating..."}
														tooltip="hidden"
														color="green"
														loading={isUpdated ? true : false}
														isDisabled={isDisabled}
														handleClick={handleUpdate}
												/>
										</form>
									</div>

							</CustomDrawer>*/}

								{/*<CustomDrawer
										title={sampleName}
										open={openDrawerView}
										handleOpenDrawer={handleDrawerView}
								>
										<div className="py-10 px-5 flex flex-col gap-3">
										
											<div className="flex flex-col">
											  <Typography className="font-normal text-sm uppercase text-dark/50">Description</Typography>
											  <Textarea
											  		value={description}
											  		disabled
											  />
											</div>

											<div className="flex flex-col">
											  <Typography className="font-normal text-sm uppercase text-dark/50">Location</Typography>
											  <Textarea
											  		value={location}
											  		disabled
											  />
											</div>

											<CustomButton
													handleClick={handleDrawerUpdate}
													label="edit"
													tooltip="hidden"
													color="green"
													btn="mx-auto mt-8"
											/>
										</div>

								</CustomDrawer>*/}

						</>
			)
}

export default ExperimentCard