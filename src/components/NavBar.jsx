import React from 'react'
import { Navbar, Typography, Button, IconButton, Drawer, Menu, MenuHandler, MenuList, MenuItem, Avatar } from "@material-tailwind/react";
import { useNavigate } from 'react-router-dom';
import { Navlinks } from './'
import { useContext } from 'react';
import { FaChevronDown } from "react-icons/fa";
import { HiOutlineLogout } from "react-icons/hi"; 
import DataContext from '../DataContext';
import { RiRefreshFill } from "react-icons/ri";
 
const NavBar = () => {

  const { data, setData, unsetData, building, setBuilding, settings } = useContext(DataContext);
  const [openMenu, setOpenMenu] = React.useState(false);
  const [openMenu2, setOpenMenu2] = React.useState(false);
  const [openNav, setOpenNav] = React.useState(false);
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const openDrawer = () => setOpen(true);
  const closeDrawer = () => setOpen(false);

  const handleLogout = () => {
      unsetData();
      setData({
        id: null,
        name: null,
        email: null,
        userRole: null,
        imageUrl: null,
        buildingsAssigned: []
      });
      setBuilding({
        buildingId: null,
        buildingName: null
      })
      navigate('/')
  }

  function ClockIcon() {
    return (
      <svg
        width="16"
        height="17"
        viewBox="0 0 16 17"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M7.99998 14.9C9.69736 14.9 11.3252 14.2257 12.5255 13.0255C13.7257 11.8252 14.4 10.1974 14.4 8.49998C14.4 6.80259 13.7257 5.17472 12.5255 3.97449C11.3252 2.77426 9.69736 2.09998 7.99998 2.09998C6.30259 2.09998 4.67472 2.77426 3.47449 3.97449C2.27426 5.17472 1.59998 6.80259 1.59998 8.49998C1.59998 10.1974 2.27426 11.8252 3.47449 13.0255C4.67472 14.2257 6.30259 14.9 7.99998 14.9ZM8.79998 5.29998C8.79998 5.0878 8.71569 4.88432 8.56566 4.73429C8.41563 4.58426 8.21215 4.49998 7.99998 4.49998C7.7878 4.49998 7.58432 4.58426 7.43429 4.73429C7.28426 4.88432 7.19998 5.0878 7.19998 5.29998V8.49998C7.20002 8.71213 7.28434 8.91558 7.43438 9.06558L9.69678 11.3288C9.7711 11.4031 9.85934 11.4621 9.95646 11.5023C10.0536 11.5425 10.1577 11.5632 10.2628 11.5632C10.3679 11.5632 10.472 11.5425 10.5691 11.5023C10.6662 11.4621 10.7544 11.4031 10.8288 11.3288C10.9031 11.2544 10.9621 11.1662 11.0023 11.0691C11.0425 10.972 11.0632 10.8679 11.0632 10.7628C11.0632 10.6577 11.0425 10.5536 11.0023 10.4565C10.9621 10.3593 10.9031 10.2711 10.8288 10.1968L8.79998 8.16878V5.29998Z"
          fill="#90A4AE"
        />
      </svg>
    );
  }
 
  return (
    <>
    <div className="sticky top-0 z-10 max-h-[768px] w-full ">
      <Navbar className="z-10 h-max max-w-full rounded-none px-4 py-1 lg:px-8 shadow-lg">
        <div className="flex items-center justify-between text-blue-gray-900">
          <Typography
            className="mr-4 cursor-pointer py-1.5"
          >
            <span className="font-bold font-ibm uppercase tracking-wide"> {settings.settingsName !== null ? settings.settingsName : <RiRefreshFill className="w-5 text-gray-600 h-5 animate-spin" />} </span> 
          </Typography>

          <div className="flex items-center gap-4">
          {/*<div className="flex flex-col hover:scale-105">
            <span className="absolute inline-flex items-center rounded-full bg-red-500 px-[4px] py-[1px] text-white text-[8px] font-bold ml-5 z-10 mt-1">
              1
            </span>
              <Menu open={openMenu2} handler={setOpenMenu2} allowHover>
                <MenuHandler>
                  <IconButton variant="text">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="h-5 w-5"
                    >
                      <path
                        fillRule="evenodd"
                        d="M5.25 9a6.75 6.75 0 0113.5 0v.75c0 2.123.8 4.057 2.118 5.52a.75.75 0 01-.297 1.206c-1.544.57-3.16.99-4.831 1.243a3.75 3.75 0 11-7.48 0 24.585 24.585 0 01-4.831-1.244.75.75 0 01-.298-1.205A8.217 8.217 0 005.25 9.75V9zm4.502 8.9a2.25 2.25 0 104.496 0 25.057 25.057 0 01-4.496 0z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </IconButton>
                </MenuHandler>
                <MenuList className="flex flex-col gap-2">
                  <MenuItem onClick={() => navigate(`/lab-manager/${building.buildingId}/notifications`)} className="flex items-center gap-4 py-2 pl-2 pr-8">
                    <Avatar
                      variant="circular"
                      alt="tania andrew"
                      src="https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1480&q=80"
                    />
                    <div className="flex flex-col gap-1">
                      <Typography variant="small" color="gray" className="font-semibold">
                        Tania send you a message
                      </Typography>
                      <Typography className="flex items-center gap-1 text-sm font-medium text-blue-gray-500">
                        <ClockIcon />
                        13 minutes ago
                      </Typography>
                    </div>
                  </MenuItem>
                </MenuList>
              </Menu>
          </div>*/}
            <div className="flex items-center gap-x-1">
               <Menu open={openMenu} handler={setOpenMenu} allowHover>
                 <MenuHandler>
                   <Button
                     variant="text"
                     className="flex py-0 px-1 items-center gap-2 text-xs font-leading uppercase tracking-normal"
                   >
                     <span className=""><img src={data.imageUrl} className="border border-blue-gray-300 w-10 max-h-10 rounded-full" /></span> {data.name} {" "}
                     <FaChevronDown
                       strokeWidth={2}
                       className={`h-3 w-3 transition-transform ${
                         openMenu ? "rotate-180" : ""
                       }`}
                     />
                   </Button>
                 </MenuHandler>
                 <MenuList>
                   <MenuItem onClick={() => navigate('/manage-profile')}  className="font-ibm flex flex-row items-center justify-center text-xs tracking-wide gap-1 uppercase">Manage Profile</MenuItem>
                   <hr className="my-3" />
                   <MenuItem onClick={handleLogout}  className="font-ibm flex flex-row items-center justify-center text-xs tracking-wide gap-1 uppercase"><HiOutlineLogout className="w-4 h-4" /> Logout</MenuItem>
                 </MenuList>
               </Menu>
            </div>
            <IconButton
              variant="text"
              className="ml-auto h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent lg:hidden"
              ripple={false}
              onClick={openDrawer}
            >
              {openNav ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  className="h-6 w-6"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M4 6h16M4 12h16M4 18h16"
                  />
                </svg>
              )}
            </IconButton>
          </div>
        </div>
        
      </Navbar>
    </div>

    <React.Fragment>
      <Drawer placement="right" open={open} onClose={closeDrawer}>
        <div className="mb-2 flex capitalize items-center justify-between p-4">
          <IconButton variant="text" color="blue-gray" onClick={closeDrawer}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={2}
              stroke="currentColor"
              className="h-5 w-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </IconButton>
        </div>
        
        <Navlinks />
        
      </Drawer>
    </React.Fragment>

    </>

  );
}

export default NavBar