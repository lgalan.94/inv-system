import { CustomButton, InvisibleButton, CustomDrawer, CustomDialogBox } from './';
import { useState, useEffect } from 'react';
import { Typography } from '@material-tailwind/react';
import { MdDeleteOutline } from "react-icons/md";
import { IoMdEye } from "react-icons/io";
import { FaRegEdit } from "react-icons/fa";
import { Tooltip, Input, Select, Textarea, Option } from '@material-tailwind/react';
import { useNavigate } from 'react-router-dom';
import { IoArrowBack } from "react-icons/io5";
import { LuSaveAll } from "react-icons/lu";
import { RiRefreshFill } from "react-icons/ri";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const InventoryItemCard = ({ itemProps }) => {
 
	const { _id, imageUrl, itemName, description, category, quantity, lowStockThreshold, location } = itemProps;

	const [categories, setCategories] = useState([]);

	const [newName, setItemName] = useState(name);
	const [newDescription, setItemDescription] = useState(description);
	const [newQuantity, setItemQuantity] = useState(quantity);
	const [newLocation, setItemLocation] = useState(location);
	const [newCategory, setItemCategory] = useState(category);
	const [newImage_url, setItemImageUrl] = useState(imageUrl);
	const [newLow_stock_threshold, setNew_low_stock_threshold] = useState(lowStockThreshold);

	const [descriptionAlert, setDescriptionAlert] = useState(false);
	const [quantityAlert, setQuantityAlert] = useState(false);
	const [lowStockAlert, setLowStockAlert] = useState(false);
	const [locationAlert, setLocationAlert] = useState(false);
	const [categoryAlert, setCategoryAlert] = useState(false);
	const [imageUrlAlert, setImageUrlAlert] = useState(false);
	const [thresholdAlert, setThresholdAlert] = useState(false);
	const [errorNameInput, setErrorNameInput] = useState(false);
	const [errorQuantityInput, setErrorQuantityInput] = useState(false);
	const [errorLSTInput, setErrorLSTInput] = useState(false);
	const [errorCategorySelect, setErrorCategorySelect] = useState(false);
	const [errorLocationInput, setErrorLocationInput] = useState(false);
	const [errorImageUrlInput, setErrorImageUrlInput] = useState(false);
	const [errorDescriptionInput, setErrorDescriptionInput] = useState(false);

	const [openDrawer, setOpenDrawer] = useState(false);
	const [openDrawerDelete, setOpenDrawerDelete] = useState(false);
	const [openDrawerUpdate, setOpenDrawerUpdate] = useState(false);
	const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
	const handleOpenDrawerDelete = () => setOpenDrawerDelete(!openDrawerDelete);
	const handleOpenDrawerUpdate = () => {
			setOpenDrawer(false);
			setOpenDrawerUpdate(!openDrawerUpdate);
	}
	const handleCloseDrawer = () => setOpenDrawer(false);
	const handleCloseDrawerDelete = () => setOpenDrawerDelete(false);
	const handleCloseDrawerUpdate = () => setOpenDrawerUpdate(false);
	const [isDeleteClicked, setIsDeleteClicked] = useState(false);
	const [isUpdateClicked, setIsUpdateClicked] = useState(false);

	/*Handle Delete*/
			const handleDelete = (e) => {
			   e.preventDefault();
			   setIsDeleteClicked(true);
			   fetch(`${import.meta.env.VITE_API_URL}/ulafis/items/${_id}`, {
			     method: "DELETE",
			     headers: {
			       'Content-type': 'application/json'
			     }
			   })
			   .then(result => result.json())
			   .then(response => {
			     if (response === true) {
			        toast.success(`${name} Successfully Deleted!`);
			        fetchData();
			        setTimeout(() => setOpenDrawerDelete(false), 4000)
			        setIsDeleteClicked(false);
			     } else {
			       toast.error('Cannot delete item')
			       setOpenDrawerDelete(false);
			       setIsDeleteClicked(false);
			     }
			   })
			}
	/**/

	/*Handle Update*/

			const updatedData = {
					newName: newName,
					newDescription: newDescription,
					newQuantity: newQuantity,
					newLocation: newLocation,
					newCategory: newCategory,
					newImageUrl: newImage_url,
					newLowStockThreshold: newLow_stock_threshold
			}

			const handleUpdate = (e) => {
					e.preventDefault();
					setIsUpdateClicked(true);
					fetch(`${import.meta.env.VITE_API_URL}/ulafis/items/${_id}`, {
							method: "PATCH",
							headers: {
									'Content-type': 'application/json'
							},
							body: JSON.stringify(updatedData)
					})
					.then(response => response.json())
					.then(data => {
								if (data === true) {
											toast.success(`${name} Successfully Updated! `);
											setTimeout(() => setOpenDrawerUpdate(false), 4000);
											fetchData();
											setIsUpdateClicked(false);
								} else {
											toast.error("Error occured! Try again!")
											setOpenDrawerUpdate(false);
											setIsUpdateClicked(false);
								}
					})

			}
			
	/**/

	/*handle update alerts*/
			useEffect(() => {
						if (newName.length >= 1) {
									setNameAlert(false)
									setErrorNameInput(false)
						}
			}, [newName])
			useEffect(() => {
						if (newDescription.length >= 1) {
									setDescriptionAlert(false)
									setErrorDescriptionInput(false)
						}
			}, [newDescription])
			useEffect(() => {
						if (newLow_stock_threshold.length >= 1) {
									setLowStockAlert(false)
									setErrorLSTInput(false)
						}
			}, [newLow_stock_threshold])
			useEffect(() => {
						if (newLocation.length >= 1) {
									setLocationAlert(false)
									setErrorLocationInput(false)
						}
			}, [newLocation])
			useEffect(() => {
						if (newCategory !== undefined) {
									setCategoryAlert(false)
									setErrorCategorySelect(false)
						}
			}, [newCategory])
			useEffect(() => {
						if (newImage_url.length >= 1) {
									setImageUrlAlert(false)
									setErrorImageUrlInput(false)
						}
			}, [newImage_url])
			useEffect(() => {
						if (newQuantity.length >= 1) {
									setQuantityAlert(false)
									setErrorQuantityInput(false)
						}
			}, [newQuantity])

			const handleNewNameChange = (e) => {
				setItemName(e.target.value);
			}
			const handleNewDescriptionChange = (e) => {
				setItemDescription(e.target.value);
			}
			const handleNewQuantityChange = (e) => {
				setItemQuantity(e.target.value);
			}
			const handleNewLocationChange = (e) => {
				setItemLocation(e.target.value);
			}
			const handleNewCategoryChange = (e) => {
				setItemCategory(e);
			}
			const handleNewImageUrlChange = (e) => {
				setItemImageUrl(e.target.value);
			}
			const handleNewLowStockThreshold = (e) => {
				setNew_low_stock_threshold(e.target.value);
			}
	/**/

			return (
						<>
								
								{/*Handle View Item Drawer*/}
										<CustomDrawer
												title={itemName.toUpperCase()}
												open={openDrawer}
												handleOpenDrawer={handleOpenDrawer}
												handleCloseDrawer={handleCloseDrawer}
												className={ openDrawer ? "min-w-[50vw]" : "" }
										>

										<div className="flex flex-col gap-4">
												<img className="border border-dark/10 max-w-[78%] mx-auto" src={imageUrl} />
												<div className="px-20 flex flex-col gap-3">
													<div className="flex flex-row">
													  <div className="basis-1/5">
													    <Typography className="font-normal text-sm text-dark/50">Description:</Typography>
													  </div>
													  <div className="basis-4/5">
													    <Typography className="font-normal text-sm">{description}</Typography>
													  </div>
													</div>

													<div className="flex flex-row">
													  <div className="basis-1/5">
													    <Typography className="font-normal text-sm text-dark/50">Quantity/Stock:</Typography>
													  </div>
													  <div className="basis-4/5">
													    <Typography className="font-normal text-sm">{quantity}</Typography>
													  </div>
													</div>

													<div className="flex flex-row">
													  <div className="basis-1/5">
													    <Typography className="font-normal text-sm text-dark/50">LS Threshold:</Typography>
													  </div>
													  <div className="basis-4/5">
													    <Typography className="font-normal text-sm">{lowStockThreshold}</Typography>
													  </div>
													</div>

													<div className="flex flex-row">
													  <div className="basis-1/5">
													    <Typography className="font-normal text-sm text-dark/50">Category:</Typography>
													  </div>
													  <div className="basis-4/5">
													    <Typography className="font-normal text-sm">{category}</Typography>
													  </div>
													</div>

													<div className="flex flex-row">
													  <div className="basis-1/5">
													    <Typography className="font-normal text-sm text-dark/50">Location:</Typography>
													  </div>
													  <div className="basis-4/5">
													    <Typography className="font-normal text-sm">{location}</Typography>
													  </div>
													</div>

												</div>
										</div>

										
										<div className="flex flex-row gap-2 mt-10 justify-center">
											<CustomButton
													label="Close"
													tooltip="hidden"
													handleClick={handleCloseDrawer}
											/>
											<CustomButton
													label="Edit"
													tooltip="hidden"
													handleClick={handleOpenDrawerUpdate}
													color="green"
											/>
											<CustomButton
													label="Delete"
													tooltip="hidden"
													handleClick={handleOpenDrawerDelete}
													color="red"
											/>
										</div>

										</CustomDrawer>
								{/**/}

								{/*Handle edit/update item drawer*/}
										<CustomDrawer
												title={`Update ${name.toUpperCase()}`}
												open={openDrawerUpdate}
												handleOpenDrawer={handleOpenDrawerUpdate}
												handleCloseDrawer={handleCloseDrawerUpdate}
												className={ openDrawerUpdate ? "min-w-[50vw]" : "" }
										>

											
												{/*handle update form*/}
														<div className="p-5"	>
																<form className="flex flex-col gap-2">
																 
																  <div className="">
																    <Input
																      type="text"
																      size="sm"
																      value={newName.toUpperCase()}
																      onChange={handleNewNameChange}
																      className={`capitalize`}
																      label="Name"
																      color="teal"
																      error={errorNameInput}
																    />
																  </div>
																  
																  <div className="">
																    <Input
																      type="number"
																      size="sm"
																      value={newQuantity}
																      onChange={handleNewQuantityChange}
																      className={`capitalize`}
																      label="Quantity"
																      color="teal"
																      error={errorQuantityInput}
																    />
																  </div>

																  <div className="">
																    <Input
																      type="number"
																      size="sm"
																      value={newLow_stock_threshold}
																      onChange={handleNewLowStockThreshold}
																      className={`capitalize`}
																      label="Low Stock Threshold"
																      color="teal"
																      error={errorLSTInput}
																    />
																  </div>

																  <div className="w-full">
																     <Select error={errorCategorySelect} color="teal" onChange={handleNewCategoryChange} label={newCategory}>
																        {categories}
																     </Select>
																  </div>

																  
																  <div className="">
																    <Input
																      type="text"
																      size="sm"
																      value={newLocation}
																      onChange={handleNewLocationChange}
																      className={`capitalize`}
																      label="Location"
																      color="teal"
																      error={errorLocationInput}
																    />
																  </div> 
																  <div className="w-full">
																    <Textarea 
																      label="Description"
																      value={newDescription} 
																      onChange={handleNewDescriptionChange}
																      rows={2}
																      resize={true}
																      className="min-h-[40px]"
																      color="teal"
																      error={errorDescriptionInput}
																    />
																  </div>
																  <div className="-mt-2 w-full">
																    <Textarea 
																      label="Image Url"
																      value={newImage_url}
																      onChange={handleNewImageUrlChange}
																      rows={2}
																      resize={true}
																      className="min-h-[60px] text-xs scrollbar-thin"
																      color="teal"
																      error={errorImageUrlInput}
																    />
																  </div>
																  <div>
																    <img className="m-auto shadow-md w-[65%]" src={newImage_url} />
																  </div>
																</form>
														</div>
														<div className="flex flex-row gap-2 mt-10 justify-center">
															<CustomButton
																	label="Close"
																	tooltip="hidden"
																	handleClick={handleCloseDrawerUpdate}
															/>
															<CustomButton
																	label="Update"
																	tooltip="hidden"
																	handleClick={handleUpdate}
																	color="cyan"
																	loading={isUpdateClicked ? true : false}
																	isDisabled={isUpdateClicked ? true : false}
															/>
														</div>
												{/**/}
											
											
										</CustomDrawer>
								{/**/}

								<CustomDialogBox
									title="Confirmation"
									open={openDrawerDelete}
									handler={handleOpenDrawerDelete}
								>

										<div className="flex items-center justify-center min-h-[30vh]">
												Delete {name}?
										</div>
										<div className="flex flex-row gap-2 mt-10 justify-center">
											<CustomButton
													label="Close"
													tooltip="hidden"
													handleClick={handleCloseDrawerDelete}
											/>
											<CustomButton
													label="Delete"
													tooltip="hidden"
													handleClick={handleDelete}
													color="red"
													loading={isDeleteClicked ? true : false}
													isDisabled={isDeleteClicked ? true : false}
											/>
										</div>

								</CustomDialogBox>

								<tr className="hover:bg-blue-gray-300/30 group text-xs">
								  <td className="border-b border-blue-gray-50">
								    <img className="w-9 border border-dark/10 ml-1 shadow shadow-sm h-9 rounded-full" src={imageUrl} />
								  </td>
								  <td className="border-b border-blue-gray-50">
								    <Typography className="text-md" >{itemName.toUpperCase().substring(0, 25) + ""}</Typography>
								  </td>
								  <td className="border-b border-blue-gray-50">
								    {category}
								  </td>
								  <td className="border-b border-blue-gray-50">
								    {description.substring(0, 40) + "..."}
								  </td>
								  <td className="border-b border-blue-gray-50">
								    <div className="flex flex-row justify-end mr-3">
								      <div className="mr-2">
								      	<InvisibleButton 
								      	  label="view"
								      	  icon={<IoMdEye className="w-4 h-4" />}
								      	  color="cyan"
								      	  handleClick={handleOpenDrawer}
								      	/>
								      </div>
								      <div className="">
								      	<InvisibleButton 
								        label="edit"
								        icon={<FaRegEdit className="w-4 h-4" />}
								        tooltip="!bg-green-600"
								        buttonClass="!text-green-600"
								        color="green"
								        handleClick={handleOpenDrawerUpdate}
								      />
								      </div>
								      <div className="ml-2">
							      		<InvisibleButton 
							      		  label="delete"
							      		  icon={<MdDeleteOutline className="w-4 h-4" />}
							      		  tooltip="!bg-red-600"
							      		  buttonClass="!text-red-600"
							      		  color="red"
							      		  handleClick={handleOpenDrawerDelete}
							      		/>
								      </div>
								    </div>
								  </td>
								</tr>
						</>
			)
}

export default InventoryItemCard