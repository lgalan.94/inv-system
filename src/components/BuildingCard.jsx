import { Typography, Input, Card, CardBody, CardHeader, Textarea, Tooltip, Menu, MenuHandler, MenuList, MenuItem, Button } from '@material-tailwind/react';
import { CustomDrawer, CustomButton, InvisibleButton } from './';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { MdDeleteOutline } from "react-icons/md";
import { FaRegEdit, FaUserMinus, FaUserPlus } from "react-icons/fa";
import { useState, useEffect } from 'react';
import { MdChecklist } from "react-icons/md";
import { IoMdEye } from "react-icons/io";
import { useNavigate } from 'react-router-dom';

const BuildingCard = ({ buildingProps, fetchdata }) => {

			const navigate = useNavigate();
			const { _id, name, imageUrl, address, categories, usersList } = buildingProps;
			const [openDrawer, setOpenDrawer] = useState(false);
			const [openDrawerUpdate, setOpenDrawerUpdate] = useState(false);
			const [openDrawerDelete, setOpenDrawerDelete] = useState(false);
			const [openUserData, setOpenUserData] = useState(false);
			const [userId, setUserId] = useState(null);
			const [userName, setUserName] = useState("");
			const [userEmail, setUserEmail] = useState("");
			const [userImage, setUserImage] = useState("");
			const [buildingName, setBuildingName] = useState('');
			const [buildingImage, setBuildingImage] = useState('');
			const [buildingAddress, setBuildingAddress] = useState('');
			const [isSave, setIsSave] = useState(false);
			const [isDelete, setIsDelete] = useState(false);
			const [isUpdated, setIsUpdated] = useState(false);
			const [userDetails, setUserDetails] = useState('');
			const [isDisabled, setIsDisabled] = useState(true);
			const [buttonLoading, setButtonLoading] = useState(false);

			const [newName, setNewName] = useState(name);
			const [newAddress, setNewAddress] = useState(address);
			const [newImageUrl, setNewImageUrl] = useState(imageUrl);
			const [openMenu, setOpenMenu] = useState(false);
			const [message, setMessage] = useState("hidden");

			const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
			const handleOpenDrawerUpdate = () => setOpenDrawerUpdate(!openDrawerUpdate);
			const handleOpenDrawerDelete = () => setOpenDrawerDelete(!openDrawerDelete);
			const handleOpenUserData = () => setOpenUserData(!openUserData);

			/*fetch users list*/
			const fetchUserData = () => {
					fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/user-details/${userId}`, {
								method: "GET"
					})
					.then(response => response.json())
					.then(data => {
							if (data === false) {
									setMessage("");
									setIsDisabled(true);
									setTimeout(() => setButtonLoading(false),2000);
							} else {
									setUserName(data.name);
									setUserEmail(data.email);
									setUserImage(data.imageUrl);
									setIsDisabled(false);
									setButtonLoading(false);
									setMessage("hidden")
							}
					})
					.catch(error => console.error)
			} 

			useEffect(() => {

					if (userId !== null) {
								setButtonLoading(true);
								fetchUserData();
					}
			}, [userId])

					useEffect(() => {
								setBuildingName(name);
								setBuildingImage(imageUrl);
								setBuildingAddress(address);
					}, [])
			/**/

			const handleInputChange = (e) => {
					setUserId(e.target.value);
					fetchUserData();
			} 

			/*handle add user*/
				const handleAddUser = (e) => {
							e.preventDefault();
							setIsSave(true);
							setIsDisabled(true);
							setButtonLoading(true);
							fetch(`${import.meta.env.VITE_API_URL}/admin/building/add-user/${_id}`, {
										method: "POST",
										headers: {
												'Content-type': 'application/json'
										},
										body: JSON.stringify({
												//user details
												userId: userId,
												name: userName,
												email: userEmail,
												imageUrl: userImage,
												// building details
												buildingName: buildingName,
												buildingImage: buildingImage,
												buildingAddress: buildingAddress
										})
							})
							.then(response => response.json())
							.then(data => {
										if (data === true) {
													toast.success(<small className="capitalize"> {userName} has been added successfully to {buildingName}! </small>);
													setUserId('');
													setTimeout(() => setIsSave(false), 3500)
													setTimeout(() => setOpenDrawer(false), 4000)
													setUserId("");
													setIsDisabled(true);
													setButtonLoading(false);
													{fetchdata}
										} else {
													toast.error('Error adding user!');
													setTimeout(() => toast.warning(<small>Make sure that the userID is correct and is not yet added in the building!</small>), 2000)
													setOpenDrawer(false);
													setIsSave(false);
													setUserId('');
													setButtonLoading(false);
										} 
							})
				}
			/**/

			/*handle delete building*/
						const handleDelete = (_id) => {
									fetch(`${import.meta.env.VITE_API_URL}/admin/building/${_id}`, {
												method: "DELETE"
									})
									.then(response => response.json())
									.then(data => {
												if (data === true) {
															toast.success("Successfully deleted!");
															{fetchdata}
												} else {
															toast.error('Error deleting data!')
												}
									})
									.catch(error => console.error)
						}
			/**/

				/*handle update building*/
							const handleUpdate = (event) => {
							     event.preventDefault();
							     setIsUpdated(true);

							     fetch(`${import.meta.env.VITE_API_URL}/admin/building/${_id}`, {
							        method: "PATCH",
							        headers: {
							          'Content-type': 'application/json'
							        },
							        body: JSON.stringify({
							          newName,
							          newImageUrl,
							          newAddress
							        })
							     })
							     .then(response => response.json())
							     .then(data => {
							        if (data === true) {
							           toast.success("Successfully Updated!");
							           setOpenDrawerUpdate(false);
							           setIsUpdated(false);
							           {fetchData}
							        } else {
							          toast.error("Unable to update building data");
							          setIsUpdated(false);
							          setOpenDrawer(false)
							        }
							     })
							  }
				/**/

			let list = (
							usersList.map(user => {
										return (
													<MenuItem onClick={() => handleOpenUserData(user._id)} key={user._id}>
														<ul className="">
																<li className="capitalize">{user.name}</li>
														</ul>
													</MenuItem>
										)
							})
				)

			const handleRemoveUser = () => {
					setIsDelete(true);
						fetch(`${import.meta.env.VITE_API_URL}/admin/building/remove-user/${_id}`, {
									method: "PATCH",
									headers: {
											'Content-type': 'application/json'
									},
									body: JSON.stringify({
											userId: userId
									})
						})
						.then(response => response.json())
						.then(result => {
									if (result === true) {
												toast.success(<small className="capitalize"> {userName} has been successfully removed from {buildingName}! </small>);
												setUserId('');
												setTimeout(() => setIsDelete(false), 3500)
												setTimeout(() => setOpenDrawerDelete(false), 4000)
												setIsDisabled(true);
												setButtonLoading(false);
												{fetchdata}
									} else {
											toast.error('Error removing user!');											
											setOpenDrawerDelete(false);
											setIsDelete(false);
											setUserId('');
											setButtonLoading(false);
									}
						})
						.catch(error => console.error(error))
			}

				return(
									<>
												<CustomDrawer
														open={openUserData}
														handleOpenDrawer={handleOpenUserData}
														handleCloseDrawer={handleOpenUserData}
														title="User"
												>
													
											
													
												</CustomDrawer>
												

											<CustomDrawer
													open={openDrawer}
													handleOpenDrawer={handleOpenDrawer}
													handleCloseDrawer={handleOpenDrawer}
													title={name}
											>
												<form onSubmit={handleAddUser} className="flex p-5 flex-col gap-5 justify-center items-center">
														<Typography className="mb-8" variant="h6">Add User</Typography>
            		<Input
            				label="User ID"
            				value={userId}
            				onChange={handleInputChange}
            		/>
            		<CustomButton
            				type="submit"
            				label={!isSave ? "Add" : "Adding..."}
            				tooltip="hidden"
            				loading={buttonLoading}
            				isDisabled={isDisabled}
            		/>
            		<div className={`text-xs ${message}`}>
            				This user ID doesn't exists in the database!
            		</div>
												</form>
											</CustomDrawer>

											<CustomDrawer
													open={openDrawerDelete}
													handleOpenDrawer={handleOpenDrawerDelete}
													handleCloseDrawer={handleOpenDrawerDelete}
													title={name}
											>
												<form className="flex p-5 flex-col gap-5 justify-center items-center">
														<Typography className="mb-8" variant="h6">Remove User</Typography>
            		<Input
            				label="User ID"
            				value={userId}
            				onChange={handleInputChange}
            		/>
            		<CustomButton
            				label={!isDelete ? "Remove" : "Removing..."}
            				tooltip="hidden"
            				loading={buttonLoading}
            				isDisabled={isDisabled}
            				handleClick={handleRemoveUser}
            		/>
            		<div className={`text-xs ${message}`}>
            				This user ID doesn't exists in the database!
            		</div>
												</form>
											</CustomDrawer>
											<Card className="w-full group hover:scale-105 max-w-[20rem] bg-blue-gray-50/50 flex-row">
													<CardHeader
													  shadow={false}
													  floated={false}
													  className="m-0 w-2/5 shrink-0 rounded-r-none"
													>
													  <img
													    src={imageUrl}
													    className="h-full w-full object-cover"
													  />

													</CardHeader>
											  <CardBody>
											  	<div className="flex items-center gap-x-1 absolute top-1 right-1">
											  	   <Menu open={openMenu} handler={setOpenMenu} allowHover>
											  	     <MenuHandler>
											  	       <Button
											  	         variant="text"
											  	         className="flex items-center gap-3 text-xs font-leading tracking-normal hover:bg-rounded-full"
											  	       >
											  	        {usersList.length} {" "}

											  	         <MdChecklist 
											  	         		className={`w-4 h-4 ${openMenu ? "text-green-600" : ""}`}
											  	         />
											  	         
											  	       </Button>
											  	     </MenuHandler>
											  	     <MenuList className="max-h-[20rem] overflow-y-auto scrollbar-thin">
											  	       <Typography className="text-sm flex flex-row justify-center">List of Users: &nbsp;<span className="uppercase text-green-500"> </span> </Typography>
											  	       <hr className="my-3" />

											  	       	{list}
											  	       
											  	     </MenuList>
											  	   </Menu>
											  	</div>

											  		<Typography variant="h6" color="blue-gray" className="mt-4 font-bold capitalize">
											  		  {name}
											  		</Typography>
											  		<Typography color="gray" className="font-normal text-xs italic">
											  		  {address}
											  		</Typography>	

					          <div className="flex flex-row absolute bottom-1 gap-1">
					          	<InvisibleButton 
					          	  label="add user"
					          	  icon={<FaUserPlus className="w-4 h-4" />}
					          	  tooltip="!bg-cyan-600"
					          	  buttonClass="!text-green-600"
					          	  color="cyan"
					          	  placement="top"
					          	  handleClick={handleOpenDrawer}
					          	/>
					          	<InvisibleButton 
					          	  label="remove user"
					          	  icon={<FaUserMinus className="w-4 h-4" />}
					          	  tooltip="!bg-dark"
					          	  buttonClass="!text-dark"
					          	  placement="top"
					          	  handleClick={handleOpenDrawerDelete}
					          	/>
					          	<InvisibleButton 
					          	  label="view"
					          	  icon={<IoMdEye className="w-4 h-4" />}
					          	  tooltip="!bg-green-600"
					          	  buttonClass="!text-green-600"
					          	  color="green"
					          	  placement="top"
					          	  handleClick={() => navigate(`/admin/buildings/building/${_id}`)}
					          	/>
					          	<InvisibleButton 
					          	  label="edit"
					          	  icon={<FaRegEdit className="w-4 h-4" />}
					          	  tooltip="!bg-cyan-600"
					          	  buttonClass="!text-dark"
					          	  color="cyan"
					          	  placement="top"
					          	  handleClick={handleOpenDrawerUpdate}
					          	/>
					          	<InvisibleButton 
					          	  label="delete"
					          	  icon={<MdDeleteOutline className="w-4 h-4" />}
					          	  tooltip="!bg-red-600"
					          	  buttonClass="!text-red-600"
					          	  color="red"
					          	  placement="top"
					          	  handleClick={() => handleDelete(_id)}
					          	/>
					          </div>

											  </CardBody>
											</Card>

											<CustomDrawer
											  title={`Update ${name}`}
											  open={openDrawerUpdate}
											  handleOpenDrawer={handleOpenDrawerUpdate}
											  handleCloseDrawer={handleOpenDrawerUpdate}
											>
											  <form className="flex flex-col gap-3 p-5" onSubmit={handleUpdate}>
											      <Input
											        label="Name"
											        value={newName}
											        onChange={(e) => setNewName(e.target.value)}
											        color="teal"
											        className="capitalize"
											      />
											      <Input
											        label="Address"
											        value={newAddress}
											        onChange={(e) => setNewAddress(e.target.value)}
											        color="teal"
											      />
											      <Textarea
											        label="Image Url"
											        value={newImageUrl}
											        onChange={(e) => setNewImageUrl(e.target.value)}
											        color="teal"
											        className="text-xs min-h-[70px]"
											      />
											      <img
											        src={newImageUrl}
											        className="w-20 mx-auto"
											      />

											      <div className="flex flex-row justify-center gap-2 mt-6">
											        <CustomButton
											          handleClick={handleOpenDrawerUpdate}
											          label="Cancel"
											          tooltip="hidden"

											        />
											        <CustomButton
											          type="submit"
											          label={!isUpdated ? "Update" : "Updating..."}
											          tooltip="hidden"
											          loading={isUpdated ? true : false}
											          isDisabled={isUpdated ? true : false}
											          color="green"
											        />
											      </div>
											  </form>
											</CustomDrawer>

									</>
				)
		} 


export default BuildingCard