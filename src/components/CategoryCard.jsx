import { useState, useEffect, useContext } from 'react';
import { CustomButton, CustomDialogBox, CustomDrawer, InvisibleButton } from './'
import { Card, CardHeader, CardBody, Typography, Input, Textarea } from '@material-tailwind/react';
import { FaRegEdit } from "react-icons/fa";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { MdDeleteOutline } from "react-icons/md";
import DataContext from '../DataContext';

	const CategoryCard = ({ CategoriesProps, fetchCategories, count }) => {

		const { building } = useContext(DataContext);

		const { _id, categoryName, imageUrl } = CategoriesProps;
		const [newName, setNewName] = useState(categoryName);
		const [new_image_url, set_new_image_url] = useState(imageUrl);
		const [isUpdateClicked, setIsUpdateClicked] = useState(false);
		const [isDisabled, setIsDisabled] = useState(true);
		const [nameAlert, setNameAlert] = useState(false);
		const [imageUrlAlert, setImageUrlAlert] = useState(false);
		const [openDrawerUpdate, setOpenDrawerUpdate] = useState(false);
		const [openDialog, setOpenDialog] = useState(false);
		const [isDeleteClicked, setIsDeleteClicked] = useState(false);

		const handleOpenDialog = () => setOpenDialog(!openDialog);

		const handleOpenDrawerUpdate = () => setOpenDrawerUpdate(!openDrawerUpdate);

		useEffect(() => {
				newName !== categoryName ||
				new_image_url !== imageUrl ?
				setIsDisabled(false) : setIsDisabled(true)
		}, [newName, new_image_url])

		const handleUpdate = (e) => {
				e.preventDefault()
				setIsUpdateClicked(true);
				setIsDisabled(true);
				if (newName.length < 3) {
							setNameAlert(true);
				}
				if (new_image_url.length === 0) {
							setImageUrlAlert(true);
				}
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/category/${building.buildingId}/update/${_id}`, {
						method: 'PATCH',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify({
								newCategoryName: newName,
								newImageUrl: new_image_url
						})
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`Successfully Updated!`)
										setTimeout(() => setOpenDrawerUpdate(false), 2000)
										fetchCategories();
										setTimeout(() => setIsUpdateClicked(false), 1000)
										setIsDisabled(true);
							} else {
										toast.error('Unable to update the category!')
										setName('');
										set_image_url('');
										setIsClicked(false);
										setIsDisabled(true);
							}
				})
		}

		const handleDelete = (_id) => {
				setIsDeleteClicked(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/category/${building.buildingId}/remove/${_id}`, {
							method: 'PATCH',
							headers: {
									'Content-type': 'application/json'
							}
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${newName.toUpperCase()} Successfully Deleted!`);
										fetchCategories();
										setOpenDialog(false);
							}
							else
							{
									toast.error('Cannot remove item!');
									setOpenDialog(false);
							}
				})
		}

				return (
					<>
						<CustomDrawer
								title={`Update ${categoryName}`}
								open={openDrawerUpdate}
								handleOpenDrawer={handleOpenDrawerUpdate}
								handleCloseDrawer={handleOpenDrawerUpdate}
								className={ openDrawerUpdate ? "min-w-[50vw]" : "" }
						>

							<div className="py-8 px-12">
									<form className="flex flex-col gap-3">
									<div className="">
									  <Input
									  		value={newName}
									  		onChange={(e) => setNewName(e.target.value)}
									    type="text"
									    size="sm"
									    className={`capitalize`}
									    label="Name"
									    color="teal"
									    error={nameAlert}
									  />
									</div>
									<div className="w-full">
									  <Textarea 
									  		value={new_image_url}
									  		onChange={(e) => set_new_image_url(e.target.value)}
									    label="Image Url"
									    rows={2}
									    resize={true}
									    className="min-h-[60px] text-xs scrollbar-thin"
									    color="teal"
									    error={imageUrlAlert}
									  />
									</div>
									<div>
									  <img className="m-auto shadow-md w-[40%]" src={new_image_url} />
									</div>
									<div className="flex flex-row gap-2 mt-10 justify-center">
										<CustomButton
												label="Clear"
												tooltip="hidden"
										/>
										<CustomButton
												label="Update"
												tooltip="hidden"
												color="cyan"
												loading={isUpdateClicked ? true : false}
												isDisabled={isDisabled}
												handleClick={handleUpdate}
										/>
									</div>
							</form>
							</div>

						</CustomDrawer>
						<Card className="w-full group hover:scale-105 max-w-[20rem] bg-blue-gray-50/50 flex-row">
						  <CardHeader
						    className="m-0 w-2/5 shadow-lg shrink-0 rounded-r-none"
						  >
						    <img
						      src={imageUrl}
						      className="h-full w-full object-cover"
						    />
						  </CardHeader>
						  <CardBody className="h-full flex items-center">
						  		<Typography className="absolute top-1 right-1 rounded-full bg-gray-600/70 text-light text-xs px-1 py-0.2" >{count}</Typography>
						    <Typography color="gray" className="text-xs font-bold uppercase">
						      {categoryName}
						    </Typography>

          <div className="flex flex-col gap-3">
          	<InvisibleButton 
          	  label="edit"
          	  icon={<FaRegEdit className="w-4 h-4" />}
          	  tooltip="!bg-green-600"
          	  buttonClass="!text-green-600"
          	  color="green"
          	  placement="left"
          	  handleClick={handleOpenDrawerUpdate}
          	/>
          	<InvisibleButton 
          	  label="delete"
          	  icon={<MdDeleteOutline className="w-4 h-4" />}
          	  tooltip="!bg-red-600"
          	  buttonClass="!text-red-600"
          	  color="red"
          	  placement="left"
          	  handleClick={handleOpenDialog}
          	/>
          </div>

						  </CardBody>
						</Card>

						<CustomDialogBox
								size="xs"
								open={openDialog}
								handleOpen={handleOpenDialog}
								title="Confirmation"
								closeButton={handleOpenDialog}
						>
								<div>
								  <Typography>Delete {newName.toUpperCase()}?</Typography>
								  <div className="mt-8 flex flex-row gap-1 justify-center mx-auto">
								      <CustomButton
								        label="Cancel"
								        handleClick={handleOpenDialog}
								        btnClass="rounded px-3 py-2 hover:scale-105"
								        tooltip="hidden"
								      />

								      <CustomButton
								        label={!isDeleteClicked ? "Delete" : "Deleting..."}
								        handleClick={() => handleDelete(_id)}
								        btnClass="rounded px-3 py-2 hover:scale-105"
								        color="red"
								        tooltip="hidden"
								        loading={ isDeleteClicked ? true : false }
								        isDisabled={ isDeleteClicked ? true : false }
								      />

								  </div>
								</div>
						</CustomDialogBox>

				</>
				)
	}

	export default CategoryCard