import { Card, CardHeader, CardBody, Button, Typography, CardFooter, Progress } from '@material-tailwind/react';
import { NavBar, SideNavBar, Loading, CustomButton } from './';
import { ToastContainer } from 'react-toastify';
import { RiRefreshFill } from "react-icons/ri";
import { TiChevronRight } from "react-icons/ti";
import DataContext from '../DataContext';
import { useContext } from 'react';

const Layout = ({ children, childrenClass, loading, refresh, title, handleToggleButton, iconClass, card2, card2Label, childrenClass2, children2, copyRight, copyRight2, itemNumber, sort, cardBody, footerText, headerButtons, search, backButton }) => {

	const toggleButton = (
					<CustomButton 
							icon={<TiChevronRight className={`h-4 w-4 transition-transform delay-300 ${iconClass}`} />}
							variant="text"
							tooltip="hidden"
							handleClick={handleToggleButton}
					/>
		)

	const { settings } = useContext(DataContext);

		return (
					<>
							<NavBar />	
							<div className="flex flex-row">
									<SideNavBar />
										<Card color="transparent" className="shadow-none mt-2 mr-2 h-full w-full">
							      <div className="py-1 bg-[#E9E9EA]/60 h-full max-h-[3rem] min-h-[3rem] rounded-t-md shadow-sm border-t-[2px] border-t-[#828484] flex flex-row justify-between items-center justify-center">
							      		{backButton}
							      		<Typography className="uppercase font-ibm font-bold tracking-wide text-xl ml-2">{title}</Typography>      		
							      		<div className={`flex flex-row ${headerButtons}`}>
							      				<div>{sort}</div>
							      				<div>{search}</div>
							      				
							      				<CustomButton
							      						icon={<RiRefreshFill className="w-5 text-gray-600 h-5" />}
							      						variant="text"
							      						handleClick={refresh}
							      						content="refresh"
							      						placement="bottom"
							      						tooltip="bg-gray-600"
							      				/>
							      				{toggleButton}
							      		</div>
							      </div>
							      <CardBody className={`${cardBody} h-full bg-[#F0F0F9] shadow-sm rounded-b-md w-full min-h-[78vh] max-h-[78vh] overflow-y-auto overflow-x-hidden scrollbar-thin scrollbar-thumb-gray-700 scrollbar-track-gray-100`}>
							        	<div className={childrenClass}>
							        			{children}
							        	</div>
							        	<div className={loading}>
							        			<Loading />
							        	</div>
							        	<div className="font-semibold text-xs absolute bottom-8 left-1">
							        			{footerText}
							        	</div>
							      </CardBody>
							      <CardFooter className="py-1 px-0.5 rounded-b-xl flex flex-row justify-between">
							        <div className="font-semibold text-xs">
							        		{settings.shortName !== null ? settings.shortName : <RiRefreshFill className="w-5 text-gray-600 h-5 animate-spin" />}
							        </div>
							        <div className="font-semibold text-xs lowercase italic">
							        		{itemNumber}
							        </div>
							        <div className={`font-semibold text-xs uppercase ${copyRight}`}>
							        			&copy; 2023
							        </div>
							      </CardFooter>
							    </Card>

				    			<Card color="transparent" className={`shadow-none mt-2 mr-3 h-full w-full max-w-[19rem] ${card2}`}>
				          <div className="py-1 bg-[#E9E9EA]/60 h-full max-h-[3rem] min-h-[3rem] rounded-t-md shadow-sm border-t-[2px] border-t-[#828484] border-[#7D9A93] flex flex-row justify-center">
				          		<div className="flex items-center justify-center font-ibm uppercase">
				          				{card2Label}
				          		</div>
				          </div>
				          <CardBody className="h-full p-5 bg-[#F0F0F9] shadow-sm rounded-b-md w-full min-h-[78vh] max-h-[78vh] overflow-y-auto scrollbar-thin scrollbar-thumb-gray-700 scrollbar-track-gray-100">
				            	<div className={childrenClass2}>
				            			{children2}
				            	</div>
				          </CardBody>
				          <CardFooter className="py-1 px-0.5 rounded-b-xl flex flex-row justify-end">
				            <div className={`font-semibold text-xs uppercase ${copyRight2}`}>
				            			&copy; 2023
				            </div>
				          </CardFooter>
				        </Card>
							</div>
							
							<div className="absolute">
							  <ToastContainer position="bottom-right" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false}draggable pauseOnHover theme="light" />
							</div>	
					</>
		)
}

export default Layout;


