import { MdDeleteOutline } from "react-icons/md";
import { FaRegEdit } from "react-icons/fa";
import { InvisibleButton, CustomDialogBox, CustomButton, CustomDrawer } from './';
import { useState, useEffect, useContext } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Input, Typography } from '@material-tailwind/react';
import DataContext from '../DataContext';

const RoomCard = ({ props, fetchData, count }) => {

	const { building } = useContext(DataContext);
	const { _id, roomName } = props;
	const [isDelete, setIsDelete] = useState(false);
	const [isUpdated, setIsUpdated] = useState(false);
	const [open, setOpen] = useState(false);
	const [openDrawer, setOpenDrawer] = useState(false);
	const [newRoomName, setNewRoomName] = useState(roomName);
	const [isDisabled, setIsDisabled] = useState(true);

	const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
	const handleOpen = () => setOpen(!open);

	const handleDelete = (_id) => {
			setIsDelete(true);
			fetch(`${import.meta.env.VITE_API_URL}/ulafis/room/${building.buildingId}/remove/${_id}`, {
						method: 'PATCH',
						headers: {
								'Content-type': 'application/json'
						}
			})
			.then(response => response.json())
			.then(data => {
						if (data === true) {
									toast.success(`${roomName.toUpperCase()} Successfully Deleted!`);
									fetchData();
									setTimeout(() => {
											setIsDelete(false)
											setOpen(false)
									}, 3000)
						}
						else
						{
								toast.error('Cannot remove item!');
								setOpen(false);
						}
			})
	}

	useEffect(() => {
			newRoomName !== roomName ? setIsDisabled(false) : setIsDisabled(true) 
	}, [newRoomName])

	const handleUpdate = () => {
			setIsDisabled(true);
			setIsUpdated(true);
			fetch(`${import.meta.env.VITE_API_URL}/ulafis/room/${building.buildingId}/update/${_id}`, {
					method: "PATCH",
					headers: {
						'Content-type': 'application/json'
					},
					body: JSON.stringify({
							newRoomName: newRoomName
					})
			})
			.then(response => response.json())
			.then(data => {
						if (data === true) {
								toast.success("Successfully Updated!")
								fetchData();
								setNewRoomName("");
								setOpenDrawer(false);
								setIsUpdated(false);
						} else {
								toast.error("Unable to update data!");
								setOpenDrawer(false);
								setIsUpdated(false);
						}

			})
			.catch(error => console.error)
	}

		return (
						<>
							<div className="text-center p-1 capitalize border border-1 border-dark/25 rounded-md group hover:scale-105 shadow-lg">
										<Typography className="absolute text-xs bg-green-600 text-light px-1 py-0.2 rounded-full">{count}</Typography>
										<Typography className="mt-6 px-5">{roomName}</Typography> 
										<div className="flex flex-row justify-center">
											<InvisibleButton 
											  label="edit"
											  icon={<FaRegEdit className="w-2.5 h-2.5" />}
											  tooltip="!bg-cyan-600"
											  buttonClass="!text-cyan-600"
											  color="cyan"
											  placement="bottom"
											  handleClick={handleOpenDrawer}
											  btn="-mr-16"
											/>
											<InvisibleButton 
											  label="delete"
											  icon={<MdDeleteOutline className="w-2.5 h-2.5" />}
											  tooltip="!bg-red-600"
											  buttonClass="!text-red-600"
											  color="red"
											  placement="bottom"
											  handleClick={handleOpen}
											/>
										</div>
							</div>
							<CustomDialogBox
									title="Confirmation"
									open={open}
									handleOpen={handleOpen}
							>

								Delete {roomName}?

								<CustomButton
										handleClick={() => handleDelete(_id)}
										label="Delete"
										loading={ isDelete ? true : false }
										tooltip="hidden"
										color="red"
										btn="mx-auto mt-10"
								/>

							</CustomDialogBox>
							<CustomDrawer
									title="Edit"
									open={openDrawer}
									handleOpenDrawer={handleOpenDrawer}
							>
									<div className="py-10 px-5">
										<Input
												label="Name"
												value={newRoomName}
												onChange={(e) => setNewRoomName(e.target.value)}
										/>
										<CustomButton
												handleClick={handleUpdate}
												label={ isUpdated ? "Saving ..." : "Save" }
												tooltip="hidden"
												color="green"
												btn="mx-auto mt-10"
												isDisabled={isDisabled}
												loading={ isUpdated ? true : false }
										/>
									</div>

							</CustomDrawer>
						</>
			)
}

export default RoomCard