import React from "react";
import {
  Drawer,
  Button,
  Typography,
  IconButton,
  Input,
  Textarea,
} from "@material-tailwind/react";
 
export default function CustomDrawer({ title, open, handleOpenDrawer, handleCloseDrawer, children, className }) {
   
  return (
    <React.Fragment>
     <Drawer className={`bg-defaultColor shadow-none w-full ${className}`} placement="right" open={open} onClose={handleCloseDrawer}>
        <div className="flex items-center justify-between px-4 pb-2">
          <Typography className="capitalize" variant="h5" color="blue-gray">
            {title} 
          </Typography>
          <IconButton variant="text" color="blue-gray" onClick={handleOpenDrawer}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={2}
              stroke="currentColor"
              className="h-5 w-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </IconButton>
        </div>
        <div className="h-full max-h-[80vh] overflow-y-auto scrollbar-thin scrollbar-thumb-gray-700 scrollbar-track-gray-100">
          {children}
        </div>
      </Drawer>
    </React.Fragment>
  );
}