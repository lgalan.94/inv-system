import { Card, CardBody, Typography, CardFooter } from '@material-tailwind/react';
import { FaRegArrowAltCircleRight } from "react-icons/fa";
import { Link } from 'react-router-dom';

const AdminHomeCard = ({ name, icon, link, count }) => {
			return (
						<Card className="shadow-none border border-2 rounded-md border-blue-gray-300 bg-[#E9E9EA]/60">
								<CardBody className="flex flex-col gap-1">
										<div className="absolute">
											{icon}
										</div>
										<Typography className="text-left ml-14 mt-3 uppercase text-md tracking-wider font-bold">{name}</Typography>
										<Typography className="absolute right-2 top-1 uppercase text-2xl tracking-wide font-bold">{count}</Typography>
										
								</CardBody>
								<CardFooter className="py-1 bg-defaultColor text-sm italic">
										<Link className="flex flex-row justify-end items-center gap-1 hover:text-blue-400" to={link}>
												Go to <span className="lowercase">{name}</span> <FaRegArrowAltCircleRight />
										</Link>
								</CardFooter>
						</Card>
			)
}

export default AdminHomeCard