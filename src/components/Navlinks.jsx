import { List, ListItem, ListItemPrefix, ListItemSuffix, Typography, Chip, Accordion, AccordionHeader, AccordionBody, Card, CardBody } from '@material-tailwind/react';
import { NavLink, useLocation } from 'react-router-dom'
import { TbCategoryPlus } from "react-icons/tb";
import { FiHome } from "react-icons/fi";
import { IoMdAdd } from "react-icons/io";
import { MdNotificationsActive } from "react-icons/md";
import { MdOutlineInventory } from "react-icons/md";
import { useState, useEffect, useContext } from 'react';
import { SiManageiq } from "react-icons/si";
import { FaUsersGear } from "react-icons/fa6";
import { GoReport } from "react-icons/go";
import { IoIosSettings } from "react-icons/io";
import { FaBuilding } from "react-icons/fa";
import { BsBuildingAdd } from "react-icons/bs";
import { FaChevronDown, FaChevronRight } from "react-icons/fa6";
import { FaHome } from "react-icons/fa";
import { IoIosContact } from "react-icons/io";
import { MdSchool } from "react-icons/md";
import { GrUserWorker } from "react-icons/gr";
import { GiSkills } from "react-icons/gi";
import { PiCertificateBold } from "react-icons/pi";
import { MdOutlineSettingsSuggest } from "react-icons/md";
import { GoProjectSymlink } from "react-icons/go";
import { FaMessage } from "react-icons/fa6";
import { MdDashboard } from "react-icons/md";
import { MdOutlineMeetingRoom } from "react-icons/md";
import DataContext from '../DataContext';

import { PiEyedropperSample } from "react-icons/pi";
import { AiFillExperiment } from "react-icons/ai";
import { GrResources } from "react-icons/gr";
import { GoDot } from "react-icons/go";

const Navlinks = () => {

	const { data, building } = useContext(DataContext);
	let currentPath = useLocation().pathname;
	let samplesPath = `/lab-manager/${building.buildingId}/samples`;
	let addSettingsPath = '/ulafis/admin/settings/add';
	let usersPath = '/ulafis/admin/manage-users';

	const [open, setOpen] = useState(0); 
 const handleOpen = (value) => {
   setOpen(open === value ? 0 : value);
 };

	const CustomLink = ({ name, linkIcon, linkPath, className, chipValue, chipClassname, listItem, listClass  }) => {

	  return (
	    <NavLink 
	      as={NavLink}
	      to={linkPath}
	      className={`${className} ${currentPath === linkPath ?  'bg-white/50 shadow-sm rounded-lg' : 'bg-none' } `}
	    >
	      <ListItem className={`!font-ibm text-sm py-2 ${listClass}`}>
	        <ListItemPrefix>
	          {linkIcon}
	        </ListItemPrefix>
	        {name}
	        <ListItemSuffix>
            <span className={`${chipClassname} text-xs bg-gray-100 px-2 py-1 rounded-full`}>{chipValue}</span>
          </ListItemSuffix>
	      </ListItem>
	    </NavLink>
	  )
	}

	return (
		<>
			<List className="font-ibm">
					<CustomLink 
						name="dashboard" 
						linkIcon={<MdDashboard className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/home`} 
						chipClassname="hidden"
					/>

					<hr className="border shadow" />
					<div className="flex ml-1.5 mt-1 flex-row items-center gap-1">
						<span> <PiEyedropperSample className="w-5 h-5" /> </span>
						<Typography className="text-sm" variant="h6" >Sample Management</Typography>
					</div>

					<CustomLink 
						name="Samples" 
						linkIcon={<GoDot strokeWidth={3} className="h-3 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/samples`} 
						chipClassname="hidden"
						listClass="ml-3"
					/>
					<CustomLink 
						name="Storage Locations" 
						linkIcon={<GoDot strokeWidth={3} className="h-3 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/samples/locations`} 
						chipClassname="hidden"
						listClass="ml-3"
					/>

					<hr className="border shadow" />
					<div className="flex ml-1.5 mt-1 flex-row items-center gap-1">
						<span> <AiFillExperiment className="w-5 h-5" /> </span>
						<Typography className="text-sm" variant="h6" >Experiment Tracking</Typography>
					</div>

					<CustomLink 
						name="Experiments" 
						linkIcon={<GoDot strokeWidth={3} className="h-3 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/experiment-tracking`} 
						chipClassname="hidden"
						listClass="ml-3"
					/>

					{/*<hr className="border shadow" />
					<div className="flex ml-2 mt-1 flex-row items-center gap-1">
						<span> <GrResources className="w-5 h-5" /> </span>
						<Typography className="text-sm" variant="h6" >Resource Allocation</Typography>
					</div>
					
					<CustomLink 
						name="Resource Allocation" 
						linkIcon={<GoDot strokeWidth={3} className="h-3 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/resource-allocation`} 
						chipClassname="hidden"
						listClass="ml-3"
					/>*/}

					<hr className="border shadow" />
					<div className="flex ml-2 mt-1 flex-row items-center gap-1">
						<span> <MdOutlineInventory className="w-5 h-5" /> </span>
						<Typography className="text-sm" variant="h6" >Inventory Management</Typography>
					</div>

					<CustomLink 
						name="Items" 
						linkIcon={<GoDot strokeWidth={3} className="h-3 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/inventory`} 
						chipClassname="hidden"
						listClass="ml-3"
					/>
					<CustomLink 
						name="Categories" 
						linkIcon={<GoDot strokeWidth={3} className="h-3 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/categories`} 
						chipClassname="hidden"
						listClass="ml-3"
					/>
					<CustomLink 
						name="Locations" 
						linkIcon={<GoDot strokeWidth={3} className="h-3 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/storage-locations`} 
						chipClassname="hidden"
						listClass="ml-3"
					/>
					 
					
					{/*<CustomLink 
						name="Inventory" 
						linkIcon={<MdOutlineInventory className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/inventory`} 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Categories" 
						linkIcon={<TbCategoryPlus className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/categories`} 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Storage Locations" 
						linkIcon={<MdOutlineMeetingRoom className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/storage-locations`} 
						chipClassname="hidden"
					/>*/}
					{/*<CustomLink 
						name="Notifications" 
						linkIcon={<MdNotificationsActive className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/notifications`} 
						chipValue="5"
					/>*/}
					{/*<CustomLink 
						name="Settings" 
						linkIcon={<MdOutlineSettingsSuggest className="h-5 w-5" />} 
						linkPath="/lab-manager/settings" 
						chipClassname="hidden"
					/>*/}
					
			</List>
		</>
	)
}

export default Navlinks