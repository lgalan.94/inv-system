import { Menu, Tooltip, MenuHandler, Button, MenuList, MenuItem } from '@material-tailwind/react';
import { BiSort } from "react-icons/bi";

const CustomMenu = ({ children,  }) => {
			return (
						<Menu>
						   <Tooltip content="sort" className="bg-green-500">
						   		<MenuHandler>
						   		  <Button variant="text" color="green"> <BiSort className="w-5 h-5" /> </Button>
						   		</MenuHandler>
						   </Tooltip>
						   <MenuList>
						     {children}
						   </MenuList>
						 </Menu>
			)
}

export default CustomMenu