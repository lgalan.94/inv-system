import { List, ListItem, ListItemPrefix, ListItemSuffix, Typography, Chip, Accordion, AccordionHeader, AccordionBody } from '@material-tailwind/react';
import { NavLink, useLocation } from 'react-router-dom'
import { useState, useEffect } from 'react';
import { FaBuilding } from "react-icons/fa";
import { FaChevronDown, FaChevronRight, FaUsersGear } from "react-icons/fa6";
import { MdDashboard, MdOutlineSettingsSuggest, MdOutlineInventory } from "react-icons/md";
import { TbReportAnalytics } from "react-icons/tb";

const AdminNavlinks = () => {

	let currentPath = useLocation().pathname;

	const [open, setOpen] = useState(0); 
  const handleOpen = (value) => {
   setOpen(open === value ? 0 : value);
 };

	const CustomLink = ({ name, linkIcon, linkPath, className, chipValue, chipClassname, listItem  }) => {

	  return (
	    <NavLink 
	      as={NavLink}
	      to={linkPath}
	      className={`${className} ${currentPath === linkPath ?  'bg-white/50 shadow-sm rounded-lg' : 'bg-none' } `}
	    >
	      <ListItem className="!font-ibm">
	        <ListItemPrefix>
	          {linkIcon}
	        </ListItemPrefix>
	        {name}
	      </ListItem>
	    </NavLink>
	  )
	}

	return (
		<>
			<List className="font-ibm">
					<CustomLink 
						name="Dashboard" 
						linkIcon={<MdDashboard className="h-5 w-5" />} 
						linkPath="/admin/home"
					/>
					<CustomLink 
						name="User Management" 
						linkIcon={<FaUsersGear className="h-5 w-5" />} 
						linkPath="/admin/users" 
					/>
					<CustomLink 
						name="System Configuration" 
						linkIcon={<MdOutlineSettingsSuggest className="h-5 w-5" />} 
						linkPath="/admin/settings" 
					/>
					<CustomLink 
						name="Building Setup" 
						linkIcon={<FaBuilding className="h-5 w-5" />} 
						linkPath="/admin/buildings"
					/>
					<CustomLink 
						name="Reporting & Analytics" 
						linkIcon={<TbReportAnalytics className="h-5 w-5" />} 
						linkPath="/admin/reports" 
					/>
			</List>
		</>
	)
}

export default AdminNavlinks