import { List, ListItem, ListItemPrefix, ListItemSuffix, Typography, Chip, Accordion, AccordionHeader, AccordionBody, Card, CardBody } from '@material-tailwind/react';
import { NavLink, useLocation } from 'react-router-dom'
import { useState, useEffect, useContext } from 'react';
import DataContext from '../DataContext';

import { MdDashboard } from "react-icons/md";
import { GrTransaction } from "react-icons/gr";
import { MdOutlineInventory } from "react-icons/md";
import { MdOutlineAddchart } from "react-icons/md";

const ClerkNavlinks = () => {

	const { data, building } = useContext(DataContext);
	let currentPath = useLocation().pathname;
	let settingsPath = '/ulafis/admin/settings';
	let addSettingsPath = '/ulafis/admin/settings/add';
	let usersPath = '/ulafis/admin/manage-users';

	const [open, setOpen] = useState(0); 
 const handleOpen = (value) => {
   setOpen(open === value ? 0 : value);
 };

	const CustomLink = ({ name, linkIcon, linkPath, className, chipValue, chipClassname, listItem  }) => {

	  return (
	    <NavLink 
	      as={NavLink}
	      to={linkPath}
	      className={`${className} ${currentPath === linkPath ?  'bg-white/50 shadow-sm rounded-lg' : 'bg-none' } `}
	    >
	      <ListItem className="!font-ibm">
	        <ListItemPrefix>
	          {linkIcon}
	        </ListItemPrefix>
	        {name}
	        <ListItemSuffix>
            <span className={`${chipClassname} text-xs bg-gray-100 px-2 py-1 rounded-full`}>{chipValue}</span>
          </ListItemSuffix>
	      </ListItem>
	    </NavLink>
	  )
	}

	const CustomAccordion = ({ children, title, icon, a_value }) => {
				return (
						<Accordion
			     open={open === a_value}
			     icon={
			       <FaChevronDown
			         strokeWidth={2.5}
			         className={`mx-auto h-4 w-4 transition-transform ${open === a_value ? "rotate-180" : ""}`}
			       />
			     }
			   >
			     <ListItem className="p-1" selected={open === a_value}>
			       <AccordionHeader onClick={() => handleOpen(a_value)} className="border-b-0 px-2 py-1">
			         <ListItemPrefix>
			           {icon}
			         </ListItemPrefix>
			         <Typography color="blue-gray" className="font-ibm mr-auto font-normal">
			           {title}
			         </Typography>
			       </AccordionHeader>
			     </ListItem>
			     <AccordionBody className="py-1">
			       <List className="ml-3 p-0">
			         	{children}
			       </List>
			     </AccordionBody>
			   </Accordion>
				)
	}

	return (
		<>
			<List className="font-ibm">
					<CustomLink 
						name="dashboard" 
						linkIcon={<MdDashboard className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/home`} 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Stock Management" 
						linkIcon={<MdOutlineInventory className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/inventory`} 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Stock Replenishment" 
						linkIcon={<MdOutlineAddchart className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/categories`} 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Record Transactions" 
						linkIcon={<GrTransaction className="h-5 w-5" />} 
						linkPath={`/lab-manager/${building.buildingId}/storage-locations`} 
						chipClassname="hidden"
					/>
			</List>
		</>
	)
}

export default ClerkNavlinks