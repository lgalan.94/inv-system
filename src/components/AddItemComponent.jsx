import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Input,
  Alert,
  Select, 
  Option,
  Textarea
} from "@material-tailwind/react";
import { Link } from 'react-router-dom';
import { CustomButton } from './';
import { useState, useEffect, useContext } from 'react';
import DataContext from '../DataContext';

const AddItemComponent = ({ 
 _id, 
 name, 
 handleNameChange, 
 description, 
 handleDescriptionChange,
 quantity, 
 handleQuantityChange,
 location,
 handleLocationChange, 
 category,
 categoryLabel,
 handleCategoryChange,
 imageUrl,
 image_url,
 handleImageUrlChange,
 lowStockThreshold,
 handleLowStockThreshold,
 inputNameClass,
 inputQuantityClass,
 inputLowStockThresholdClass,
 inputLocationClass,
 inputCategoryClass,
 inputImageUrlClass,
 inputDescriptionClass,
 divClass,
 textAreaClass,
 displayAlert,
 imageClass,
 errorNameInput,
 errorQuantityInput,
 errorLSTInput,
 errorCategorySelect,
 errorLocationInput,
 errorImageUrlInput,
 errorDescriptionInput
}) => {

const { building } = useContext(DataContext);
const [categoryItems, setCategoryItems] = useState([]);

useEffect(() => {
  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`)
  .then(response => response.json())
  .then(result => {
     setCategoryItems(result.categories.map(cat => {
        return (
            <Option key={cat._id} value={cat.categoryName} > {cat.categoryName.toUpperCase()} </Option>
         )
     }))
  })
}, [])

  return (
    <div>
      <div className="flex flex-col flex-wrap mb-3 gap-1">
         {displayAlert}
      </div>
       
       <form className="flex flex-col gap-2">
        
         <div className={inputNameClass}>
           <Input
             type="text"
             size="sm"
             value={name.toUpperCase()}
             onChange={handleNameChange}
             className={`capitalize`}
             label="Name"
             color="teal"
             error={errorNameInput}
           />
         </div>
         
         <div className={inputQuantityClass}>
           <Input
             type="number"
             size="sm"
             value={quantity}
             onChange={handleQuantityChange}
             className={`capitalize`}
             label="Quantity"
             color="teal"
             error={errorQuantityInput}
           />
         </div>

         <div className={inputLowStockThresholdClass}>
           <Input
             type="number"
             size="sm"
             value={lowStockThreshold}
             onChange={handleLowStockThreshold}
             className={`capitalize`}
             label="Low Stock Threshold"
             color="teal"
             error={errorLSTInput}
           />
         </div>

         <div className={`${inputCategoryClass} w-full`}>
            <Select error={errorCategorySelect} color="teal" onChange={handleCategoryChange} label={categoryLabel}>
               {categoryItems}
            </Select>
         </div>

         
         <div className={inputLocationClass}>
           <Input
             type="text"
             size="sm"
             value={location}
             onChange={handleLocationChange}
             className={`capitalize`}
             label="Location"
             color="teal"
             error={errorLocationInput}
           />
         </div> 
         <div className={`${inputDescriptionClass} w-full`}>
           <Textarea 
             label="Description"
             value={description} 
             onChange={handleDescriptionChange}
             rows={2}
             resize={true}
             className="min-h-[40px]"
             color="teal"
             error={errorDescriptionInput}
           />
         </div>
         <div className={`${inputImageUrlClass} -mt-2 w-full`}>
           <Textarea 
             label="Image Url"
             value={image_url}
             onChange={handleImageUrlChange}
             rows={2}
             resize={true}
             className="min-h-[60px] text-xs scrollbar-thin"
             color="teal"
             error={errorImageUrlInput}
           />
         </div>
         <div>
           <img className={`${imageClass} m-auto shadow-md`} src={imageUrl} />
         </div>
       </form>

    </div>
  );
}

export default AddItemComponent