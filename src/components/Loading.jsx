import { ImSpinner2 } from "react-icons/im";
import { RiRefreshFill } from "react-icons/ri";

const Loading = ({ className }) => {
  return (
    <div className={`flex items-center justify-center h-full min-h-[82vh] md:min-h-[85vh] lg:min-h-[70vh]`}>
      <div className="flex flex-col p-5 rounded-lg gap-1 items-center justify-center">
        <RiRefreshFill className="animate-spin text-gray-400 w-14 h-14" /> 
        <div className="tracking-wider text-gray-500"> Loading... </div>
      </div>
    </div>
  );
};
 
export default Loading;