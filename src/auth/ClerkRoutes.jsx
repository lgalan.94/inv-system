import { Outlet, useNavigate } from 'react-router-dom';
import DataContext from '../DataContext';
import { useContext, useEffect } from 'react';
import { Typography } from '@material-tailwind/react';
import { MdNoEncryptionGmailerrorred } from "react-icons/md";

export default function ClerkRoutes() {
  const { data } = useContext(DataContext);
  const navigate = useNavigate();

  const value = (
    <div className="w-full h-full min-h-screen min-w-screen flex flex-col justify-center items-center">
      <MdNoEncryptionGmailerrorred className="h-16 w-16" />
      <Typography variant="h5">
        You don't have permission to access this route! 
      </Typography>
      <Typography variant="h5">
        <button onClick={() => navigate('/')} className="text-blue-500 underline">Home</button>
      </Typography>
    </div>
  );

  const loading = (
    <div className="w-full h-full min-h-screen min-w-screen flex flex-row justify-center items-center">
      <Typography variant="h5">
        Loading .....
      </Typography>
    </div>
    );

  return (
    <>
      {data.id === null ? (
        loading
      ) : data.id !== undefined && data.userRole === 2 ? (
        <Outlet />
      ) : (
        value
      )}
    </>
  );
}
