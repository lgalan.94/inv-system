import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import DataContext from '../DataContext';
import { CustomButton } from '../components';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Card, CardHeader, CardBody, CardFooter, Typography, Input, Button, Alert} from "@material-tailwind/react";
import { IoMdArrowBack } from "react-icons/io";
  
const Login = () => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isClicked, setIsClicked] = useState(false);
  const [loginAlert, setLoginAlert] = useState(false);
  const [redirectAlert, setRedirectAlert] = useState(false);
  const [deactivatedAlert, setDeactivatedAlert] = useState(false)
 
  const navigate = useNavigate();
  const { data, setData, building, setBuilding, setSettings} = useContext(DataContext);

  const BackToHome = () => {
    return navigate('/');
  }

  const fetchSettingsData = () => {
      fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings`, {
          method: "GET"
      })
      .then(response => response.json())
      .then(resp => {
          let SNAME = resp.find((name) => name.key === 'SYSTEM NAME'); 
          let SSNAME = resp.find((sname) => sname.key === 'SHORT NAME'); 

          if (SNAME && SSNAME) {
            setSettings({
                settingsName: SNAME.value,
                shortName: SSNAME.value
            })
          }
      })
      .catch(error => console.error)
  }

  const UserDetails = (token) => {
    fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/details`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(result => result.json())
      .then(userData => {
           if (userData.isActive) {
              setData({
                id: userData._id,
                email: userData.email,
                name: userData.name,
                userRole: userData.userRole,
                imageUrl: userData.imageUrl
              });
              setLoginAlert(true);
              setTimeout(() => setRedirectAlert(true), 1800);
              if (userData.userRole === 0) {
                setTimeout(() => navigate('/admin/home'), 5000);
                setTimeout(() => setIsClicked(false), 4900);
              } else if (userData.userRole === 1) {
                userData.buildingsAssigned.map(info => {
                   setBuilding({
                      buildingId: info.buildingId,
                      buildingName: info.buildingName,
                      buildingImage: info.buildingImage,
                      buildingAddress: info.buildingAddress
                   })
                })
                setTimeout(() => navigate('/lab-manager'), 5000);
                setTimeout(() => setIsClicked(false), 4900);
              } else {
                userData.buildingsAssigned.map(info => {
                   setBuilding({
                      buildingId: info.buildingId,
                      buildingName: info.buildingName,
                      buildingImage: info.buildingImage,
                      buildingAddress: info.buildingAddress
                   })
                })
                setTimeout(() => navigate('/clerk'), 5000);
                setTimeout(() => setIsClicked(false), 4900);
              }
              setDeactivatedAlert(false);
              toast.success(`Login successful!`);
           } else {
              setDeactivatedAlert(true);
              setIsClicked(false);
           }
      });
  }; 

  const handleLogin = (e) => {
    e.preventDefault();
    setIsClicked(true);
    fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/login`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(result => result.json())
      .then(info => {
        if (info === false) {
          toast.error('Incorrect email or password!');
          setTimeout(() => 1500);
          setTimeout(() => setIsClicked(false), 1000);
        } else {
          localStorage.setItem('token', info.auth);
          UserDetails(info.auth);
          fetchSettingsData();
          
        }
      });
  };

  function Icon() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        fill="currentColor"
        className="h-4 w-4"
      >
        <path
          fillRule="evenodd"
          d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z"
          clipRule="evenodd"
        />
      </svg>
    );
  }

  return (

   <>
     <div className="flex relative flex-col h-screen justify-center items-center bg-defaultColor">
      
       <Card className="w-96">
         <CardHeader
           variant="gradient"
           color="gray"
           className="mb-4 grid h-28 place-items-center"
         >
           <Typography 
             variant="h3" 
             color="white"
             className="uppercase"
             >
             Login
             
           </Typography>
         </CardHeader>
         <CardBody>
         <div className="mb-10 flex flex-col gap-2">
           <Alert
               icon={<Icon />}
               animate={{
                mount: { y: 0 },
                unmount : {y: 1},
               }}
               open={loginAlert}
               className="text-xs border-l-4 border-[#2ec946] bg-[#2ec946]/10 font-medium text-[#2ec946]"
             >
               Logged in as {data.userRole === 0 ? "admin!" : data.userRole === 1 ? "Lab Manager" : "Clerk/Technician"}
             </Alert>
             <Alert
                 icon={<Icon />}
                 animate={{
                  mount: { y: 0 },
                  unmount : {y: 1},
                 }}
                 open={redirectAlert}
                 className="text-xs border-l-4 border-[#2ec946] bg-[#2ec946]/10 font-medium text-[#2ec946]"
               >
                 Redirecting {data.userRole === 0 ? "to admin dashboard ..." : " ..."}
               </Alert>
               <Alert
                   className="bg-red-500"
                   onClose={() => setDeactivatedAlert(false)}
                   open={deactivatedAlert}
                 >
                   Your account has been deactivated!
                 </Alert>
         </div>
         <form className="flex flex-col gap-4" onSubmit={handleLogin}>
           <Input 
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              label="Email"
              size="lg" 
            />
           <Input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)} 
              label="Password"
              size="lg"
            />

            <div className="pt-0 flex flex-row gap-1">
            <Button onClick={BackToHome} variant="text" className="hover:scale-105 outline outline-1 outline-dark/25" >
              <IoMdArrowBack />
            </Button>

            <Button 
              type="submit" 
              className="capitalize justify-center" 
              fullWidth
              disabled={ isClicked ? true : false }
              loading={ isClicked ? true : false}
            >
              Login
            </Button>

            {/*{
              data.id === null ? (
                  
               ) : (
                  <CustomButton
                    fullWidth={true}
                    label={data.userRole === 0 ? "Admin dashboard" : `${building.buildingName !== null ? building.buildingName : "Dashboard"  }`}
                    color="blue"
                    tooltip="hidden"
                    handleClick={() => { data.userRole === 0 ? navigate('/admin/home') : building.buildingId !== null ? navigate(`/lab-manager/${building.buildingId}/home`) : navigate(`/lab-manager`) }}
                  />
               )
            }*/}
              
            </div>
          </form>
         </CardBody>
       </Card>
     </div>

     <ToastContainer position="bottom-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="colored" />
      
   </>
    
  );
}

export default Login