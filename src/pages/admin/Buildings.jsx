import { Layout, CustomButton, InvisibleButton, BuildingCard } from '../../components';
import { useState, useEffect } from 'react';
import { Input, Textarea, Typography } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';

const Buildings = () => {

let navigate = useNavigate();
const [buildings, setBuildings] = useState([]);
const [isCardOpen, setIsCardOpen] = useState(false);
const [isLoading, setIsLoading] = useState(true);
const [name, setName] = useState('');
const [imageUrl, setImageUrl] = useState('');
const [address, setAddress] = useState('');
const [categories, setCategories] = useState([]);
const [isSave, setIsSave] = useState(false);

const handleCardOpen = () => setIsCardOpen(!isCardOpen);

const fetchdata = () => {
			fetch(`${import.meta.env.VITE_API_URL}/admin/building`, {
						method: "GET"
			})
			.then(response => response.json())
			.then(data => {
						if (data.length > 0) {
									setBuildings(data);
									setIsLoading(false);
						} else {
								setBuildings(<div>No data!</div>)
								setIsLoading(false);
						}
			})
}

useEffect(() => {
			fetchdata();
}, [])

const handleClear = () => {
			setName('');
			setImageUrl('');
			setAddress('');
			setCategories([]); 
}

const handleFormSubmit = (e) => {
			e.preventDefault();
			setIsSave(true);
			let buildingData = {
					name,
					imageUrl,
					address
			}

			fetch(`${import.meta.env.VITE_API_URL}/admin/building/add`, {
						method: "POST",
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify(buildingData)
			})
			.then(response => response.json())
			.then(data => {
							if (data === true) {
										toast.success('New data successfully added!');
										fetchdata();
										handleClear();
										setIsSave(false);
							} else {
									toast.error('Error adding new data!');
									setIsSave(false);
							}
			})
			.catch(error => console.error)
}

let formAdd = (
					<form className="flex flex-col gap-3" onSubmit={handleFormSubmit}>
							<Input
										label="Name"
										value={name}
										onChange={(e) => setName(e.target.value)}
										color="teal"
							/>
							<Textarea
										label="Image Url"
										value={imageUrl}
										onChange={(e) => setImageUrl(e.target.value)}
										className="min-h-[40px]"
										color="teal"
							/>
							<div className="-mt-2">
									<Textarea
												label="Address"
												value={address}
												onChange={(e) => setAddress(e.target.value)}
												className="min-h-[40px]"
												color="teal"
									/>
							</div>

							<CustomButton
									type="submit"
									label={!isSave ? "Save" : "Saving..."}
									tooltip="hidden"
									loading={isSave ? true : false}
									isDisabled={isSave ? true : false}
							/>
					</form>
	)

const refresh = () => {
			setIsLoading(true);
			fetchdata();
			setTimeout(() => setIsLoading(false), 6000)
}

 
	  return (
	  			<Layout
	  					title="Buildings"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New Building Form"
	  					children2={formAdd}
	  					childrenClass={isLoading ? "hidden" : ""}
	  					refresh={refresh}
	  					itemNumber={ buildings.length > 0 ? `${buildings.length} items` : "No data" }
	  					copyRight={ isCardOpen ? "hidden" : "" }
	  					// footerText={ <span className="text-red-400 italic">*Note: You can add mutiple user in a building, but you cannot add a user into different building if already added!</span> }
	  			>

				  					<div className={`grid gap-2 ${isCardOpen ? "grid-cols-2" : "grid-cols-3"}`}>
						 							{
						 									buildings.length > 0 ? (
						 													buildings.map((building) => {
						 																return (
						 																			<BuildingCard  
						 																					key={building._id}
						 																					buildingProps={building}
						 																					fetchdata={fetchdata()}
						 																			/>
						 																	)
						 													})
						 										) : (
						 												"No data"
						 										)
						 							}
						 				</div>

	  			</Layout>
	  )
}

export default Buildings