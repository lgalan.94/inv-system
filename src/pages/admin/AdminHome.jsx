import { Layout, CustomButton, AdminHomeCard } from '../../components';
import { useState, useEffect } from 'react';
import { Typography } from '@material-tailwind/react';
import { FaBuilding } from "react-icons/fa";
import { FaUsersGear } from "react-icons/fa6";
import { RiRefreshFill } from "react-icons/ri";

const AdminHome = () => {

const [isLoading, setIsLoading] = useState(true);
const [buildingCount, setBuildingCount] = useState(null);
const [userCount, setUserCount] = useState(null);

let iconClassName = "text-green-300 w-12 h-12 shadow-lg border border-2 border-blue-gray-300 rounded-full p-1"

const fetchUsers = () => {
		fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/list`, {
	  		method: "GET"
	  })
	  .then(response => response.json())
	  .then(data => {
	     if (data.length > 0) {
	     			setUserCount(data.length);
	     			setIsLoading(false)
	     } else {
	     		setUserCount(0)
	     		setIsLoading(false);
	     }
	  })
}

const fetchBuildings = () => {
			fetch(`${import.meta.env.VITE_API_URL}/admin/building`, {
						method: "GET"
			})
			.then(response => response.json())
			.then(data => {
						if (data.length > 0) {
									setBuildingCount(data.length);
									setIsLoading(false);
						} else {
								setBuildingCount(0)
								setIsLoading(false);
						}
			})
}

useEffect(() => {
		fetchBuildings();
		fetchUsers();
		
}, [])

	  return (
	  			<Layout
	  					title="dashboard"
	  					card2="hidden"
	  					loading={ isLoading ? "" : "hidden" }
	  					headerButtons="hidden"
	  					childrenClass={ isLoading ? "hidden":"" }
	  			>

	  				<div className="grid grid-cols-2 gap-3">
	  						<AdminHomeCard
	  								name="Buildings"
	  								icon={<FaBuilding className={iconClassName} />}
	  								link="/admin/buildings"
	  								count={buildingCount === null ? <RiRefreshFill className="animate-spin" /> : buildingCount}
	  						/>
	  						<AdminHomeCard
	  								name="Users"
	  								icon={<FaUsersGear className={iconClassName} />}
	  								link="/admin/users"
	  								count={userCount === null ? <RiRefreshFill className="animate-spin" /> : userCount}
	  						/>
	  				</div>

	  			</Layout>
	  )
}

export default AdminHome