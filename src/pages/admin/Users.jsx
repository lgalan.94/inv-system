import { Layout, CustomButton, UsersList } from '../../components';
import { useState, useEffect } from 'react';
import { Input, Textarea, Select, Option } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useParams } from 'react-router-dom'

const Users = () => {

let no_image = "https://cdn.vectorstock.com/i/preview-1x/82/99/no-image-available-like-missing-picture-vector-43938299.jpg";
const { userId } = useParams();
const [users, setUsers] = useState([]);
const [isCardOpen, setIsCardOpen] = useState(false);
const [isLoading, setIsLoading] = useState(true);
const [name, setName] = useState('');
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [imageUrl, setImageUrl] = useState(no_image);
const [userRole, setUserRole] = useState('');
const [isSave, setIsSave] = useState(false);
const [isDisabled, setIsDisabled] = useState(true);

useEffect(() => {
	name.length > 0 &&
	email.length > 0 &&
	password.length > 0 && 
	imageUrl.length > 0 &&
	userRole !== null ? 
	setIsDisabled(false) : setIsDisabled(true)  
}, [name, email, password, imageUrl, userRole])

const handleCardOpen = () => setIsCardOpen(!isCardOpen);

const fetchUsers = () => {
		fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/list`, {
	  		method: "GET"
	  })
	  .then(response => response.json())
	  .then(data => {
	  			data.sort((a, b) => a.userRole - b.userRole);
	     if (data.length > 0) {
	     			setUsers(data);
	     			setIsLoading(false)
	     } else {
	     		setUsers(<div>No data</div>)
	     		setIsLoading(false);
	     }
	  }) 
}

useEffect(() => {
		fetchUsers();
}, [])

const handleClearInputs = () => {
		setName('');
		setEmail('');
		setPassword('');
		setImageUrl('')
		setIsSave(false);
}

const handleFormSubmit = (e) => {
			e.preventDefault();
			setIsSave(true);
			setIsDisabled(true);
			let userData = {
						name,
						email,
						password,
						userRole,
						imageUrl
			}
				console.log(imageUrl)
			fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/register`, {
					method: "POST",
					headers: {
							'Content-type': 'application/json'
					},
					body:JSON.stringify(userData)
			})
			.then(response => response.json())
			.then(data => {
						if (data === true) {
								toast.success('New user successfully added to the system!');
								fetchUsers();
								handleClearInputs();
								setImageUrl(no_image);
								setIsSave(false);
						} else if (data.email === email) {
								toast.error("Email already taken!");
								setIsSave(false);
						} else {
								toast.error('Unable to add item');
								setIsSave(false);
						}
			}) 
}

let formAdd = (
					<form className="flex flex-col gap-3" onSubmit={handleFormSubmit}>
							<Input
										label="Name"
										value={name}
										onChange={(e) => setName(e.target.value)}
										color="teal"
										className="capitalize"
							/>
							<Input
										label="Email"
										type="email"
										value={email}
										onChange={(e) => setEmail(e.target.value)}
										color="teal"
							/>
							<Input
										label="Password"
										value={password}
										onChange={(e) => setPassword(e.target.value)}
										color="teal"
							/>
							<Select label="Select User Role" onChange={(e) => setUserRole(e)} >
							  <Option value="0" disabled>Admininistrator</Option>
							  <Option value="1">Lab Manager</Option>
							  <Option value="2">Inventory Clerk/Technician</Option>
							</Select>
							<Textarea
										label="Image Url"
										value={imageUrl}
										onChange={(e) => setImageUrl(e.target.value)}
										color="teal"
										className="text-xs min-h-[70px]"
							/>
							<img
							  src={imageUrl}
							  className="w-20 mx-auto"
							/>

							<div className="flex flex-row justify-center gap-2 mt-6">
									<CustomButton
											handleClick={handleClearInputs}
											label="Clear"
											tooltip="hidden"

									/>
									<CustomButton
											type="submit"
											label={!isSave ? "Save" : "Saving..."}
											tooltip="hidden"
											loading={isSave ? true : false}
											isDisabled={isDisabled}
											color="green"
									/>
							</div>
					</form>
	)

const refresh = () => {
		setIsLoading(true);
		fetchUsers();
		setTimeout(() => setIsLoading(false), 5000)
}

	  return (
	  			<Layout
	  					title="Users"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New User"
	  					children2={formAdd}
	  					childrenClass={ isLoading ? "hidden" : "" }
	  					refresh={refresh}
	  					itemNumber={ users.length > 0 ? `${users.length} users` : "No data" }
	  					copyRight={ isCardOpen ? "hidden" : "" }
	  			>

	  					<div className={`grid gap-3 ${isCardOpen ? "grid-cols-2" : "grid-cols-3"}`}>
	  						{
	  								users.length > 0 ? (
	  												users.map(user => {
	  														return (
	  																		<UsersList  
	  																				key={user._id} 
	  																				userProps={user}
	  																				fetchData={fetchUsers}
	  																		/>
	  															)
	  												})
	  									) : (
	  											"No Data"
	  									)
	  						}
	  					</div>

	  			</Layout>
	  )
}

export default Users