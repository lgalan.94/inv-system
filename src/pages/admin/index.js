import AdminHome from './AdminHome';
import Buildings from './Buildings';
import Users from './Users';
import Settings from './Settings';
import ManageBuilding from './ManageBuilding';

export {
	AdminHome,
	Buildings,
	Users,
	Settings,
	ManageBuilding,
}