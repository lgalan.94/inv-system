import { Layout, CustomButton, SettingsCard } from '../../components';
import { useState, useEffect } from 'react';

const Settings = () => {

const [isLoading, setIsLoading] = useState(true);
const [settingsData, setSettingsData] = useState([]);

const fetchSettingsData = () => {
		fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings`, {
				method: "GET"
		})
		.then(response => response.json())
		.then(data => {
				if (data.length > 0) {
						setSettingsData(data);
						setIsLoading(false);
				} else {
						setSettingsData([]);
						setIsLoading(false);
				} 
		})
		.catch(error => console.error)
}

useEffect(() => {
		fetchSettingsData()
}, [])

	  return (
	  			<Layout
	  					title="Settings"
	  					card2="hidden"
	  					loading={ isLoading ? "" : "hidden" }
	  					headerButtons="hidden"
	  					childrenClass={ isLoading ? "hidden" : "" }
	  			>
	  			{
	  					settingsData.length > 0 ? (
	  								<div className="grid grid-cols-2 gap-2">
	  								 	{settingsData.map((item) => (
  								 				<SettingsCard
  								 						fetchdata={fetchSettingsData()}
  								 						key={item._id}
  								 						SettingsProps={item}
  								 				/>
  								 		))}
	  								</div>
	  						) : (
	  								<p>No data</p>
	  						)
	  			}
	  			</Layout>
	  )
}

export default Settings