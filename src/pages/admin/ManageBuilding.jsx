import { Layout, CustomButton, CustomDrawer } from '../../components';
import { InventoryCard, Items, AddItem, Categories, StorageLocations, AddCategory, AddStorageLocation } from '../../components/admin';
import { useState, useEffect } from 'react';
import { Input, Textarea, Select, Option, Typography } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useParams, useNavigate } from 'react-router-dom'
import { IoArrowBack } from "react-icons/io5";

const ManageBuilding = () => {

let no_image = "https://cdn.vectorstock.com/i/preview-1x/82/99/no-image-available-like-missing-picture-vector-43938299.jpg";
let navigate = useNavigate();

const [buildingInfo, setBuildingInfo] = useState([]);
const [isLoading, setIsLoading] = useState(true);

	const { buildingId } = useParams();
	const [name, setName] = useState('');
	const [address, setAddress] = useState('');
	const [image, setImage] = useState('');
	const [categories, setCategories] = useState([]);
	const [items, setItems] = useState([]);
	const [locations, setLocations] = useState([]);
	const [users, setUsers] = useState([]); 

/*fetch building info*/
	const [locationItems, setLocationItems] = useState([]);
	const [categoryItems, setCategoryItems] = useState([]);
	
	const fetchBuildingInfo = () => {
			fetch(`${import.meta.env.VITE_API_URL}/admin/building/${buildingId}`, {
									method: "GET"
						})
						.then(response => response.json())
						.then(data => {
									setName(data.name);
									setAddress(data.address);
									setImage(data.imageUrl);
									setCategories(data.categories);
									setItems(data.items);
									setLocations(data.rooms);

									setLocationItems(data.rooms.map((item) => (
												<Option key={item._id} value={item.roomName} > {item.roomName.toUpperCase()} </Option>
										)))
									setCategoryItems(data.categories.map((item) => (
												<Option key={item._id} value={item.categoryName} > {item.categoryName.toUpperCase()} </Option>
										)))

									setTimeout(() => setIsLoading(false), 2000);
				})
						.catch(error => console.error)
	}

	useEffect(() => {
				fetchBuildingInfo();
	}, [])
/**/

/*add item states*/
	let url = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMtaN6L0FkLLyNRmuaul-Vdh6MIt9P5M5bqpihnmWpzw&s';
	const [itemName, setItemName] = useState('');
	const [description, setDescription] = useState('');
	const [quantity, setQuantity] = useState('');
	const [location, setLocation] = useState(null);
	const [category, setCategory] = useState(null);
	const [itemImage, setItemImage] = useState(url);
	const [lowStockThreshold, setLowStockThreshold] = useState('');
	const [isItemDeleted, setIsItemDeleted] = useState(false);
	const [isItemSave, setIsItemSave] = useState(false);
/**/

	// add category
	const [categoryName, setCategoryName] = useState('');
	const [categoryImage, setCategoryImage] = useState(url);
	const [isCategorySave, setIsCategorySave] = useState(false);
	const [isDisabled, setIsDisabled] = useState(true);
	
	// add storage location
	const [roomName, setRoomName] = useState('');
	const [isRoomSave, setIsRoomSave] = useState(false);

/*handle add items, categories, & rooms*/
	const [addItemDrawer, setAddItemDrawer] = useState(false);
	const [addCategoryDrawer, setAddCategoryDrawer] = useState(false);
	const [addLocationDrawer, setAddLocationDrawer] = useState(false);
	const [deleteDialogBox, setDeleteDialogBox] = useState(false);
	const handleAddItemDrawer = () => setAddItemDrawer(!addItemDrawer);
	const handleAddCategoryDrawer = () => setAddCategoryDrawer(!addCategoryDrawer);
	const handleAddLocationDrawer = () => setAddLocationDrawer(!addLocationDrawer);
	const handleDeleteDialogBox = () => setDeleteDialogBox(!deleteDialogBox);
/**/

/*handle change item data input*/
	const handleNameChange = (e) => setItemName(e.target.value);
	const handleDescriptionChange = (e) => setDescription(e.target.value);
	const handleQuantityChange = (e) => setQuantity(e.target.value);
	const handleLocationChange = (e) => setLocation(e);
	const handleCategoryChange = (e) => setCategory(e);
	const handleLowStockThresholdChange = (e) => setLowStockThreshold(e.target.value);
	const handleItemImageChange = (e) => setItemImage(e.target.value);
/**/

/*handle add item api*/
	let itemData = {
			itemName: itemName,
			quantity: quantity,
			lowStockThreshold: lowStockThreshold,
			location: location,
			category: category,
			imageUrl: itemImage,
			description: description
	}
		const handleAddItem = (e) => {
				e.preventDefault();
				setIsItemSave(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/${buildingId}/add-item`, {
						method: 'POST',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify(itemData)
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${itemName.toUpperCase()} Successfully Added!`);
										fetchBuildingInfo();
										setTimeout(() => {
												setIsItemSave(false)
												setAddItemDrawer(false)
										}, 3500);
										clearFields();
							} else {
										toast.error('Unable to add new item!')
										setIsItemSave(false);
							}
				})
		}

		const clearFields = () => {
				setItemName("");
				setDescription("");
				setLowStockThreshold("");
				setQuantity("");
				setLocation([]);
				setCategory([]);
				setItemImage(url);
		}
/**/

		/*handle add category*/
				const handleAddCategory = (e) => {
						e.preventDefault();
						setIsCategorySave(true);
						setIsDisabled(true);
						fetch(`${import.meta.env.VITE_API_URL}/ulafis/category/${buildingId}/add-category`, {
								method: 'POST',
								headers: {
										'Content-type': 'application/json'
								},
								body: JSON.stringify({
										categoryName: categoryName.toUpperCase(),
										imageUrl: categoryImage
								})
						})
						.then(response => response.json())
						.then(data => {
									if (data === true) {
												toast.success(`${categoryName} Successfully added!`);
												fetchBuildingInfo();
												setTimeout(() => {
														setAddCategoryDrawer(false)
														setIsCategorySave(false)
												}, 3500)
												setIsDisabled(true);
												setCategoryName('');
												setCategoryImage(url);
									} else {
												toast.error('Unable to add category!');
												setAddCategoryDrawer(false);
												setIsCategorySave(false);
												setIsDisabled(true);
									}
						})
						.catch(error => console.error(error))
				}

				useEffect(() => {
						categoryName.length > 0 &&
						categoryImage.length > 0  ? 
						setIsDisabled(false) : setIsDisabled(true);
				}, [categoryName, categoryImage])

		/**/

		/*handle add storage location*/
				const handleAddLocation = (e) => {
						e.preventDefault();
						setIsRoomSave(true);
						setIsDisabled(true);
						fetch(`${import.meta.env.VITE_API_URL}/ulafis/room/${buildingId}/add-room`, {
								method: 'POST',
								headers: {
										'Content-type': 'application/json'
								},
								body: JSON.stringify({ roomName: roomName.toUpperCase() })
						})
						.then(response => response.json())
						.then(data => {
									if (data === true) {
												toast.success(`${roomName.toUpperCase()} Successfully added!`);
												fetchBuildingInfo();
												setTimeout(() => {
														setIsRoomSave(false)
														setAddLocationDrawer(false)
												}, 3500);
												setRoomName('');
												setIsDisabled(true);	
									} else {
												toast.error('Unable to add category!')
												setRoomName('');
												setIsDisabled(true);
												setIsRoomSave(false);
									}
						})
						.catch(error => console.error(error))
				}

				useEffect(() => {
						roomName.length > 2 ? setIsDisabled(false) : setIsDisabled(true)
				}, [roomName])

		/**/


const refresh = () => {
		setIsLoading(true);
		fetchBuildingInfo();
}

const backButton = (
				<CustomButton
						icon={<IoArrowBack className="w-4 h-4" />}
						tooltip="hidden"
						handleClick={() => navigate('/admin/buildings')}
						variant="text"
				/>
	)

/*filter item*/
	const [searchTerm, setSearchTerm] = useState('');
	const [filteredData, setFilteredData] = useState(items);

	const handleSearchChange = (event) => {
	  setSearchQuery(event.target.value);
	};

	const handleInputSearchChange = (event) => {
	    const { value } = event.target;
	    setSearchTerm(value);
	    filterData(value);
	  };


	const filterData = (searchTerm) => {
	  const filteredData = items.filter((item) =>
	    item.itemName.toLowerCase().includes(searchTerm.toLowerCase())
	  );
	  setFilteredData(filteredData);
	};
/**/	

	  return (
	  			<Layout
	  					backButton={backButton}
	  					title={name}
	  					loading={ isLoading ? "" : "hidden" }
	  					childrenClass={ isLoading ? "hidden" : "" }
	  					copyRight="hidden"
	  					cardBody="p-4"
	  					refresh={refresh}
	  					headerButtons="hidden"
	  					card2="hidden"
	  			>

	  				<InventoryCard
	  						handleAddItemDrawer={handleAddItemDrawer}
	  						handleAddCategoryDrawer={handleAddCategoryDrawer}
	  						handleAddLocationDrawer={handleAddLocationDrawer}
	  						handleInputSearchChange={handleInputSearchChange}
	  						itemCount={
	  								searchTerm.length > 0 ? (
	  											filteredData.length > 0 ? (
	  														`${filteredData.length} items`
	  												) : (
	  														"No data"
	  												)
	  									) : (
	  											`${items.length} items`
	  									)
	  						}
	  						items={
	  									searchTerm.length > 0 ? (
	  														filteredData.length > 0 ? (
	  																		filteredData.map((item, index) => (
	  																						<Items
	  																								key={item._id}
	  																								itemProps={item}
	  																								buildingId={buildingId}
	  																								fetchData={fetchBuildingInfo}
	  																								count={index + 1}
	  																						/>
	  																			))
	  															) : (
	  																	"Nothing found"
	  															)
	  										) : (
	  												items.map((item, index) => (
	  																<Items
	  																		key={item._id}
	  																		itemProps={item}
	  																		buildingId={buildingId}
	  																		fetchData={fetchBuildingInfo}
	  																		count={index + 1}
	  																/>
	  													))
	  										)
	  						}
	  						categories={
	  									categories.map((category, index) => (
	  													<Categories
	  															key={category._id}
	  															itemProps={category}
	  															buildingId={buildingId}
	  															fetchData={fetchBuildingInfo}
	  															count={index + 1}
	  													/>
	  										))
	  						}
	  						rooms={
	  									locations.map((location, index) => (
	  													<StorageLocations
	  															key={location._id}
	  															itemProps={location}
	  															buildingId={buildingId}
	  															fetchData={fetchBuildingInfo}
	  															count={index + 1}
	  													/>
	  										))
	  						}
	  						name={name}
	  						address={address}
	  						imageUrl={image}
	  				/>

	  				{/*add item drawer*/}
	  					<CustomDrawer
	  							title="Add Item"
	  							open={addItemDrawer}
	  							handleOpenDrawer={handleAddItemDrawer}
	  							handleCloseDrawer={handleAddItemDrawer}
	  							className={ addItemDrawer ? "min-w-[40vw]" : "" }
	  					>

	  						<div className="px-4 py-2">
	  								<AddItem 
	  										categoryItems={categoryItems}
	  										locationItems={locationItems} 
	  										itemName={itemName}
	  										description={description}
	  										quantity={quantity}
	  										location={location}
	  										category={category}
	  										imageUrl={itemImage}
	  										lowStockThreshold={lowStockThreshold}
	  										setItemName={handleNameChange}
	  										setDescription={handleDescriptionChange}
	  										setQuantity={handleQuantityChange}
	  										setLocation={handleLocationChange}
	  										setCategory={handleCategoryChange}
	  										setImageUrl={handleItemImageChange}
	  										setLowStockThreshold={handleLowStockThresholdChange}
	  										handleAdd={handleAddItem}
	  										isSave={isItemSave}
	  								/>
	  						</div>

	  					</CustomDrawer>
	  				{/**/}
	  				{/*add category drawer*/}
	  						<CustomDrawer
	  								title="Add Category"
	  								open={addCategoryDrawer}
	  								handleOpenDrawer={handleAddCategoryDrawer}
	  								handleCloseDrawer={handleAddCategoryDrawer}
	  								className={ addCategoryDrawer ? "min-w-[35vw]" : "" }
	  						>

	  							<div className="px-4 py-2">
	  									<AddCategory 
	  											categoryName={categoryName}
	  											imageUrl={categoryImage}
	  											setCategoryName={(e) => setCategoryName(e.target.value)}
	  											setImageUrl={(e) => setCategoryImage(e.target.value)}
	  											handleAddCategory={handleAddCategory}
	  											isSave={isCategorySave}
	  											isDisabled={isDisabled}
	  									/>
	  							</div>

	  						</CustomDrawer>
	  				{/**/}

	  				{/*add storage location drawer*/}
	  						<CustomDrawer
	  								title="Add Storage Location"
	  								open={addLocationDrawer}
	  								handleOpenDrawer={handleAddLocationDrawer}
	  								handleCloseDrawer={handleAddLocationDrawer}
	  								className={ addLocationDrawer ? "min-w-[25vw]" : "" }
	  						>

	  							<div className="px-4 py-2">
	  									<AddStorageLocation 
	  											locationName={roomName}
	  											setLocationName={(e) => setRoomName(e.target.value)}
	  											handleAddLocation={handleAddLocation}
	  											isSave={isRoomSave}
	  											isDisabled={isDisabled}
	  									/>
	  							</div>

	  						</CustomDrawer>
	  				{/**/}

	  			</Layout>
	  )
}

export default ManageBuilding