import { Layout, CustomButton } from '../../components';
import { SampleCard } from '../../components/labManager';
import { useState, useEffect, useContext } from 'react';
import DataContext from '../../DataContext';
import { Input, Textarea, Typography, Select, Option } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Samples = () => { 

const { building } = useContext(DataContext);
const TABLE_HEAD = ["#", "Name", "Location", "Description", ""];
const [isCardOpen, setIsCardOpen] = useState(false);
const [isLoading, setIsLoading] = useState(true);
const [isSave, setIsSave] = useState(false);
const [isDisabled, setIsDisabled] = useState(true);

const [samples, setSamples] = useState([]);
const [storageLocations, setStorageLocations] = useState([]);

const [sampleName, setSampleName] = useState('');
const [description, setDescription] = useState('');
const [location, setLocation] = useState('');

const handleCardOpen = () => setIsCardOpen(!isCardOpen);

/*handle add*/
		const handleAdd = (e) => {
				e.preventDefault();
				setIsSave(true);
				setIsDisabled(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/sample/${building.buildingId}/add-sample`, {
						method: 'POST',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify({ 
							sampleName,
							description,
							location 
						})
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${sampleName.toUpperCase()} Successfully added!`)
										fetchSamples();
										setSampleName('');
										setDescription('');
										setLocation('');
										setIsDisabled(true);
										setIsSave(false);
							} else {
										toast.error('Unable to add category!')
										setRoomName('');
										setIsDisabled(true);
										setIsSave(false);
							}
				})
		}
/**/

const fetchSamples = () => {
  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
    method: "GET"
  })
  .then(response => response.json())
  .then(result => {
    	if (result.samples.length > 0) {
    		setSamples(result.samples);
    		setIsLoading(false);
    	} else {
    			setSamples([]);
    			setIsLoading(false)
    	}
    	setStorageLocations(result.samplesStorage);
  })
  .catch(error => console.error);
}

const refresh = () => {
		setIsLoading(true);
		fetchSamples();
}

		useEffect(() => {
				fetchSamples();
		}, [])

		useEffect(() => {
				sampleName.length > 0 &&
				description.length > 0 &&
				location.length > 0 ? setIsDisabled(false) : setIsDisabled(true) 
		}, [sampleName, description, location])

const addSample = (
					<form className="flex flex-col gap-7">
							<div className="flex flex-col gap-2">
							  <Input
							    type="text"
							    className="capitalize"
							    value={sampleName}
							    onChange={(e) => setSampleName(e.target.value)}
							    label="Sample Name"
							    color="teal"
							  />
							  <Select onChange={(e) => setLocation(e)} label="Select Storage Location" >
							  		{
							  				storageLocations.map((storage) => (
							  								<Option key={storage._id} value={storage.storageName}> {storage.storageName} </Option>
							  					))
							  		}
							  </Select>
							  <Textarea
							  		value={description}
							  		onChange={(e) => setDescription(e.target.value)}
							    label="Description"
							    color="teal"
							  />
							  
							</div>
							<CustomButton
									label={!isSave ? "Save" : "Saving..."}
									tooltip="hidden"
									color="green"
									loading={isSave ? true : false}
									isDisabled={isDisabled}
									handleClick={handleAdd}
							/>
					</form>
	)

	  return (
	  			<Layout
	  					title="Samples"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New Sample"
	  					childrenClass={ isLoading ? "hidden" : "" }
	  					itemNumber={samples.length > 0 ? `${samples.length} items` : "No data"}
	  					copyRight={ isCardOpen ? "hidden" : "" }
	  					children2={addSample}
	  					cardBody="p-0"
	  					refresh={refresh}
	  			>


	  				<table className="w-full min-w-max table-auto text-left">
	  				  <thead className="sticky top-0 shadow uppercase font-bold">
	  				    <tr>
	  				      {TABLE_HEAD.map((head) => (
	  				        <th
	  				          key={head}
	  				          className="border-b border-blue-gray-100 bg-blue-gray-50 first:px-1 py-4"
	  				        >
	  				          <Typography
	  				            variant="small"
	  				            color="blue-gray"
	  				            className="font-normal leading-none opacity-70"
	  				          >
	  				            {head}
	  				          </Typography>
	  				        </th>
	  				      ))}
	  				    </tr>
	  				  </thead>
	  				  <tbody>

	  				  	{samples.length > 0 ? (
	  				  				samples.map((sample, index) => (
	  				  							<SampleCard
	  				  								fetchData={fetchSamples}
	  				  								key={sample._id}
	  				  								props={sample}
	  				  								count={index + 1}
	  				  							/>
	  				  					))
	  				  		) : (
	  				  			"No data"
	  				  		)}

	  				  </tbody>
	  				</table>


	  			</Layout>
	  )
} 

export default Samples