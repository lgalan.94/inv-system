import { Layout, CustomButton, HomeCard } from '../../components';
import { useState, useEffect, useContext } from 'react';
import { Typography } from '@material-tailwind/react';
import { RiRefreshFill } from "react-icons/ri";
import DataContext from '../../DataContext';
import { TbCategoryPlus } from "react-icons/tb";
import { MdOutlineInventory } from "react-icons/md";
import { MdOutlineMeetingRoom } from "react-icons/md";

const Home = () => {

const { building } = useContext(DataContext);
const [isLoading, setIsLoading] = useState(true);
const [itemsCount, setItemsCount] = useState(null);
const [categoryCount, setCategoryCount] = useState(null);
const [storageCount, setStorageCount] = useState(null);

let iconClassName = "text-green-300 w-12 h-12 shadow-lg border border-2 border-blue-gray-300 rounded-full p-1"

			const fetchData = () => {
			  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
			    method: "GET"
			  })
			  .then(response => response.json())
			  .then(result => {
			    		setItemsCount(result.items.length);
			    		setCategoryCount(result.categories.length);
			    		setStorageCount(result.rooms.length);
			    		setIsLoading(false);
			  })
			  .catch(error => console.error);
			}

	useEffect(() => {
			fetchData();
	}, [])

	  return (
	  			<Layout
	  					title="dashboard"
	  					card2="hidden"
	  					loading={ isLoading ? "" : "hidden" }
	  					headerButtons="hidden"
	  					childrenClass={ isLoading ? "hidden":"" }
	  			>

	  				<div className="grid grid-cols-3 gap-2">
	  						<HomeCard
	  								name="Inventory Items"
	  								icon={<MdOutlineInventory className={iconClassName} />}
	  								link={`/lab-manager/${building.buildingId}/inventory`}
	  								count={itemsCount === null ? <RiRefreshFill className="animate-spin" /> : itemsCount}
	  						/>
	  						<HomeCard
	  								name="Categories"
	  								icon={<TbCategoryPlus className={iconClassName} />}
	  								link={`/lab-manager/${building.buildingId}/categories`}
	  								count={categoryCount === null ? <RiRefreshFill className="animate-spin" /> : categoryCount}
	  						/>
	  						<HomeCard
	  								name="Storage Locations"
	  								icon={<MdOutlineMeetingRoom className={iconClassName} />}
	  								link={`/lab-manager/${building.buildingId}/storage-locations`}
	  								count={categoryCount === null ? <RiRefreshFill className="animate-spin" /> : storageCount}
	  						/>
	  				</div>

	  			</Layout>
	  )
}

export default Home