import { Layout, CustomButton, InvisibleButton, CustomDrawer, AddItemComponent, CustomAlert, CustomDialogBox, InventoryItemCard } from '../../components';
import { useState, useEffect, useContext } from 'react';
import { Typography } from '@material-tailwind/react';
import { Tooltip, Input, Textarea } from '@material-tailwind/react';
import { useNavigate } from 'react-router-dom';
import { IoArrowBack } from "react-icons/io5";
import { LuSaveAll } from "react-icons/lu";
import { RiRefreshFill } from "react-icons/ri";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import DataContext from '../../DataContext';

const Inventory = () => {

	const { building, buildingData } = useContext(DataContext);

	const [inventoryItems, setInventoryItems] = useState([]);

/*handle add new item*/
	let url = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMtaN6L0FkLLyNRmuaul-Vdh6MIt9P5M5bqpihnmWpzw&s';

	let navigate = useNavigate();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [quantity, setQuantity] = useState('');
	const [location, setLocation] = useState('');
	const [category, setCategory] = useState();
	const [image_url, set_image_url] = useState(url);
	const [low_stock_threshold, set_low_stock_threshold] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isClicked, setIsClicked] = useState(false);
	const [nameAlert, setNameAlert] = useState(false);
	const [descriptionAlert, setDescriptionAlert] = useState(false);
	const [quantityAlert, setQuantityAlert] = useState(false);
	const [lowStockAlert, setLowStockAlert] = useState(false);
	const [locationAlert, setLocationAlert] = useState(false);
	const [categoryAlert, setCategoryAlert] = useState(false);
	const [imageUrlAlert, setImageUrlAlert] = useState(false);
	const [thresholdAlert, setThresholdAlert] = useState(false);
	const [loading, setLoading] = useState(true);
	const [errorNameInput, setErrorNameInput] = useState(false);
	const [errorQuantityInput, setErrorQuantityInput] = useState(false);
	const [errorLSTInput, setErrorLSTInput] = useState(false);
	const [errorCategorySelect, setErrorCategorySelect] = useState(false);
	const [errorLocationInput, setErrorLocationInput] = useState(false);
	const [errorImageUrlInput, setErrorImageUrlInput] = useState(false);
	const [errorDescriptionInput, setErrorDescriptionInput] = useState(false);
	const TABLE_HEAD = ["Image", "Name", "Category", "Description", ""];
	const [isCardOpen, setIsCardOpen] = useState(false);
	const [isLoading, setIsLoading] = useState(true);

	const handleCardOpen = () => setIsCardOpen(!isCardOpen);

	/*handle add*/
		const handleAdd = (e) => {
				e.preventDefault();
				setIsClicked(true);
				if (name.length < 1) {
								setNameAlert(true)
								setErrorNameInput(true);
				}
				if (quantity.length < 1) {
								setQuantityAlert(true)
								setErrorQuantityInput(true);
				}
				if (low_stock_threshold.length < 1) {
								setLowStockAlert(true)
								setErrorLSTInput(true);
				}
				if (location.length < 1) {
								setLocationAlert(true)
								setErrorLocationInput(true);
				}
				if (category === undefined) {
								setCategoryAlert(true)
								setErrorCategorySelect(true);
				}
				if (image_url.length < 1) {
								setImageUrlAlert(true)
								setErrorImageUrlInput(true);
				}
				if (description.length < 1) {
								setDescriptionAlert(true)
								setErrorDescriptionInput(true);
				}

				fetch(`${import.meta.env.VITE_API_URL}/ulafis/${building.buildingId}/add-item`, {
						method: 'POST',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify({
								itemName: name.toUpperCase(),
								quantity: quantity,
								lowStockThreshold: low_stock_threshold,
								location: location,
								category: category,
								imageUrl: image_url,
								description:description
						})
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${name.toUpperCase()} Successfully Added!`);
										setIsClicked(false);
										setIsCardOpen(false);
										fetchData();
										handleClear();
							} else {
										toast.error('Unable to add new item!')
										setIsClicked(false);
							}
				})
		}
	/**/

	/*handle alerts*/
			useEffect(() => {
						if (name.length >= 1) {
									setNameAlert(false)
									setErrorNameInput(false)
						}
			}, [name])
			useEffect(() => {
						if (description.length >= 1) {
									setDescriptionAlert(false)
									setErrorDescriptionInput(false)
						}
			}, [description])
			useEffect(() => {
						if (low_stock_threshold.length >= 1) {
									setLowStockAlert(false)
									setErrorLSTInput(false)
						}
			}, [low_stock_threshold])
			useEffect(() => {
						if (location.length >= 1) {
									setLocationAlert(false)
									setErrorLocationInput(false)
						}
			}, [location])
			useEffect(() => {
						if (category !== undefined) {
									setCategoryAlert(false)
									setErrorCategorySelect(false)
						}
			}, [category])
			useEffect(() => {
						if (image_url.length >= 1) {
									setImageUrlAlert(false)
									setErrorImageUrlInput(false)
						}
			}, [image_url])
			useEffect(() => {
						if (quantity.length >= 1) {
									setQuantityAlert(false)
									setErrorQuantityInput(false)
						}
			}, [quantity])

			const handleNameChange = (e) => {
				setName(e.target.value);
			}
			const handleDescriptionChange = (e) => {
				setDescription(e.target.value);
			}
			const handleQuantityChange = (e) => {
				setQuantity(e.target.value);
			}
			const handleLocationChange = (e) => {
				setLocation(e.target.value);
			}
			const handleCategoryChange = (e) => {
				setCategory(e);
			}
			const handleImageUrlChange = (e) => {
				set_image_url(e.target.value);
			}
			const handleLowStockThreshold = (e) => {
				set_low_stock_threshold(e.target.value);
			}
	/**/
	
	const handleClear = () => {
			setName('');
			setDescription('');
			setQuantity('');
			setLocation('');
			setCategory();
			set_image_url('');
			set_low_stock_threshold('');
	}

	useEffect(() => {
	  if (parseInt(low_stock_threshold) >= parseInt(quantity)) {
	    setThresholdAlert(true);
	    setErrorLSTInput(true);
	    setIsDisabled(true);
	  } else {
	    setThresholdAlert(false);
	    setErrorLSTInput(false);
	    setIsDisabled(false);
	  }
	}, [low_stock_threshold, quantity]);

	const refresh = () => {
			setIsLoading(true);
			setTimeout(() => setIsLoading(false), 4000)
			setName('');
			setDescription('');
			setQuantity('');
			setLocation('');
			set_low_stock_threshold('');
			set_image_url(url);
			setNameAlert(false);
			setQuantityAlert(false);
			setLowStockAlert(false);
			setCategoryAlert(false);
			setLocationAlert(false);
			setImageUrlAlert(false);
			setDescriptionAlert(false);

			setThresholdAlert(false);
	  setErrorLSTInput(false);
	  setQuantityAlert(false)
			setErrorQuantityInput(false)
			setImageUrlAlert(false)
			setErrorImageUrlInput(false)
			setCategoryAlert(false)
			setErrorCategorySelect(false)
			setLocationAlert(false)
			setErrorLocationInput(false)
			setDescriptionAlert(false)
			setErrorDescriptionInput(false)
			setNameAlert(false)
			setErrorNameInput(false)
	}	
/*********************/

/*handle fetch data from db*/
const fetchData = () => {
			fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
					method: 'GET',
					headers: {
							'Content-type': 'application/json'
					}
			})
			.then(response => response.json())
			.then(result => {
						if (result.items.length > 0) {
								setInventoryItems(result.items.map(item => {
									return (
												<InventoryItemCard key={item._id} itemProps={item} />
										)
								}))
								setIsLoading(false);
						} else {
								setInventoryItems("No data")
								setIsLoading(false);
						}				
			})
}

useEffect(() => {
			fetchData();
}, [])

	  return (
	  			<Layout
	  					title="Items"
	  					refresh={refresh}
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					childrenClass={ isLoading ? "hidden" : "" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add Item Form"
	  					cardBody="p-0"
	  					children2={
	  								<>
	  										<AddItemComponent
	  											imageUrl={image_url}
	  											name={name.toUpperCase()}
	  											description={description}
	  											quantity={quantity}
	  											location={location}
	  											category={category}
	  											categoryLabel="Select Category"
	  										
	  											image_url={image_url}
	  											lowStockThreshold={low_stock_threshold}

	  											handleNameChange={handleNameChange}
	  											handleDescriptionChange={handleDescriptionChange}
	  											handleQuantityChange={handleQuantityChange}
	  											handleLocationChange={handleLocationChange}
	  											handleCategoryChange={handleCategoryChange}
	  											handleImageUrlChange={handleImageUrlChange}
	  											handleLowStockThreshold={handleLowStockThreshold}

	  											errorNameInput={errorNameInput}
	  											errorQuantityInput={errorQuantityInput}
	  											errorLSTInput={errorLSTInput}
	  											errorCategorySelect={errorCategorySelect}
	  											errorLocationInput={errorLocationInput}
	  											errorImageUrlInput={errorImageUrlInput}
	  											errorDescriptionInput={errorDescriptionInput}

	  											displayAlert={
	  														<>
	  															<CustomAlert
	  																	openAlert={nameAlert}
	  																	alertLabel="Name is required!"
	  															/>
	  															<CustomAlert
	  																	openAlert={descriptionAlert}
	  																	alertLabel="Description is required!"
	  															/>
	  															<CustomAlert
	  																	openAlert={quantityAlert}
	  																	alertLabel="Quantity is required!"
	  															/>
	  															<CustomAlert
	  																	openAlert={lowStockAlert}
	  																	alertLabel="Low Stock Threshold is required!"
	  															/>
	  															<CustomAlert
	  																	openAlert={locationAlert}
	  																	alertLabel="Location is required!"
	  															/>
	  															<CustomAlert
	  																	openAlert={categoryAlert}
	  																	alertLabel="Category is required!"
	  															/>
	  															<CustomAlert
	  																	openAlert={imageUrlAlert}
	  																	alertLabel="Image Url is required!"
	  															/>
	  															<CustomAlert
	  																	openAlert={thresholdAlert}
	  																	alertLabel="Low Stock Threshold value must be lower than the number of quantity! "
	  															/>
	  														</>
	  											}

	  										/>
	  										<div className="mt-6 flex flex-row gap-1 justify-center mx-auto">
	  										    
	  										    <CustomButton
	  										      label="Clear"
	  										      handleClick={handleClear}
	  										      btnClass="rounded px-3 py-2 hover:scale-105"
	  										      tooltip="hidden"
	  										    />
	  										    <CustomButton
	  										      label="Save"
	  										      handleClick={handleAdd}
	  										      btnClass="rounded px-3 py-2 hover:scale-105"
	  										      color="green"
	  										      isDisabled={isClicked ? true : false}
	  										      tooltip="hidden"
	  										      loading={isClicked ? true : false}
	  										    />
	  										</div>
	  								</>
	  					}
	  					itemNumber={inventoryItems === "No data" ? inventoryItems : `${inventoryItems.length} items`}
	  					copyRight={isCardOpen ? "hidden" : ""}
	  			>
	  				<table className="w-full min-w-max table-auto text-left">
	  				  <thead className="sticky top-0 shadow uppercase font-bold">
	  				    <tr>
	  				      {TABLE_HEAD.map((head) => (
	  				        <th
	  				          key={head}
	  				          className="border-b border-blue-gray-100 bg-blue-gray-50 first:px-1 py-4"
	  				        >
	  				          <Typography
	  				            variant="small"
	  				            color="blue-gray"
	  				            className="font-normal leading-none opacity-70"
	  				          >
	  				            {head}
	  				          </Typography>
	  				        </th>
	  				      ))}
	  				    </tr>
	  				  </thead>
	  				  <tbody>
	  				   	{inventoryItems}
	  				  </tbody>
	  				</table>

	  			</Layout>
	  )
}

export default Inventory