import { Layout, CustomButton, RoomCard } from '../../components';
import { useState, useEffect, useContext } from 'react';
import DataContext from '../../DataContext';
import { Input } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Rooms = () => { 

const { building } = useContext(DataContext);
const [isCardOpen, setIsCardOpen] = useState(false);
const [isLoading, setIsLoading] = useState(true);
const [isSave, setIsSave] = useState(false);
const [isDisabled, setIsDisabled] = useState(true);

const [roomName, setRoomName] = useState("");
const [rooms, setRooms] = useState([]);

const handleCardOpen = () => setIsCardOpen(!isCardOpen);

/*handle add*/
		const handleAdd = (e) => {
				e.preventDefault();
				setIsSave(true);
				setIsDisabled(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/room/${building.buildingId}/add-room`, {
						method: 'POST',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify({ roomName: roomName.toUpperCase() })
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${roomName.toUpperCase()} Successfully added!`)
										fetchRooms();
										setRoomName('');
										setIsDisabled(true);
										setIsSave(false);
							} else {
										toast.error('Unable to add category!')
										setRoomName('');
										setIsDisabled(true);
										setIsSave(false);
							}
				})
		}
/**/

const fetchRooms = () => {
  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
    method: "GET"
  })
  .then(response => response.json())
  .then(result => {
    	if (result.rooms.length > 0) {
    		setRooms(result.rooms)
    		setIsLoading(false);
    	} else {
    			setRooms([]);
    			setIsLoading(false)
    	}
  })
  .catch(error => console.error);
}

		useEffect(() => {
				fetchRooms();
		}, [])

		useEffect(() => {	
				roomName.length > 2 ? setIsDisabled(false) : setIsDisabled(true);
		}, [roomName])

const addRoom = (
					<form className="flex flex-col gap-3">
							<div className="mb-4 mt-6">
							  <Input
							  		value={roomName}
							  		onChange={(e) => setRoomName(e.target.value)}
							    type="text"
							    size="sm"
							    className="uppercase"
							    label="Name"
							    color="teal"
							  />
							</div>
							<CustomButton
									label={!isSave ? "Save" : "Saving..."}
									tooltip="hidden"
									color="green"
									loading={isSave ? true : false}
									isDisabled={isDisabled}
									handleClick={handleAdd}
							/>
					</form>
	)

	  return (
	  			<Layout
	  					title="Inventory Storage Locations"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New Storage Location"
	  					childrenClass={ isLoading ? "hidden" : "" }
	  					itemNumber={rooms.length > 0 ? `${rooms.length} items` : "No data"}
	  					copyRight={ isCardOpen ? "hidden" : "" }
	  					children2={addRoom}
	  			>

	  				<div className={`grid gap-2 ${isCardOpen ? "grid-cols-3" : "grid-cols-4"}`}>
	  					{
	  						rooms.length > 0 ? (
	  									rooms.map((room, index) => {
	  											return (
	  														<RoomCard  
	  															key={room._id} 
	  															props={room} 
	  															fetchData={fetchRooms}
	  															count={index + 1}
	  														/>
	  												)
	  									})
	  							) : (
	  									"No data"
	  							)
	  					}
	  				</div>

	  			</Layout>
	  )
} 

export default Rooms