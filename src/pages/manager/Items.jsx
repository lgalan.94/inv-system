import {
  Tabs,
  TabsHeader,
  TabsBody,
  Tab,
  TabPanel,
  Typography,
  Menu, MenuHandler, MenuList, MenuItem, Button, Input
} from "@material-tailwind/react";
import DataContext from '../../DataContext';
import { useState, useEffect, useContext } from 'react';
import { InventoryItems, Inventory } from './';
import { Layout, CustomButton, AddItem, ItemList, SortItems } from '../../components'
import { IoMdArrowDropleft } from "react-icons/io";

const Items = () => {

const TABLE_HEAD = ["#", "Image", "Name", "Category", "Description", ""];
const [isCardOpen, setIsCardOpen] = useState(false);
const [isLoading, setIsLoading] = useState(true);
const [viewByCategory, setViewByCategory] = useState(false);
const [viewByLocation, setViewByLocation] = useState(false);
const [viewAll, setViewAll] = useState(true);

const { building } = useContext(DataContext);
const [categoryData, setCategoryData] = useState([]);
const [itemsData, setItemsData] = useState([]);
const [locationsData, setLocationsData] = useState([]);
const [firstItem, setFirstItem] = useState("");
const [firstLocationItem, setFirstLocationItem] = useState("");
const [openMenu, setOpenMenu] = useState(false);

const [searchTerm, setSearchTerm] = useState('');
const [filteredData, setFilteredData] = useState(itemsData);

const handleSearchChange = (event) => {
  setSearchQuery(event.target.value);
};

const handleCardOpen = () => setIsCardOpen(!isCardOpen);
const handleViewAll = () => {
		setViewAll(true);
		setViewByLocation(false);
		setViewByCategory(false);
}
const handleViewByCategory = () => {
		setViewAll(false);
		setViewByLocation(false);
		setViewByCategory(true);
}

const handleViewByLocation = () => {
		setViewAll(false);
		setViewByLocation(true);
		setViewByCategory(false);
}
const fetchData = () => {
  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
    method: "GET"
  })
  .then(response => response.json())
  .then(result => {
  		result.items.sort((a, b) => b.createdOn.localeCompare(a.createdOn));
      if (result.items.length > 0) {
      		setLocationsData(result.rooms);
      		setCategoryData(result.categories);
      		setItemsData(result.items);
      		setFirstItem(result.categories[0].categoryName)
      		setFirstLocationItem(result.rooms[0].roomName)
      		setIsLoading(false);
      } else {
      		setCategoryData([]);
      		setItemsData([]);
      		setIsLoading(false)
      }
  })
  .catch(error => console.error);
}


useEffect(() => {
    fetchData();
}, [])

let itemsByCategory = (
				<Tabs id="custom-animation" value={firstItem} className="">
				  <TabsHeader className="min-w-full max-w-[40rem] overflow-x-auto scrollbar-thin">
				    {categoryData.map((category) => (
				      <Tab 
				        key={category._id} 
				        value={category.categoryName}
				        className="min-w-[10rem] max-w-[10rem]"
				      >
				        {category.categoryName}
				      </Tab>
				    ))}
				  </TabsHeader>
				  <TabsBody
				   className={`grid gap-3 p-2 max-h-[68vh] overflow-y-auto scrollbar-thin ${isCardOpen ? "grid-cols-3" : "grid-cols-4"}`}
				    animate={{
				      initial: { y: 250 },
				      mount: { y: 0 },
				      unmount: { y: 250 },
				    }}
				  >
		        {
		        	searchTerm.length > 0 ? (
		        				filteredData.length > 0 ? (
		        							filteredData.map((item, index) => (
		        										<TabPanel className="p-0" key={item._id} value={item.category}>
		        										 <SortItems count={index + 1} key={item._id} props={item} />
		        										</TabPanel>
		        								))
		        					) : (
		        						<p>No match found!</p>
		        					)
		        		)	: (
		        			itemsData.map((item, index) => (
		    			      <TabPanel className="p-0" key={item._id} value={item.category}>
		    			       <SortItems count={index + 1} key={item._id} props={item} />
		    			      </TabPanel>
		    			    ))
		        		)
		        }

				  </TabsBody>
				</Tabs>
	)

let itemsByLocation = (
				<Tabs id="custom-animation" value={firstLocationItem} className="">
				  <TabsHeader className="min-w-full max-w-[40rem] overflow-x-auto scrollbar-thin">
				    {locationsData.map((location) => (
				      <Tab 
				        key={location._id} 
				        value={location.roomName}
				        className="min-w-[10rem] max-w-[10rem]"
				      >
				        {location.roomName}
				      </Tab>
				    ))}
				  </TabsHeader>
				  <TabsBody
				   className={`grid gap-3 p-2 max-h-[68vh] overflow-y-auto scrollbar-thin ${isCardOpen ? "grid-cols-3" : "grid-cols-4"}`}
				    animate={{
				      initial: { y: 250 },
				      mount: { y: 0 },
				      unmount: { y: 250 },
				    }}
				  >
				    {
				    	searchTerm.length > 0 ? (
				    				filteredData.length > 0 ? (
				    							filteredData.map((item, index) => (
				    										<TabPanel className="p-0" key={item._id} value={item.location}>
													       <SortItems count={index + 1} key={item._id} props={item} />
													      </TabPanel>
				    								))
				    					) : (
				    						<p>No match found!</p>
				    					)
				    		)	: (
				    			itemsData.map((item, index) => (
							      <TabPanel className="p-0" key={item._id} value={item.location}>
							       <SortItems count={index + 1} key={item._id} props={item} />
							      </TabPanel>
							    ))
				    		)
				    }
				  </TabsBody>
				</Tabs>
	)

let itemsTable = (
			<table className="w-full min-w-max table-auto text-left">
			  <thead className="sticky top-0 shadow uppercase font-bold">
			    <tr>
			      {TABLE_HEAD.map((head) => (
			        <th
			          key={head}
			          className="border-b border-blue-gray-100 bg-blue-gray-50 first:px-1 py-4"
			        >
			          <Typography
			            variant="small"
			            color="blue-gray"
			            className="font-normal leading-none opacity-70"
			          >
			            {head}
			          </Typography>
			        </th>
			      ))}
			    </tr>
			  </thead>
			  <tbody>

			  	{
			  		searchTerm.length > 0 ? (
			  					filteredData.length > 0 ? (
			  								filteredData.map((item, index) => (
			  											<ItemList
			  													fetchData={fetchData}
			  													itemProps={item}
			  													key={item._id}
			  													count={index + 1}
			  											/>
			  									))
			  						) : (
			  							<p>No match found!</p>
			  						)
			  			)	: (
			  				itemsData.map((item, index) => (
			  							<ItemList
			  									fetchData={fetchData}
			  									itemProps={item}
			  									key={item._id}
			  									count={index + 1}
			  							/>
			  					))
			  			)
			  	}


			  </tbody>
			</table>
	)

const refresh = () => {
		setIsLoading(true);
		fetchData();
		setTimeout(() => setIsLoading(false), 4000)
}

const handleInputChange = (event) => {
    const { value } = event.target;
    setSearchTerm(value);
    filterData(value);
  };


const filterData = (searchTerm) => {
  const filteredData = itemsData.filter((item) =>
    item.itemName.toLowerCase().includes(searchTerm.toLowerCase())
  );
  setFilteredData(filteredData);
};

const search = (
		<div className="flex flex-row mt-1 mr-4">
		 <div className="relative w-full gap-2 md:w-max">
		   <Input
		     type="search"
		     value={searchTerm}
		     onChange={handleInputChange}
		     placeholder="Search By Item Name"
		     containerProps={{
		       className: "min-w-[180px] max-w-[180px]",
		     }}
		     className="!border-t-blue-gray-300 pl-9 placeholder:text-blue-gray-300 focus:!border-blue-gray-300"
		     labelProps={{
		       className: "before:content-none after:content-none",
		     }}
		   />

		   <div className="!absolute left-3 top-[13px]">
		     <svg
		       width="13"
		       height="14"
		       viewBox="0 0 14 15"
		       fill="none"
		       xmlns="http://www.w3.org/2000/svg"
		     >
		       <path
		         d="M9.97811 7.95252C10.2126 7.38634 10.3333 6.7795 10.3333 6.16667C10.3333 4.92899 9.84167 3.742 8.9665 2.86683C8.09133 1.99167 6.90434 1.5 5.66667 1.5C4.42899 1.5 3.242 1.99167 2.36683 2.86683C1.49167 3.742 1 4.92899 1 6.16667C1 6.7795 1.12071 7.38634 1.35523 7.95252C1.58975 8.51871 1.93349 9.03316 2.36683 9.4665C2.80018 9.89984 3.31462 10.2436 3.88081 10.4781C4.447 10.7126 5.05383 10.8333 5.66667 10.8333C6.2795 10.8333 6.88634 10.7126 7.45252 10.4781C8.01871 10.2436 8.53316 9.89984 8.9665 9.4665C9.39984 9.03316 9.74358 8.51871 9.97811 7.95252Z"
		         fill="#CFD8DC"
		       />
		       <path
		         d="M13 13.5L9 9.5M10.3333 6.16667C10.3333 6.7795 10.2126 7.38634 9.97811 7.95252C9.74358 8.51871 9.39984 9.03316 8.9665 9.4665C8.53316 9.89984 8.01871 10.2436 7.45252 10.4781C6.88634 10.7126 6.2795 10.8333 5.66667 10.8333C5.05383 10.8333 4.447 10.7126 3.88081 10.4781C3.31462 10.2436 2.80018 9.89984 2.36683 9.4665C1.93349 9.03316 1.58975 8.51871 1.35523 7.95252C1.12071 7.38634 1 6.7795 1 6.16667C1 4.92899 1.49167 3.742 2.36683 2.86683C3.242 1.99167 4.42899 1.5 5.66667 1.5C6.90434 1.5 8.09133 1.99167 8.9665 2.86683C9.84167 3.742 10.3333 4.92899 10.3333 6.16667Z"
		         stroke="#CFD8DC"
		         stroke-width="2"
		         stroke-linecap="round"
		         stroke-linejoin="round"
		       />
		     </svg>
		   </div>
		 </div>
		</div>
	)

	  return (
	  			<Layout
	  					title="Items"
	  					refresh={refresh}
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New"
	  					childrenClass={ isLoading ? "hidden" : "" }
	  					children2={<AddItem fetchData={fetchData} />}
	  					// itemNumber={itemsData.length > 0 ? `${itemsData.length} items` : "No Data"}
	  					copyRight={isCardOpen ? "hidden" : ""}
	  					itemNumber=""
	  					cardBody="p-0"
	  					sort={
	  								<Menu placement="bottom-end" open={openMenu} handler={setOpenMenu} allowHover>
	  								  <MenuHandler>
	  								    <Button
	  								      variant="text"
	  								      className="flex items-center gap-3 text-xs font-leading tracking-normal hover:bg-rounded-full"
	  								    >
	  								   { viewAll ? "All items" : viewByCategory ? "Items By Category" : "Items By Location" } {" "}

	  								      <IoMdArrowDropleft 
	  								      		className={`w-6 h-6 transition-transform transition-colors delay-75 ${openMenu ? "-rotate-90 text-green-600" : ""} `}
	  								      />
	  								      
	  								    </Button>
	  								  </MenuHandler>
	  								  <MenuList className="max-h-[20rem] overflow-y-auto scrollbar-thin">
	  								   <MenuItem onClick={handleViewAll}> View All </MenuItem>
	  								   <MenuItem onClick={handleViewByCategory}> View By Category </MenuItem>
	  								   <MenuItem onClick={handleViewByLocation}> View By Location </MenuItem>
	  								  </MenuList>
	  								</Menu>
	  					}
	  					search={search}
	  			>
	  						{viewAll ? (
	  						  <div>
	  						    {itemsTable}
	  						  </div>
	  						) : viewByCategory ? (
	  						  <div>
	  						    {itemsByCategory}
	  						  </div>
	  						) : (
	  						  <div>
	  						    {itemsByLocation}
	  						  </div>
	  						)}

	  			</Layout>
	  )
}

export default Items