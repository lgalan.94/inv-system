import { Typography, Button } from '@material-tailwind/react';
import DataContext from '../../DataContext';
import { useContext, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const Landing = () => {

	const { data, unsetData, setData, building, setBuilding } = useContext(DataContext);
	const navigate = useNavigate();
	const [isClicked, setIsClicked] = useState(false);

	const handleButtonClick = () => {
				setIsClicked(true);
				setTimeout(() => {
						navigate(`/lab-manager/${building.buildingId}/home`);
						setIsClicked(false);
				}, 5000)
	}

	const handleBack = () => {
	    unsetData();
	    setData({
	      id: null,
	      name: null,
	      email: null,
	      userRole: null,
	      buildingsAssigned: []
	    });
	    setBuilding({
	      buildingId: null,
	      buildingName: null,
	      buildingImage: null,
	      buildingAddress: null
	    })
	    navigate('/')
	}

		return (
						<div className="min-h-screen flex flex-col items-center justify-center gap-4">
								<Typography 
									animate={{
				        mount: { scale: 1, y: 0 },
				        unmount: { scale: 0, y: 25 }
				      }}
									variant="h1" className="capitalize tracking-wide" > Welcome {data.name} </Typography>

									<img
											src={data.imageUrl}
											className="rounded-lg w-[12%] shadow-lg"
									/>
									
									{
											data.id !== undefined ? (
															building.buildingId !== null ? (
																			<Button
																					onClick={handleButtonClick}
																					loading={isClicked}
																					disabled={isClicked}
																					className="mt-5"
																			>
																				{ !isClicked ? `Go to ${building.buildingName} Dashboard` : `redirecting to ${building.buildingName} dashboard...` }
																			</Button>
																) : (
																		<>
																			<Typography className="mt-5" variant="h5">You don't have a building assignment yet!</Typography>
																			<Button
																					onClick={handleBack}
																			>
																				Back to home
																			</Button>
																		</>
																)
												) : (
														""
												)
									}

						</div>
			)
}

export default Landing