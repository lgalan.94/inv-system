import { Layout, CustomButton } from '../../components';
import { SamplesStorageLocationCard } from '../../components/labManager';
import { useState, useEffect, useContext } from 'react';
import DataContext from '../../DataContext';
import { Input } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const SamplesStorageLocation = () => { 

const { building } = useContext(DataContext);
const [isCardOpen, setIsCardOpen] = useState(false);
const [isLoading, setIsLoading] = useState(true);
const [isSave, setIsSave] = useState(false);
const [isDisabled, setIsDisabled] = useState(true);

const [storageName, setStorageName] = useState("");
const [storageLocations, setStorageLocations] = useState([]);

const handleCardOpen = () => setIsCardOpen(!isCardOpen);

/*handle add*/
		const handleAdd = (e) => {
				e.preventDefault();
				setIsSave(true);
				setIsDisabled(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/storage/${building.buildingId}/add-storage`, {
						method: 'POST',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify({ storageName: storageName.toUpperCase() })
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${storageName.toUpperCase()} Successfully added!`)
										fetchStorageLocations();
										setStorageName('');
										setIsDisabled(true);
										setIsSave(false);
							} else {
										toast.error('Unable to add storage!')
										setStorageName('');
										setIsDisabled(true);
										setIsSave(false);
							}
				})
		}
/**/

const fetchStorageLocations = () => {
  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
    method: "GET"
  })
  .then(response => response.json())
  .then(result => {
    	if (result.samplesStorage.length > 0) {
    		setStorageLocations(result.samplesStorage)
    		setIsLoading(false);
    	} else {
    			setStorageLocations([]);
    			setIsLoading(false)
    	}
  })
  .catch(error => console.error);
}

		useEffect(() => {
				fetchStorageLocations();
		}, [])

		useEffect(() => {	
				storageName.length > 2 ? setIsDisabled(false) : setIsDisabled(true);
		}, [storageName])

		const refresh = () => {
				setIsLoading(true);
				fetchStorageLocations();
		}

const addStorage = (
					<form className="flex flex-col gap-3">
							<div className="mb-4 mt-6">
							  <Input
						  		value={storageName}
						  		onChange={(e) => setStorageName(e.target.value)}
							    type="text"
							    className="uppercase"
							    label="Storage Name"
							    color="teal"
							  />
							</div>
							<CustomButton
									label={!isSave ? "Save" : "Saving..."}
									tooltip="hidden"
									color="green"
									loading={isSave ? true : false}
									isDisabled={isDisabled}
									handleClick={handleAdd}
							/>
					</form>
	)

	  return (
	  			<Layout
	  					title="Samples Storage Locations"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New Storage Location"
	  					childrenClass={ isLoading ? "hidden" : "" }
	  					itemNumber={storageLocations.length > 0 ? `${storageLocations.length} items` : "No data"}
	  					copyRight={ isCardOpen ? "hidden" : "" }
	  					children2={addStorage}
	  					refresh={refresh}
	  			>

	  				<div className={`grid gap-2 ${isCardOpen ? "grid-cols-3" : "grid-cols-4"}`}>
	  					{
	  						storageLocations.length > 0 ? (
	  									storageLocations.map((storage, index) => {
	  											return (
	  														<SamplesStorageLocationCard
	  															key={storage._id} 
	  															props={storage} 
	  															fetchData={fetchStorageLocations}
	  															count={index + 1}
	  														/>
	  												)
	  									})
	  							) : (
	  									"No data"
	  							)
	  					}
	  				</div>

	  			</Layout>
	  )
} 

export default SamplesStorageLocation