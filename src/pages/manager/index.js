import Home from './Home';
import Inventory from './Inventory';
import Categories from './Categories';
import Settings from './Settings';
import Landing from './Landing';
import ManageProfile from './ManageProfile';
import InventoryItems from './InventoryItems';
import Items from './Items';
import Rooms from './Rooms';
import Notifications from './Notifications';
import Samples from './Samples';
import SamplesStorageLocation from './SamplesStorageLocation';
import Experiments from './Experiments';

export {
	Home,
	Inventory,
	Categories,
	Settings,
	Landing,
	ManageProfile,
	InventoryItems,
	Items,
	Rooms,
	Notifications,
	Samples,
	SamplesStorageLocation,
	Experiments
}