import { Layout, CustomButton } from '../../components';
import { useState, useEffect, useContext } from 'react';
import DataContext from '../../DataContext';
import { ExperimentCard } from '../../components/labManager';
import { Input, Textarea, Typography, Select, Option } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Experiments = () => {

const {building} = useContext(DataContext);

const [items, setItems] = useState([]);
const TABLE_HEAD = ["#", "Title", "Status", "Description", ""];
const [isCardOpen, setIsCardOpen] = useState(false);
const [isLoading, setIsLoading] = useState(true);
const [isSave, setIsSave] = useState(false);
const [isDisabled, setIsDisabled] = useState(true);
const [title, setTitle] = useState('');
const [status, setStatus] = useState('');
const [description, setDescription] = useState('');
const [results, setResults] = useState('');
const [researcher, setResearcher] = useState('');

const handleCardOpen = () => setIsCardOpen(!isCardOpen);

useEffect(() => {
		title.length > 0 &&
		status !== null &&
		description.length > 0 && 
		results.length > 0 &&
		researcher.length > 0 ? setIsDisabled(false) : setIsDisabled(true)
}, [title, status, description, results, researcher])

const fetchExperiments = () => {
  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
    method: "GET"
  })
  .then(response => response.json())
  .then(result => {
    	if (result.experiments.length > 0) {
    		setItems(result.experiments);
    		setIsLoading(false);
    	} else {
    			setItems([]);
    			setIsLoading(false)
    	}
  })
  .catch(error => console.error);
}

useEffect(() => {
		fetchExperiments();
}, [])

const refresh = () => {
		setIsLoading(true);
		fetchExperiments();
}

/*handle add*/
		const handleAdd = (e) => {
				e.preventDefault();
				setIsSave(true);
				setIsDisabled(true);
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/experiment/${building.buildingId}/create-experiment`, {
						method: 'POST',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify({ 
							title,
							description,
							status,
							results,
							researcher
						})
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${title.toUpperCase()} Successfully added!`)
										fetchExperiments();
										setTitle('');
										setDescription('');
										setResults('');
										setResearcher('');
										setIsDisabled(true);
										setIsSave(false);
							} else {
										toast.error('Unable to add category!')
										setTitle('');
										setDescription('');
										setResults('');
										setResearcher('');
										setIsDisabled(true);
										setIsSave(false);
							}
				})
		}
/**/

		const createExperiment = (
							<form className="flex flex-col gap-7">
									<div className="flex flex-col gap-2">
									  <Input
									  		value={title}
									  		onChange={(e) => setTitle(e.target.value)}
									    type="text"
									    className="capitalize"
									    label="Title"
									    color="teal"
									  />
									  <Select label="Status" onChange={(e) => setStatus(e)} >
									    <Option value="Planning">Planning</Option>
									    <Option value="InProgress">InProgress</Option>
									    <Option value="Completed">Completed</Option>
									  </Select>
									  <Textarea
									  		value={description}
									  		onChange={(e) => setDescription(e.target.value)}
									    label="Description"
									    color="teal"
									    className="min-h-[40px]"
									  />
									  <div className="-mt-1">
									  	<Textarea
									  			value={results}
									  			onChange={(e) => setResults(e.target.value)}
									  	  label="Results"
									  	  color="teal"
									  	  className="min-h-[40px]"
									  	/>
									  </div>
									  <div className="-mt-1">
									  	<Input
									  			value={researcher}
									  			onChange={(e) => setResearcher(e.target.value)}
									  	  type="text"
									  	  className="capitalize"
									  	  label="Researcher"
									  	  color="teal"
									  	/>
									  </div>
									  
									</div>
									<CustomButton
											label={!isSave ? "Save" : "Saving..."}
											tooltip="hidden"
											color="green"
											loading={isSave ? true : false}
											isDisabled={isDisabled}
											handleClick={handleAdd}
									/>
							</form>
			)

	  return (
	  			<Layout
	  					title="Experiments"
	  					loading={ isLoading ? "" : "hidden" }
	  					childrenClass={ isLoading ? "hidden" : "" }
	  					children2={createExperiment}
	  					card2Label="Create Experiment"
	  					refresh={refresh}
	  					cardBody="p-0"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  			>
	  				
	  					<table className="w-full min-w-max table-auto text-left">
	  					  <thead className="sticky top-0 shadow uppercase font-bold">
	  					    <tr>
	  					      {TABLE_HEAD.map((head) => (
	  					        <th
	  					          key={head}
	  					          className="border-b border-blue-gray-100 bg-blue-gray-50 first:px-1 py-4"
	  					        >
	  					          <Typography
	  					            variant="small"
	  					            color="blue-gray"
	  					            className="font-normal leading-none opacity-70"
	  					          >
	  					            {head}
	  					          </Typography>
	  					        </th>
	  					      ))}
	  					    </tr>
	  					  </thead>
	  					  <tbody>

	  					  	{items.length > 0 ? (
	  					  				items.map((item, index) => (
	  					  							<ExperimentCard
	  					  								fetchData={fetchExperiments}
	  					  								key={item._id}
	  					  								props={item}
	  					  								count={index + 1}
	  					  							/>
	  					  					))
	  					  		) : (
	  					  			"No data"
	  					  		)}

	  					  </tbody>
	  					</table>

	  			</Layout>
	  )
}

export default Experiments