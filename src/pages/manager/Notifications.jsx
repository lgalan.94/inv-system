import { Layout, CustomButton } from '../../components';
import { useState, useEffect } from 'react';

const Notifications = () => {

const [isLoading, setIsLoading] = useState(true);

	  return (
	  			<Layout
	  					title="Notifications"
	  					card2="hidden"
	  					loading={ isLoading ? "" : "hidden" }
	  					headerButtons="hidden"
	  					childrenClass={ isLoading ? "hidden" : "" }
	  			>
	  			
	  			</Layout>
	  )
}

export default Notifications