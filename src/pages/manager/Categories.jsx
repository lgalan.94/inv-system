import { Layout, CustomButton, CustomDialogBox, InvisibleButton, CustomDrawer, CategoryCard } from '../../components';
import { useState, useEffect, useContext } from 'react';
import {
  Card,
  CardHeader,
  CardBody,
  Typography,
  Button,
  Tooltip,
  Input,
  Textarea
} from "@material-tailwind/react";
import { MdDeleteOutline } from "react-icons/md";
import { FaRegEdit } from "react-icons/fa";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DataContext from '../../DataContext';

const Categories = () => {

const { building, buildingData } = useContext(DataContext);
const [items, setItems] = useState([]);
const [isCardOpen, setIsCardOpen] = useState(false);
const [isLoading, setIsLoading] = useState(true);
const [nameAlert, setNameAlert] = useState(false);
const [imageUrlAlert, setImageUrlAlert] = useState(false);
const [image_url, set_image_url] = useState('');
const [name, setName] = useState('');
const [isDisabled, setIsDisabled] = useState(true);
const [isSaveClicked, setIsSaveClicked] = useState(false);

const handleCardOpen = () => setIsCardOpen(!isCardOpen);

/*handle add*/
		const handleAdd = (e) => {
				e.preventDefault();
				setIsSaveClicked(true);
				if (name.length < 3) {
							setNameAlert(true);
				}
				if (image_url.length === 0) {
							setImageUrlAlert(true);
				}
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/category/${building.buildingId}/add-category`, {
						method: 'POST',
						headers: {
								'Content-type': 'application/json'
						},
						body: JSON.stringify({
								categoryName: name.toUpperCase(),
								imageUrl: image_url,
								buildingID: building.buildingId 
						})
				})
				.then(response => response.json())
				.then(data => {
							if (data === true) {
										toast.success(`${name} Successfully added!`)
										fetchCategories()
										setName('');
										set_image_url('');
										setIsDisabled(true);
										setIsSaveClicked(false);
							} else {
										toast.error('Unable to add category!')
										setName('');
										set_image_url('');
										setIsDisabled(true);
										setIsSaveClicked(false);
							}
				})
		}
		const handleClear = () => {
					setName('');
					set_image_url('');
		}
/**/

		const fetchCategories = () => {
		  fetch(`${import.meta.env.VITE_API_URL}/admin/building/${building.buildingId}`, {
		    method: "GET"
		  })
		  .then(response => response.json())
		  .then(result => {
		    	if (result.categories.length > 0) {
		    		setItems(result.categories)
		    		setIsLoading(false);
		    	} else {
		    			setItems("No data");
		    			setIsLoading(false)
		    	}
		  })
		  .catch(error => console.error);
		}

useEffect(() => {
		fetchCategories();
}, [])

const fetchData = () => {
	fetchCategories();
}

const refresh = () => {
	setIsLoading(true);
	fetchCategories();
	setTimeout(() => setIsLoading(false), 3000)
}

const addForm = (
					<form className="flex flex-col gap-3">
							<div className="">
							  <Input
							  		value={name}
							  		onChange={(e) => setName(e.target.value)}
							    type="text"
							    size="sm"
							    className={`capitalize`}
							    label="Name"
							    color="teal"
							    error={nameAlert}
							  />
							</div>
							<div className="w-full">
							  <Textarea 
							  		value={image_url}
							  		onChange={(e) => set_image_url(e.target.value)}
							    label="Image Url"
							    rows={2}
							    resize={true}
							    className="min-h-[60px] text-xs scrollbar-thin"
							    color="teal"
							    error={imageUrlAlert}
							  />
							</div>
							<div>
							  <img className="m-auto shadow-md w-[65%]" src={image_url} />
							</div>
							<div className="flex flex-row gap-2 mt-10 justify-center">
								<CustomButton
										label="Clear"
										tooltip="hidden"
										handleClick={handleClear}
								/>
								<CustomButton
										label={!isSaveClicked ? "Save" : "Saving..."}
										tooltip="hidden"
										color="green"
										loading={isSaveClicked ? true : false}
										isDisabled={isSaveClicked ? true : false}
										handleClick={handleAdd}
								/>
							</div>
					</form>
	)

	  return (
	  			<Layout
	  					title="Categories"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New Category Form"
	  					itemNumber={items === "No data" ? items : `${items.length} items`}
	  					copyRight={ isCardOpen ? "hidden" : ""}
	  					children2={addForm}
	  					refresh={refresh}
	  					childrenClass={ isLoading ? "hidden" : "" }
	  			>

	  					<div className={`grid gap-2 ${isCardOpen ? "grid-cols-3" : "grid-cols-4"}`}>
			 							{
			 								items.length > 0 ? (
			 											items.map((item, index) => (
			 														<CategoryCard 
			 															fetchCategories={fetchData} 
			 															key={item._id} 
			 															CategoriesProps={item}
			 															count={index + 1} 
			 														/>
			 												))
			 									) : (
			 											"No data"
			 									)
			 							}
			 				</div>

	  			</Layout>
	  )
}

export default Categories