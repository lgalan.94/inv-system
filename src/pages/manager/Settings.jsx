import { Layout, CustomButton } from '../../components';
import { useState } from 'react';

const Settings = () => {

const [isCardOpen, setIsCardOpen] = useState(true);
const [isLoading, setIsLoading] = useState(true);

const handleCardOpen = () => setIsCardOpen(!isCardOpen);


	  return (
	  			<Layout
	  					title="Settings"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New"
	  			>
	  			</Layout>
	  )
}

export default Settings