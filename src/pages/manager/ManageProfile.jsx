import { Layout, CustomButton, CustomDrawer, CustomDialogBox } from '../../components';
import { useState, useContext, useEffect } from 'react';
import DataContext from '../../DataContext';
import { Typography, Input, Textarea, Button } from '@material-tailwind/react';
import { useNavigate } from 'react-router-dom';
import { IoIosEye, IoIosEyeOff } from "react-icons/io";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ManageProfile = () => {

const { data, setData, unsetData, building, setBuilding } = useContext(DataContext);
const navigate = useNavigate();
const [isLoading, setIsLoading] = useState(true);
const [openEditProfile, setOpenEditProfile] = useState(false);
const [openEditPassword, setOpenEditPassword] = useState(false);

const [newName, setNewName] = useState(data.name);
const [newEmail, setNewEmail] = useState(data.email);
const [newImage, setNewImage] = useState(data.imageUrl);
const [isSave, setIsSave] = useState(false);
const [isDisabled, setIsDisabled] = useState(true);
const [showPassword1, setShowPassword1] = useState(true);
const [showPassword2, setShowPassword2] = useState(true);
const [showPassword3, setShowPassword3] = useState(true);
const [openDialog, setOpenDialog] = useState(false)

const handleOpenEditProfile = () => setOpenEditProfile(!openEditProfile);
const handleOpenEditPassword = () => setOpenEditPassword(!openEditPassword); 
const handleShowPassword1 = () => setShowPassword1(!showPassword1);
const handleShowPassword2 = () => setShowPassword2(!showPassword2);
const handleShowPassword3 = () => setShowPassword3(!showPassword3);

useEffect(() => {
		setTimeout(() => setIsLoading(false), 500)
}, [])

const handleNewNameChange = (e) => setNewName(e.target.value);
const handleNewEmailChange = (e) => setNewEmail(e.target.value);
const handleNewImageChange = (e) => setNewImage(e.target.value);

useEffect(() => {
		newName !== data.name || newEmail !== data.email || newImage !== data.imageUrl ? setIsDisabled(false) : setIsDisabled(true);
}, [newName, newEmail, newImage])

const handleUpdateDetails = (event) => {
   event.preventDefault();
   setIsSave(true);
   setIsDisabled(true);
   fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/${data.id}`, {
      method: "PATCH",
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        newName: newName,
        newEmail: newEmail,
        newImageUrl: newImage
      })
   })
   .then(response => response.json())
   .then(data => {
      if (data === true) {
         toast.success("Successfully Updated!");
         setOpenEditProfile(false)
         setIsSave(false);
         setTimeout(() => setOpenDialog(true),3000)
      } else {
        toast.error("Unable to update user");
        setIsSave(false);
        setOpenEditProfile(false)
      }
   })
}

const handleLogout = () => {
    unsetData();
    setData({
      id: null,
      name: null,
      email: null,
      userRole: null,
      imageUrl: null,
      buildingsAssigned: []
    });
    setBuilding({
      buildingId: null,
      buildingName: null
    })
    navigate('/')
}

	  return (
	  			<Layout
	  					title="Manage Profile"
	  					card2="hidden"
	  					loading={ isLoading ? "" : "hidden" }
	  					headerButtons="hidden"
	  					childrenClass={ isLoading ? "hidden":"" }
	  			>
	  			<div className="flex flex-row gap-4	">
	  				<div className="">
	  						<img src={data.imageUrl} className="rounded-md max-h-[80vh] max-w-[60vw]" />
	  				</div>
	  				<div className="pl-2 rounded-md flex flex-col justify-center">
	  						<Typography variant="h1" className="capitalize" > {data.name} </Typography>
	  						<Typography variant="h6" className="italic" >User Id: {data.id} </Typography>
	  						<Typography variant="h6" className="italic" >Email: {data.email} </Typography>
	  						<Typography variant="h6" > Role: {data.userRole === 0 ? "Admin" : data.userRole === 1 ? "Lab Manager" : "Inventory Clerk/Technician"} </Typography>
	  						<div className="flex flex-row gap-2 mt-8 justify-center">
	  							<CustomButton
	  									label="Dashboard"
	  									tooltip="hidden"
	  									color="cyan"
	  									handleClick={() => data.userRole === 1 ? navigate(`/lab-manager/${building.buildingId}/home`) : data.userRole === 0 ? navigate(`/admin/home`) : navigate(`/clerk/${building.buildingId}/home`) }
	  							/>
	  							<CustomButton
	  									label="Edit Profile"
	  									tooltip="hidden"
	  									color="green"
	  									handleClick={handleOpenEditProfile}
	  							/>
	  							<CustomButton
	  									label="Change Password"
	  									tooltip="hidden"
	  									handleClick={handleOpenEditPassword}
	  							/>
	  						</div>
	  				</div>
	  			</div>

	  			
	  			<CustomDrawer
	  					title="Edit Profile"
	  					open={openEditProfile}
	  					handleOpenDrawer={handleOpenEditProfile}
	  					handleCloseDrawer={handleOpenEditProfile}
	  			>

	  			<form className="p-5 flex flex-col gap-3">
	  					<Input		
	  						label="Name"
	  						value={newName}
	  						onChange={handleNewNameChange}
	  						className="capitalize"
	  					/>
	  					<Input
	  						label="Email"
	  						value={newEmail}
	  						onChange={handleNewEmailChange}
	  					/>
	  					<Textarea
	  						label="Image Url"
	  						value={newImage}
	  						onChange={handleNewImageChange}
	  					/>
	  					<img src={newImage} className="rounded-md w-[70%] mx-auto" />
	  					<CustomButton
	  						handleClick={handleUpdateDetails}
	  						label={ isSave ? "Saving ..." : "Save" }
	  						loading={ isSave ? true : false }
	  						btn="mt-5"
	  						isDisabled={isDisabled}
	  					/>
	  			</form>

	  			</CustomDrawer>

	  			<CustomDrawer
	  					title="Change Password"
	  					open={openEditPassword}
	  					handleOpenDrawer={handleOpenEditPassword}
	  					handleCloseDrawer={handleOpenEditPassword}
	  			>

	  			<form className="p-5 flex flex-col gap-3">
	  					<div className="flex flex-row">
	  							<div className="w-[90%]">
	  									<Input
	  										type={ showPassword1 ? "password" : "text" }
	  										label="Current Password"
	  									/>
	  							</div>
	  							<Button 
	  									variant="text"
	  									className="text-[9px] px-2 py-0 pr-3"
	  									onClick={handleShowPassword1} 
	  									> 
	  									{ showPassword1 ? "show" : "hide" } 
	  							</Button>
	  					</div>

	  					<div className="flex flex-row">
	  							<div className="w-[90%]">
	  									<Input
	  										type={ showPassword2 ? "password" : "text" }
	  										label="New Password"
	  									/>
	  							</div>
	  							<Button 
	  									variant="text"
	  									className="text-[9px] px-2 py-0 pr-3"
	  									onClick={handleShowPassword2} 
	  									> 
	  									{ showPassword2 ? "show" : "hide" } 
	  							</Button>
	  					</div>

	  				<div className="flex flex-row">
  							<div className="w-[90%]">
  									<Input
  										type={ showPassword3 ? "password" : "text" }
  										label="Confirm New Password"
  									/>
  							</div>
  							<Button 
  									variant="text"
  									className="text-[9px] px-2 py-0 pr-3"
  									onClick={handleShowPassword3} 
  									> 
  									{ showPassword3 ? "show" : "hide" } 
  							</Button>
  					</div>
	  						
  					<div className="flex flex-row gap-2 mt-6">
  						<CustomButton
  							handleClick={handleOpenEditPassword}
	  						label="Cancel"
	  						btn="w-[50%]"
	  						tooltip="hidden"
	  					/>
	  					<CustomButton
	  						label="Save"
	  						btn="w-[50%]"
	  						tooltip="hidden"
	  						color="green"
	  					/>
  					</div>
	  					
	  			</form>
	  			
	  			</CustomDrawer>

	  			<CustomDialogBox
	  					title="Re-login"
	  					open={openDialog}
	  			>
	  				 <Typography className="py-5"> For the changes to take effect, you have to log back in! </Typography>

	  				 	<CustomButton
	  				 			label="Go"
	  				 			tooltip="hidden"
	  				 			handleClick={handleLogout}
	  				 			btn="mx-auto"
	  				 	/>

	  			</CustomDialogBox>

	  			</Layout>
	  )
}

export default ManageProfile