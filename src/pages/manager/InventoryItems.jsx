import { Layout, CustomButton } from '../../components';
import { useState, useEffect } from 'react';

const InventoryItems = () => {

const [isCardOpen, setIsCardOpen] = useState(true);
const [isLoading, setIsLoading] = useState(true);

const handleCardOpen = () => setIsCardOpen(!isCardOpen);

useEffect(() => {
		setTimeout(() => setIsLoading(false), 500)
}, [])

	  return (
	  			<Layout
	  					title="Inventory Items"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New"
	  					childrenClass={ isLoading ? "hidden" : "" }
	  			>


	  			</Layout>
	  )
}

export default InventoryItems