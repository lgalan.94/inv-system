import { Layout, CustomButton } from '../../components';
import { useState, useEffect } from 'react';

const ClerkHome = () => {

const [isLoading, setIsLoading] = useState(true);

	  return (
	  			<Layout
	  					title="Dashboard"
	  					card2="hidden"
	  					loading={ isLoading ? "" : "hidden" }
	  					headerButtons="hidden"
	  					childrenClass={ isLoading ? "hidden" : "" }
	  			>
	  			
	  			</Layout>
	  )
}

export default ClerkHome