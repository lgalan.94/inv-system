const data = 
[
  {
    "_id": "659c80a29507f57b1a24ff8c",
    "isActive": false,
    "picture": "https://images.pexels.com/photos/2280571/pexels-photo-2280571.jpeg?auto=compress&cs=tinysrgb&w=600",
    "name": "Cecile Cardenas",
    "description": "Qui sit quis officia eu ea id sit velit irure aliqua adipisicing exercitation reprehenderit. Laborum commodo sit proident qui ullamco. Nisi fugiat ullamco sint aliqua est cupidatat cillum do dolore ipsum dolor pariatur. Consectetur do adipisicing adipisicing ex occaecat. Proident laborum qui excepteur anim sunt eu proident voluptate ad in et enim dolore occaecat. Anim magna velit proident enim aute ullamco ea quis. Dolor aliquip nostrud cillum voluptate fugiat aliqua cillum.\r\n",
    "category": "other",
    "quantity": 647,
    "low_stock_threshold": 30,
    "location": "Room 3"
  },
  {
    "_id": "659c80a2eed0d0b5d41a7cab",
    "isActive": false,
    "picture": "https://images.pexels.com/photos/4021773/pexels-photo-4021773.jpeg?auto=compress&cs=tinysrgb&w=600",
    "name": "Paige Blake",
    "description": "Ea labore proident sit velit incididunt non occaecat. Quis nostrud magna non duis dolor enim ex. Laborum elit nostrud aute mollit do magna nisi consequat officia qui ea. Irure aute dolor eu consequat tempor nulla consectetur consectetur. Ipsum sit magna qui labore.\r\n",
    "category": "equipments",
    "quantity": 657,
    "low_stock_threshold": 30,
    "location": "Room 3"
  },
  {
    "_id": "659c80a21ba20a3cc90f8b0f",
    "isActive": false,
    "picture": "http://placehold.it/32x32",
    "name": "Hicks Christian",
    "description": "Ea deserunt ad duis deserunt esse esse et consectetur commodo reprehenderit eiusmod magna esse sunt. Culpa adipisicing quis eu fugiat nostrud exercitation exercitation qui elit adipisicing incididunt elit aliquip deserunt. Velit nisi sint excepteur sint pariatur non tempor culpa amet eu in mollit. Esse veniam in mollit est laborum sint cupidatat officia exercitation adipisicing pariatur esse. Excepteur amet ipsum excepteur commodo mollit duis.\r\n",
    "category": "equipments",
    "quantity": 839,
    "low_stock_threshold": 69,
    "location": "Room 2"
  },
  {
    "_id": "659c80a2121e69c1e867ac05",
    "isActive": true,
    "picture": "http://placehold.it/32x32",
    "name": "Crystal Cross",
    "description": "Qui amet exercitation enim culpa aliquip eiusmod minim incididunt adipisicing laboris et consectetur anim reprehenderit. Excepteur excepteur qui nostrud proident consectetur irure laborum pariatur pariatur. Minim sunt nulla laboris ullamco elit eiusmod mollit id ad et.\r\n",
    "category": "consumables",
    "quantity": 234,
    "low_stock_threshold": 89,
    "location": "Room 1"
  },
  {
    "_id": "659c80a2ab4d52f54209d040",
    "isActive": false,
    "picture": "http://placehold.it/32x32",
    "name": "Mamie Burch",
    "description": "Eiusmod voluptate ullamco dolor minim aute voluptate occaecat ut esse esse laboris anim sunt. Dolore esse cillum sit enim pariatur laborum et elit. Amet cillum dolor voluptate labore. Dolore aliquip quis adipisicing enim deserunt sunt incididunt velit aute laborum magna officia. Anim nostrud ea fugiat laborum et. Et eiusmod ipsum non duis.\r\n",
    "category": "other",
    "quantity": 597,
    "low_stock_threshold": 57,
    "location": "Room 1"
  },
  {
    "_id": "659c80a25f30bf612859366a",
    "isActive": false,
    "picture": "http://placehold.it/32x32",
    "name": "Langley Castaneda",
    "description": "Tempor ut incididunt nostrud non. Mollit tempor est occaecat ad sunt nostrud in magna magna occaecat consequat nostrud. Deserunt dolor est culpa dolore non voluptate veniam aliqua voluptate nisi aute aliquip dolore. Pariatur laboris do exercitation sint est ea aute elit commodo excepteur pariatur. Deserunt ipsum sunt consequat dolore consequat occaecat cillum anim ut nulla deserunt sint consequat exercitation. Voluptate elit voluptate sunt ut. Ex ad voluptate fugiat anim occaecat et laborum officia aliquip consequat.\r\n",
    "category": "consumables",
    "quantity": 848,
    "low_stock_threshold": 48,
    "location": "Room 3"
  },
  {
    "_id": "659c80a28a4f324f08f7e3ee",
    "isActive": true,
    "picture": "http://placehold.it/32x32",
    "name": "Sybil Albert",
    "description": "Enim consectetur non fugiat mollit proident exercitation ullamco velit reprehenderit veniam commodo in qui. Ut occaecat ex fugiat veniam est minim elit. Qui quis velit excepteur sunt sint voluptate. Laboris consectetur non elit incididunt ea pariatur pariatur commodo occaecat magna dolore officia do.\r\n",
    "category": "equipments",
    "quantity": 680,
    "low_stock_threshold": 77,
    "location": "Room 2"
  },
  {
    "_id": "659c80a260c020a67c537f4a",
    "isActive": true,
    "picture": "http://placehold.it/32x32",
    "name": "Deborah Clements",
    "description": "Ullamco exercitation excepteur ad nulla ullamco do enim amet. Commodo anim irure aliquip in consequat commodo nulla. Anim culpa excepteur reprehenderit id dolore laboris culpa elit occaecat cupidatat anim ullamco ullamco commodo. Fugiat labore sunt ea ut anim aliquip.\r\n",
    "category": "consumables",
    "quantity": 989,
    "low_stock_threshold": 22,
    "location": "Room 2"
  },
  {
    "_id": "659c80a2dc98059f996c62ec",
    "isActive": true,
    "picture": "http://placehold.it/32x32",
    "name": "Roberta Knox",
    "description": "Magna tempor incididunt ea nisi est non occaecat minim. Eu irure incididunt cillum commodo anim ipsum magna. Aute quis aliquip tempor amet in quis amet ex dolor excepteur minim.\r\n",
    "category": "other",
    "quantity": 959,
    "low_stock_threshold": 73,
    "location": "Room 2"
  },
  {
    "_id": "659c80a24d23187ceb5428d8",
    "isActive": false,
    "picture": "http://placehold.it/32x32",
    "name": "Cheryl Wiley",
    "description": "Nisi tempor pariatur consequat Lorem consequat. Sint cillum quis dolore sint proident eu est aliqua non cillum dolore aliqua amet elit. Veniam reprehenderit elit labore irure incididunt Lorem do. Eu et ad deserunt ipsum ut aliquip occaecat ex quis non eiusmod magna ea ipsum. Do magna consectetur proident quis officia sunt anim laborum in dolore aliquip. Aliqua qui est laboris elit veniam anim qui Lorem sit voluptate. Minim do commodo reprehenderit pariatur.\r\n",
    "category": "other",
    "quantity": 305,
    "low_stock_threshold": 44,
    "location": "Room 1"
  },
  {
    "_id": "659c80a29ceadb29969e1c01",
    "isActive": false,
    "picture": "http://placehold.it/32x32",
    "name": "Campbell Reilly",
    "description": "Voluptate pariatur eiusmod esse ut velit dolore ipsum minim nisi aute minim eiusmod qui excepteur. Voluptate veniam elit minim ipsum quis sunt laboris quis deserunt labore. Culpa commodo irure ea laborum amet labore est sunt cillum.\r\n",
    "category": "consumables",
    "quantity": 350,
    "low_stock_threshold": 30,
    "location": "Room 3"
  },
  {
    "_id": "659c80a255aead9ebed42cad",
    "isActive": true,
    "picture": "http://placehold.it/32x32",
    "name": "Tammi Moran",
    "description": "Officia do ea ut id cupidatat ea adipisicing aliqua dolor. Veniam officia est nulla occaecat enim nisi laborum magna. Officia duis sunt sint non cupidatat dolor mollit. Id qui ea reprehenderit reprehenderit exercitation ex esse enim officia. Elit eu ad duis proident tempor tempor quis. Labore esse amet anim laboris amet do tempor proident. Velit ex fugiat sit qui non esse est consectetur ex dolor ut laboris incididunt.\r\n",
    "category": "equipments",
    "quantity": 268,
    "low_stock_threshold": 37,
    "location": "Room 1"
  },
  {
    "_id": "659c80a24d231187ceb5428d8",
    "isActive": false,
    "picture": "http://placehold.it/32x32",
    "name": "Cheryl Wiley",
    "description": "Nisi tempor pariatur consequat Lorem consequat. Sint cillum quis dolore sint proident eu est aliqua non cillum dolore aliqua amet elit. Veniam reprehenderit elit labore irure incididunt Lorem do. Eu et ad deserunt ipsum ut aliquip occaecat ex quis non eiusmod magna ea ipsum. Do magna consectetur proident quis officia sunt anim laborum in dolore aliquip. Aliqua qui est laboris elit veniam anim qui Lorem sit voluptate. Minim do commodo reprehenderit pariatur.\r\n",
    "category": "other",
    "quantity": 305,
    "low_stock_threshold": 44,
    "location": "Room 1"
  },
  {
    "_id": "659c80a29cea2db29969e1c01",
    "isActive": false,
    "picture": "http://placehold.it/32x32",
    "name": "Campbell Reilly",
    "description": "Voluptate pariatur eiusmod esse ut velit dolore ipsum minim nisi aute minim eiusmod qui excepteur. Voluptate veniam elit minim ipsum quis sunt laboris quis deserunt labore. Culpa commodo irure ea laborum amet labore est sunt cillum.\r\n",
    "category": "consumables",
    "quantity": 350,
    "low_stock_threshold": 30,
    "location": "Room 3"
  },
  {
    "_id": "659c80a255aead93ebed42cad",
    "isActive": true,
    "picture": "http://placehold.it/32x32",
    "name": "Tammi Moran",
    "description": "Officia do ea ut id cupidatat ea adipisicing aliqua dolor. Veniam officia est nulla occaecat enim nisi laborum magna. Officia duis sunt sint non cupidatat dolor mollit. Id qui ea reprehenderit reprehenderit exercitation ex esse enim officia. Elit eu ad duis proident tempor tempor quis. Labore esse amet anim laboris amet do tempor proident. Velit ex fugiat sit qui non esse est consectetur ex dolor ut laboris incididunt.\r\n",
    "category": "equipments",
    "quantity": 268,
    "low_stock_threshold": 37,
    "location": "Room 1"
  }
]

export default data;