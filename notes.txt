import { Layout, CustomButton } from '../../components';
import { useState } from 'react';

const Home = () => {

const [isCardOpen, setIsCardOpen] = useState(true);
const [isLoading, setIsLoading] = useState(true);

const handleCardOpen = () => setIsCardOpen(!isCardOpen);


	  return (
	  			<Layout
	  					title="dashboard"
	  					card2={ isCardOpen ? "" : "hidden" }
	  					handleToggleButton={handleCardOpen}
	  					iconClass={ isCardOpen ? "" : "rotate-180" }
	  					loading={ isLoading ? "" : "hidden" }
	  					card2Label="Add New"
	  			>
	  			</Layout>
	  )
}

export default Home



<div className="mt-5">
		<Typography variant="h6">List of Users </Typography>
		{
				users.map((user) => {
							return(
										<div key={user._id} className="flex mb-1">
										  <Input
										    disabled
										    className="capitalize rounded-r-none"
										    value={user.name}
										  />
										  <Button 
										  		onClick={
										  					navigator.clipboard.writeText(user._id)
										  		}
										  		className="rounded-l-none text-xs py-1 min-w-[3rem]">
										  	Copy Id
										  </Button>
										</div>

								)
				})
		}
</div>